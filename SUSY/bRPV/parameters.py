# ----------------------------------------------------------------------  
# This model file was automatically created by SARAH version4.5.6 
# SARAH References: arXiv:0806.0538, arXiv:0909.2863, arXiv:1002.0840    
# (c) Florian Staub, 2011  
# ----------------------------------------------------------------------  
# File created at 18:51 on 11.6.2015   
# ----------------------------------------------------------------------  
 
 
from object_library import all_parameters,Parameter 
 
from function_library import complexconjugate,re,im,csc,sec,acsc,asec 
 
ZERO=Parameter(name='ZERO', 
                      nature='internal', 
                      type='real', 
                      value='0.0', 
                      texname='0') 
 
Mgo = 	 Parameter(name = 'Mgo', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mgo}', 
	 lhablock = 'MASS', 
	 lhacode = [1000021]) 
 
Wgo = 	 Parameter(name = 'Wgo', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wgo}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000021]) 
 
Mnu4 = 	 Parameter(name = 'Mnu4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mnu4}', 
	 lhablock = 'MASS', 
	 lhacode = [1000022]) 
 
Wnu4 = 	 Parameter(name = 'Wnu4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wnu4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000022]) 
 
Mnu5 = 	 Parameter(name = 'Mnu5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mnu5}', 
	 lhablock = 'MASS', 
	 lhacode = [1000023]) 
 
Wnu5 = 	 Parameter(name = 'Wnu5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wnu5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000023]) 
 
Mnu6 = 	 Parameter(name = 'Mnu6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mnu6}', 
	 lhablock = 'MASS', 
	 lhacode = [1000025]) 
 
Wnu6 = 	 Parameter(name = 'Wnu6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wnu6}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000025]) 
 
Mnu7 = 	 Parameter(name = 'Mnu7', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mnu7}', 
	 lhablock = 'MASS', 
	 lhacode = [1000035]) 
 
Wnu7 = 	 Parameter(name = 'Wnu7', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wnu7}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000035]) 
 
Me1 = 	 Parameter(name = 'Me1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Me1}', 
	 lhablock = 'MASS', 
	 lhacode = [11]) 
 
Me2 = 	 Parameter(name = 'Me2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Me2}', 
	 lhablock = 'MASS', 
	 lhacode = [13]) 
 
Me3 = 	 Parameter(name = 'Me3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Me3}', 
	 lhablock = 'MASS', 
	 lhacode = [15]) 
 
Me4 = 	 Parameter(name = 'Me4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Me4}', 
	 lhablock = 'MASS', 
	 lhacode = [1000024]) 
 
We4 = 	 Parameter(name = 'We4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{We4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000024]) 
 
Me5 = 	 Parameter(name = 'Me5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Me5}', 
	 lhablock = 'MASS', 
	 lhacode = [1000037]) 
 
We5 = 	 Parameter(name = 'We5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{We5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000037]) 
 
Md1 = 	 Parameter(name = 'Md1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0035, 
	 texname = '\\text{Md1}', 
	 lhablock = 'MASS', 
	 lhacode = [1]) 
 
Md2 = 	 Parameter(name = 'Md2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.104, 
	 texname = '\\text{Md2}', 
	 lhablock = 'MASS', 
	 lhacode = [3]) 
 
Md3 = 	 Parameter(name = 'Md3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 4.2, 
	 texname = '\\text{Md3}', 
	 lhablock = 'MASS', 
	 lhacode = [5]) 
 
Mu1 = 	 Parameter(name = 'Mu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0015, 
	 texname = '\\text{Mu1}', 
	 lhablock = 'MASS', 
	 lhacode = [2]) 
 
Mu2 = 	 Parameter(name = 'Mu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.27, 
	 texname = '\\text{Mu2}', 
	 lhablock = 'MASS', 
	 lhacode = [4]) 
 
Mu3 = 	 Parameter(name = 'Mu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 171.2, 
	 texname = '\\text{Mu3}', 
	 lhablock = 'MASS', 
	 lhacode = [6]) 
 
Wu3 = 	 Parameter(name = 'Wu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.51, 
	 texname = '\\text{Wu3}', 
	 lhablock = 'DECAY', 
	 lhacode = [6]) 
 
Msd1 = 	 Parameter(name = 'Msd1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000001]) 
 
Wsd1 = 	 Parameter(name = 'Wsd1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000001]) 
 
Msd2 = 	 Parameter(name = 'Msd2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000003]) 
 
Wsd2 = 	 Parameter(name = 'Wsd2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000003]) 
 
Msd3 = 	 Parameter(name = 'Msd3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000005]) 
 
Wsd3 = 	 Parameter(name = 'Wsd3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000005]) 
 
Msd4 = 	 Parameter(name = 'Msd4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd4}', 
	 lhablock = 'MASS', 
	 lhacode = [2000001]) 
 
Wsd4 = 	 Parameter(name = 'Wsd4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd4}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000001]) 
 
Msd5 = 	 Parameter(name = 'Msd5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd5}', 
	 lhablock = 'MASS', 
	 lhacode = [2000003]) 
 
Wsd5 = 	 Parameter(name = 'Wsd5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd5}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000003]) 
 
Msd6 = 	 Parameter(name = 'Msd6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd6}', 
	 lhablock = 'MASS', 
	 lhacode = [2000005]) 
 
Wsd6 = 	 Parameter(name = 'Wsd6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd6}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000005]) 
 
Msu1 = 	 Parameter(name = 'Msu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000002]) 
 
Wsu1 = 	 Parameter(name = 'Wsu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000002]) 
 
Msu2 = 	 Parameter(name = 'Msu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000004]) 
 
Wsu2 = 	 Parameter(name = 'Wsu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000004]) 
 
Msu3 = 	 Parameter(name = 'Msu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000006]) 
 
Wsu3 = 	 Parameter(name = 'Wsu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000006]) 
 
Msu4 = 	 Parameter(name = 'Msu4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu4}', 
	 lhablock = 'MASS', 
	 lhacode = [2000002]) 
 
Wsu4 = 	 Parameter(name = 'Wsu4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu4}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000002]) 
 
Msu5 = 	 Parameter(name = 'Msu5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu5}', 
	 lhablock = 'MASS', 
	 lhacode = [2000004]) 
 
Wsu5 = 	 Parameter(name = 'Wsu5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu5}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000004]) 
 
Msu6 = 	 Parameter(name = 'Msu6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu6}', 
	 lhablock = 'MASS', 
	 lhacode = [2000006]) 
 
Wsu6 = 	 Parameter(name = 'Wsu6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu6}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000006]) 
 
Mh1 = 	 Parameter(name = 'Mh1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh1}', 
	 lhablock = 'MASS', 
	 lhacode = [25]) 
 
Wh1 = 	 Parameter(name = 'Wh1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh1}', 
	 lhablock = 'DECAY', 
	 lhacode = [25]) 
 
Mh2 = 	 Parameter(name = 'Mh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh2}', 
	 lhablock = 'MASS', 
	 lhacode = [35]) 
 
Wh2 = 	 Parameter(name = 'Wh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh2}', 
	 lhablock = 'DECAY', 
	 lhacode = [35]) 
 
Mh3 = 	 Parameter(name = 'Mh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000012]) 
 
Wh3 = 	 Parameter(name = 'Wh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000012]) 
 
Mh4 = 	 Parameter(name = 'Mh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh4}', 
	 lhablock = 'MASS', 
	 lhacode = [1000014]) 
 
Wh4 = 	 Parameter(name = 'Wh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000014]) 
 
Mh5 = 	 Parameter(name = 'Mh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh5}', 
	 lhablock = 'MASS', 
	 lhacode = [1000016]) 
 
Wh5 = 	 Parameter(name = 'Wh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000016]) 
 
MAh2 = 	 Parameter(name = 'MAh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MAh2}', 
	 lhablock = 'MASS', 
	 lhacode = [36]) 
 
WAh2 = 	 Parameter(name = 'WAh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh2}', 
	 lhablock = 'DECAY', 
	 lhacode = [36]) 
 
MAh3 = 	 Parameter(name = 'MAh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MAh3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000017]) 
 
WAh3 = 	 Parameter(name = 'WAh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000017]) 
 
MAh4 = 	 Parameter(name = 'MAh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MAh4}', 
	 lhablock = 'MASS', 
	 lhacode = [1000018]) 
 
WAh4 = 	 Parameter(name = 'WAh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000018]) 
 
MAh5 = 	 Parameter(name = 'MAh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MAh5}', 
	 lhablock = 'MASS', 
	 lhacode = [1000019]) 
 
WAh5 = 	 Parameter(name = 'WAh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000019]) 
 
MHm2 = 	 Parameter(name = 'MHm2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm2}', 
	 lhablock = 'MASS', 
	 lhacode = [37]) 
 
WHm2 = 	 Parameter(name = 'WHm2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm2}', 
	 lhablock = 'DECAY', 
	 lhacode = [37]) 
 
MHm3 = 	 Parameter(name = 'MHm3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000011]) 
 
WHm3 = 	 Parameter(name = 'WHm3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000011]) 
 
MHm4 = 	 Parameter(name = 'MHm4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm4}', 
	 lhablock = 'MASS', 
	 lhacode = [2000011]) 
 
WHm4 = 	 Parameter(name = 'WHm4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm4}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000011]) 
 
MHm5 = 	 Parameter(name = 'MHm5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm5}', 
	 lhablock = 'MASS', 
	 lhacode = [1000013]) 
 
WHm5 = 	 Parameter(name = 'WHm5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000013]) 
 
MHm6 = 	 Parameter(name = 'MHm6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm6}', 
	 lhablock = 'MASS', 
	 lhacode = [2000013]) 
 
WHm6 = 	 Parameter(name = 'WHm6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm6}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000013]) 
 
MHm7 = 	 Parameter(name = 'MHm7', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm7}', 
	 lhablock = 'MASS', 
	 lhacode = [1000015]) 
 
WHm7 = 	 Parameter(name = 'WHm7', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm7}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000015]) 
 
MHm8 = 	 Parameter(name = 'MHm8', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm8}', 
	 lhablock = 'MASS', 
	 lhacode = [2000015]) 
 
WHm8 = 	 Parameter(name = 'WHm8', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm8}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000015]) 
 
MZ = 	 Parameter(name = 'MZ', 
	 nature = 'external', 
	 type = 'real', 
	 value = 91.1876, 
	 texname = '\\text{MZ}', 
	 lhablock = 'MASS', 
	 lhacode = [23]) 
 
WZ = 	 Parameter(name = 'WZ', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2.4952, 
	 texname = '\\text{WZ}', 
	 lhablock = 'DECAY', 
	 lhacode = [23]) 
 
WWm = 	 Parameter(name = 'WWm', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2.141, 
	 texname = '\\text{WWm}', 
	 lhablock = 'DECAY', 
	 lhacode = [24]) 
 
rMu = 	 Parameter(name='rMu', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Mu}', 
	 lhablock = 'HMIX', 
	 lhacode = [1] ) 
 
iMu = 	 Parameter(name='iMu', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Mu}', 
	 lhablock = 'IMHMIX', 
	 lhacode = [1] ) 
 
rREps1 = 	 Parameter(name='rREps1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{REps1}', 
	 lhablock = 'RVKAPPA', 
	 lhacode = [1] ) 
 
iREps1 = 	 Parameter(name='iREps1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{REps1}', 
	 lhablock = 'IMRVKAPPA', 
	 lhacode = [1] ) 
 
rREps2 = 	 Parameter(name='rREps2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{REps2}', 
	 lhablock = 'RVKAPPA', 
	 lhacode = [2] ) 
 
iREps2 = 	 Parameter(name='iREps2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{REps2}', 
	 lhablock = 'IMRVKAPPA', 
	 lhacode = [2] ) 
 
rREps3 = 	 Parameter(name='rREps3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{REps3}', 
	 lhablock = 'RVKAPPA', 
	 lhacode = [3] ) 
 
iREps3 = 	 Parameter(name='iREps3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{REps3}', 
	 lhablock = 'IMRVKAPPA', 
	 lhacode = [3] ) 
 
rYd11 = 	 Parameter(name='rYd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd11}', 
	 lhablock = 'YD', 
	 lhacode = [1, 1] ) 
 
iYd11 = 	 Parameter(name='iYd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd11}', 
	 lhablock = 'IMYD', 
	 lhacode = [1, 1] ) 
 
rYd12 = 	 Parameter(name='rYd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd12}', 
	 lhablock = 'YD', 
	 lhacode = [1, 2] ) 
 
iYd12 = 	 Parameter(name='iYd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd12}', 
	 lhablock = 'IMYD', 
	 lhacode = [1, 2] ) 
 
rYd13 = 	 Parameter(name='rYd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd13}', 
	 lhablock = 'YD', 
	 lhacode = [1, 3] ) 
 
iYd13 = 	 Parameter(name='iYd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd13}', 
	 lhablock = 'IMYD', 
	 lhacode = [1, 3] ) 
 
rYd21 = 	 Parameter(name='rYd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd21}', 
	 lhablock = 'YD', 
	 lhacode = [2, 1] ) 
 
iYd21 = 	 Parameter(name='iYd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd21}', 
	 lhablock = 'IMYD', 
	 lhacode = [2, 1] ) 
 
rYd22 = 	 Parameter(name='rYd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd22}', 
	 lhablock = 'YD', 
	 lhacode = [2, 2] ) 
 
iYd22 = 	 Parameter(name='iYd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd22}', 
	 lhablock = 'IMYD', 
	 lhacode = [2, 2] ) 
 
rYd23 = 	 Parameter(name='rYd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd23}', 
	 lhablock = 'YD', 
	 lhacode = [2, 3] ) 
 
iYd23 = 	 Parameter(name='iYd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd23}', 
	 lhablock = 'IMYD', 
	 lhacode = [2, 3] ) 
 
rYd31 = 	 Parameter(name='rYd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd31}', 
	 lhablock = 'YD', 
	 lhacode = [3, 1] ) 
 
iYd31 = 	 Parameter(name='iYd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd31}', 
	 lhablock = 'IMYD', 
	 lhacode = [3, 1] ) 
 
rYd32 = 	 Parameter(name='rYd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd32}', 
	 lhablock = 'YD', 
	 lhacode = [3, 2] ) 
 
iYd32 = 	 Parameter(name='iYd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd32}', 
	 lhablock = 'IMYD', 
	 lhacode = [3, 2] ) 
 
rYd33 = 	 Parameter(name='rYd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd33}', 
	 lhablock = 'YD', 
	 lhacode = [3, 3] ) 
 
iYd33 = 	 Parameter(name='iYd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd33}', 
	 lhablock = 'IMYD', 
	 lhacode = [3, 3] ) 
 
rTd11 = 	 Parameter(name='rTd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td11}', 
	 lhablock = 'TD', 
	 lhacode = [1, 1] ) 
 
iTd11 = 	 Parameter(name='iTd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td11}', 
	 lhablock = 'IMTD', 
	 lhacode = [1, 1] ) 
 
rTd12 = 	 Parameter(name='rTd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td12}', 
	 lhablock = 'TD', 
	 lhacode = [1, 2] ) 
 
iTd12 = 	 Parameter(name='iTd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td12}', 
	 lhablock = 'IMTD', 
	 lhacode = [1, 2] ) 
 
rTd13 = 	 Parameter(name='rTd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td13}', 
	 lhablock = 'TD', 
	 lhacode = [1, 3] ) 
 
iTd13 = 	 Parameter(name='iTd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td13}', 
	 lhablock = 'IMTD', 
	 lhacode = [1, 3] ) 
 
rTd21 = 	 Parameter(name='rTd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td21}', 
	 lhablock = 'TD', 
	 lhacode = [2, 1] ) 
 
iTd21 = 	 Parameter(name='iTd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td21}', 
	 lhablock = 'IMTD', 
	 lhacode = [2, 1] ) 
 
rTd22 = 	 Parameter(name='rTd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td22}', 
	 lhablock = 'TD', 
	 lhacode = [2, 2] ) 
 
iTd22 = 	 Parameter(name='iTd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td22}', 
	 lhablock = 'IMTD', 
	 lhacode = [2, 2] ) 
 
rTd23 = 	 Parameter(name='rTd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td23}', 
	 lhablock = 'TD', 
	 lhacode = [2, 3] ) 
 
iTd23 = 	 Parameter(name='iTd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td23}', 
	 lhablock = 'IMTD', 
	 lhacode = [2, 3] ) 
 
rTd31 = 	 Parameter(name='rTd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td31}', 
	 lhablock = 'TD', 
	 lhacode = [3, 1] ) 
 
iTd31 = 	 Parameter(name='iTd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td31}', 
	 lhablock = 'IMTD', 
	 lhacode = [3, 1] ) 
 
rTd32 = 	 Parameter(name='rTd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td32}', 
	 lhablock = 'TD', 
	 lhacode = [3, 2] ) 
 
iTd32 = 	 Parameter(name='iTd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td32}', 
	 lhablock = 'IMTD', 
	 lhacode = [3, 2] ) 
 
rTd33 = 	 Parameter(name='rTd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td33}', 
	 lhablock = 'TD', 
	 lhacode = [3, 3] ) 
 
iTd33 = 	 Parameter(name='iTd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td33}', 
	 lhablock = 'IMTD', 
	 lhacode = [3, 3] ) 
 
rTe11 = 	 Parameter(name='rTe11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te11}', 
	 lhablock = 'TE', 
	 lhacode = [1, 1] ) 
 
iTe11 = 	 Parameter(name='iTe11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te11}', 
	 lhablock = 'IMTE', 
	 lhacode = [1, 1] ) 
 
rTe12 = 	 Parameter(name='rTe12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te12}', 
	 lhablock = 'TE', 
	 lhacode = [1, 2] ) 
 
iTe12 = 	 Parameter(name='iTe12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te12}', 
	 lhablock = 'IMTE', 
	 lhacode = [1, 2] ) 
 
rTe13 = 	 Parameter(name='rTe13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te13}', 
	 lhablock = 'TE', 
	 lhacode = [1, 3] ) 
 
iTe13 = 	 Parameter(name='iTe13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te13}', 
	 lhablock = 'IMTE', 
	 lhacode = [1, 3] ) 
 
rTe21 = 	 Parameter(name='rTe21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te21}', 
	 lhablock = 'TE', 
	 lhacode = [2, 1] ) 
 
iTe21 = 	 Parameter(name='iTe21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te21}', 
	 lhablock = 'IMTE', 
	 lhacode = [2, 1] ) 
 
rTe22 = 	 Parameter(name='rTe22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te22}', 
	 lhablock = 'TE', 
	 lhacode = [2, 2] ) 
 
iTe22 = 	 Parameter(name='iTe22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te22}', 
	 lhablock = 'IMTE', 
	 lhacode = [2, 2] ) 
 
rTe23 = 	 Parameter(name='rTe23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te23}', 
	 lhablock = 'TE', 
	 lhacode = [2, 3] ) 
 
iTe23 = 	 Parameter(name='iTe23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te23}', 
	 lhablock = 'IMTE', 
	 lhacode = [2, 3] ) 
 
rTe31 = 	 Parameter(name='rTe31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te31}', 
	 lhablock = 'TE', 
	 lhacode = [3, 1] ) 
 
iTe31 = 	 Parameter(name='iTe31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te31}', 
	 lhablock = 'IMTE', 
	 lhacode = [3, 1] ) 
 
rTe32 = 	 Parameter(name='rTe32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te32}', 
	 lhablock = 'TE', 
	 lhacode = [3, 2] ) 
 
iTe32 = 	 Parameter(name='iTe32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te32}', 
	 lhablock = 'IMTE', 
	 lhacode = [3, 2] ) 
 
rTe33 = 	 Parameter(name='rTe33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te33}', 
	 lhablock = 'TE', 
	 lhacode = [3, 3] ) 
 
iTe33 = 	 Parameter(name='iTe33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te33}', 
	 lhablock = 'IMTE', 
	 lhacode = [3, 3] ) 
 
rYu11 = 	 Parameter(name='rYu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu11}', 
	 lhablock = 'YU', 
	 lhacode = [1, 1] ) 
 
iYu11 = 	 Parameter(name='iYu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu11}', 
	 lhablock = 'IMYU', 
	 lhacode = [1, 1] ) 
 
rYu12 = 	 Parameter(name='rYu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu12}', 
	 lhablock = 'YU', 
	 lhacode = [1, 2] ) 
 
iYu12 = 	 Parameter(name='iYu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu12}', 
	 lhablock = 'IMYU', 
	 lhacode = [1, 2] ) 
 
rYu13 = 	 Parameter(name='rYu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu13}', 
	 lhablock = 'YU', 
	 lhacode = [1, 3] ) 
 
iYu13 = 	 Parameter(name='iYu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu13}', 
	 lhablock = 'IMYU', 
	 lhacode = [1, 3] ) 
 
rYu21 = 	 Parameter(name='rYu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu21}', 
	 lhablock = 'YU', 
	 lhacode = [2, 1] ) 
 
iYu21 = 	 Parameter(name='iYu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu21}', 
	 lhablock = 'IMYU', 
	 lhacode = [2, 1] ) 
 
rYu22 = 	 Parameter(name='rYu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu22}', 
	 lhablock = 'YU', 
	 lhacode = [2, 2] ) 
 
iYu22 = 	 Parameter(name='iYu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu22}', 
	 lhablock = 'IMYU', 
	 lhacode = [2, 2] ) 
 
rYu23 = 	 Parameter(name='rYu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu23}', 
	 lhablock = 'YU', 
	 lhacode = [2, 3] ) 
 
iYu23 = 	 Parameter(name='iYu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu23}', 
	 lhablock = 'IMYU', 
	 lhacode = [2, 3] ) 
 
rYu31 = 	 Parameter(name='rYu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu31}', 
	 lhablock = 'YU', 
	 lhacode = [3, 1] ) 
 
iYu31 = 	 Parameter(name='iYu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu31}', 
	 lhablock = 'IMYU', 
	 lhacode = [3, 1] ) 
 
rYu32 = 	 Parameter(name='rYu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu32}', 
	 lhablock = 'YU', 
	 lhacode = [3, 2] ) 
 
iYu32 = 	 Parameter(name='iYu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu32}', 
	 lhablock = 'IMYU', 
	 lhacode = [3, 2] ) 
 
rYu33 = 	 Parameter(name='rYu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu33}', 
	 lhablock = 'YU', 
	 lhacode = [3, 3] ) 
 
iYu33 = 	 Parameter(name='iYu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu33}', 
	 lhablock = 'IMYU', 
	 lhacode = [3, 3] ) 
 
rTu11 = 	 Parameter(name='rTu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu11}', 
	 lhablock = 'TU', 
	 lhacode = [1, 1] ) 
 
iTu11 = 	 Parameter(name='iTu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu11}', 
	 lhablock = 'IMTU', 
	 lhacode = [1, 1] ) 
 
rTu12 = 	 Parameter(name='rTu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu12}', 
	 lhablock = 'TU', 
	 lhacode = [1, 2] ) 
 
iTu12 = 	 Parameter(name='iTu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu12}', 
	 lhablock = 'IMTU', 
	 lhacode = [1, 2] ) 
 
rTu13 = 	 Parameter(name='rTu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu13}', 
	 lhablock = 'TU', 
	 lhacode = [1, 3] ) 
 
iTu13 = 	 Parameter(name='iTu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu13}', 
	 lhablock = 'IMTU', 
	 lhacode = [1, 3] ) 
 
rTu21 = 	 Parameter(name='rTu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu21}', 
	 lhablock = 'TU', 
	 lhacode = [2, 1] ) 
 
iTu21 = 	 Parameter(name='iTu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu21}', 
	 lhablock = 'IMTU', 
	 lhacode = [2, 1] ) 
 
rTu22 = 	 Parameter(name='rTu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu22}', 
	 lhablock = 'TU', 
	 lhacode = [2, 2] ) 
 
iTu22 = 	 Parameter(name='iTu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu22}', 
	 lhablock = 'IMTU', 
	 lhacode = [2, 2] ) 
 
rTu23 = 	 Parameter(name='rTu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu23}', 
	 lhablock = 'TU', 
	 lhacode = [2, 3] ) 
 
iTu23 = 	 Parameter(name='iTu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu23}', 
	 lhablock = 'IMTU', 
	 lhacode = [2, 3] ) 
 
rTu31 = 	 Parameter(name='rTu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu31}', 
	 lhablock = 'TU', 
	 lhacode = [3, 1] ) 
 
iTu31 = 	 Parameter(name='iTu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu31}', 
	 lhablock = 'IMTU', 
	 lhacode = [3, 1] ) 
 
rTu32 = 	 Parameter(name='rTu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu32}', 
	 lhablock = 'TU', 
	 lhacode = [3, 2] ) 
 
iTu32 = 	 Parameter(name='iTu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu32}', 
	 lhablock = 'IMTU', 
	 lhacode = [3, 2] ) 
 
rTu33 = 	 Parameter(name='rTu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu33}', 
	 lhablock = 'TU', 
	 lhacode = [3, 3] ) 
 
iTu33 = 	 Parameter(name='iTu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu33}', 
	 lhablock = 'IMTU', 
	 lhacode = [3, 3] ) 
 
vd = 	 Parameter(name='vd', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{vd}', 
	 lhablock = 'HMIX', 
	 lhacode = [102] ) 
 
vu = 	 Parameter(name='vu', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{vu}', 
	 lhablock = 'HMIX', 
	 lhacode = [103] ) 
 
vL1 = 	 Parameter(name='vL1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vL1}', 
	 lhablock = 'RVSNVEV', 
	 lhacode = [1] ) 
 
vL2 = 	 Parameter(name='vL2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vL2}', 
	 lhablock = 'RVSNVEV', 
	 lhacode = [2] ) 
 
vL3 = 	 Parameter(name='vL3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vL3}', 
	 lhablock = 'RVSNVEV', 
	 lhacode = [3] ) 
 
rpG = 	 Parameter(name='rpG', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{pG}', 
	 lhablock = 'PHASES', 
	 lhacode = [1] ) 
 
ipG = 	 Parameter(name='ipG', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{pG}', 
	 lhablock = 'IMPHASES', 
	 lhacode = [1] ) 
 
rZD11 = 	 Parameter(name='rZD11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD11}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 1] ) 
 
iZD11 = 	 Parameter(name='iZD11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD11}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 1] ) 
 
rZD12 = 	 Parameter(name='rZD12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD12}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 2] ) 
 
iZD12 = 	 Parameter(name='iZD12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD12}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 2] ) 
 
rZD13 = 	 Parameter(name='rZD13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD13}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 3] ) 
 
iZD13 = 	 Parameter(name='iZD13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD13}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 3] ) 
 
rZD14 = 	 Parameter(name='rZD14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD14}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 4] ) 
 
iZD14 = 	 Parameter(name='iZD14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD14}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 4] ) 
 
rZD15 = 	 Parameter(name='rZD15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD15}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 5] ) 
 
iZD15 = 	 Parameter(name='iZD15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD15}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 5] ) 
 
rZD16 = 	 Parameter(name='rZD16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD16}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 6] ) 
 
iZD16 = 	 Parameter(name='iZD16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD16}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 6] ) 
 
rZD21 = 	 Parameter(name='rZD21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD21}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 1] ) 
 
iZD21 = 	 Parameter(name='iZD21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD21}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 1] ) 
 
rZD22 = 	 Parameter(name='rZD22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD22}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 2] ) 
 
iZD22 = 	 Parameter(name='iZD22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD22}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 2] ) 
 
rZD23 = 	 Parameter(name='rZD23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD23}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 3] ) 
 
iZD23 = 	 Parameter(name='iZD23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD23}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 3] ) 
 
rZD24 = 	 Parameter(name='rZD24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD24}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 4] ) 
 
iZD24 = 	 Parameter(name='iZD24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD24}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 4] ) 
 
rZD25 = 	 Parameter(name='rZD25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD25}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 5] ) 
 
iZD25 = 	 Parameter(name='iZD25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD25}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 5] ) 
 
rZD26 = 	 Parameter(name='rZD26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD26}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 6] ) 
 
iZD26 = 	 Parameter(name='iZD26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD26}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 6] ) 
 
rZD31 = 	 Parameter(name='rZD31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD31}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 1] ) 
 
iZD31 = 	 Parameter(name='iZD31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD31}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 1] ) 
 
rZD32 = 	 Parameter(name='rZD32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD32}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 2] ) 
 
iZD32 = 	 Parameter(name='iZD32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD32}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 2] ) 
 
rZD33 = 	 Parameter(name='rZD33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD33}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 3] ) 
 
iZD33 = 	 Parameter(name='iZD33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD33}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 3] ) 
 
rZD34 = 	 Parameter(name='rZD34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD34}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 4] ) 
 
iZD34 = 	 Parameter(name='iZD34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD34}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 4] ) 
 
rZD35 = 	 Parameter(name='rZD35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD35}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 5] ) 
 
iZD35 = 	 Parameter(name='iZD35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD35}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 5] ) 
 
rZD36 = 	 Parameter(name='rZD36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD36}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 6] ) 
 
iZD36 = 	 Parameter(name='iZD36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD36}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 6] ) 
 
rZD41 = 	 Parameter(name='rZD41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD41}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 1] ) 
 
iZD41 = 	 Parameter(name='iZD41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD41}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 1] ) 
 
rZD42 = 	 Parameter(name='rZD42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD42}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 2] ) 
 
iZD42 = 	 Parameter(name='iZD42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD42}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 2] ) 
 
rZD43 = 	 Parameter(name='rZD43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD43}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 3] ) 
 
iZD43 = 	 Parameter(name='iZD43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD43}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 3] ) 
 
rZD44 = 	 Parameter(name='rZD44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD44}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 4] ) 
 
iZD44 = 	 Parameter(name='iZD44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD44}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 4] ) 
 
rZD45 = 	 Parameter(name='rZD45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD45}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 5] ) 
 
iZD45 = 	 Parameter(name='iZD45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD45}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 5] ) 
 
rZD46 = 	 Parameter(name='rZD46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD46}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 6] ) 
 
iZD46 = 	 Parameter(name='iZD46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD46}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 6] ) 
 
rZD51 = 	 Parameter(name='rZD51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD51}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 1] ) 
 
iZD51 = 	 Parameter(name='iZD51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD51}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 1] ) 
 
rZD52 = 	 Parameter(name='rZD52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD52}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 2] ) 
 
iZD52 = 	 Parameter(name='iZD52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD52}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 2] ) 
 
rZD53 = 	 Parameter(name='rZD53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD53}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 3] ) 
 
iZD53 = 	 Parameter(name='iZD53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD53}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 3] ) 
 
rZD54 = 	 Parameter(name='rZD54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD54}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 4] ) 
 
iZD54 = 	 Parameter(name='iZD54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD54}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 4] ) 
 
rZD55 = 	 Parameter(name='rZD55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD55}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 5] ) 
 
iZD55 = 	 Parameter(name='iZD55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD55}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 5] ) 
 
rZD56 = 	 Parameter(name='rZD56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD56}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 6] ) 
 
iZD56 = 	 Parameter(name='iZD56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD56}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 6] ) 
 
rZD61 = 	 Parameter(name='rZD61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD61}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 1] ) 
 
iZD61 = 	 Parameter(name='iZD61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD61}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 1] ) 
 
rZD62 = 	 Parameter(name='rZD62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD62}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 2] ) 
 
iZD62 = 	 Parameter(name='iZD62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD62}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 2] ) 
 
rZD63 = 	 Parameter(name='rZD63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD63}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 3] ) 
 
iZD63 = 	 Parameter(name='iZD63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD63}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 3] ) 
 
rZD64 = 	 Parameter(name='rZD64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD64}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 4] ) 
 
iZD64 = 	 Parameter(name='iZD64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD64}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 4] ) 
 
rZD65 = 	 Parameter(name='rZD65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD65}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 5] ) 
 
iZD65 = 	 Parameter(name='iZD65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD65}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 5] ) 
 
rZD66 = 	 Parameter(name='rZD66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD66}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 6] ) 
 
iZD66 = 	 Parameter(name='iZD66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD66}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 6] ) 
 
rZU11 = 	 Parameter(name='rZU11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU11}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 1] ) 
 
iZU11 = 	 Parameter(name='iZU11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU11}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 1] ) 
 
rZU12 = 	 Parameter(name='rZU12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU12}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 2] ) 
 
iZU12 = 	 Parameter(name='iZU12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU12}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 2] ) 
 
rZU13 = 	 Parameter(name='rZU13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU13}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 3] ) 
 
iZU13 = 	 Parameter(name='iZU13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU13}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 3] ) 
 
rZU14 = 	 Parameter(name='rZU14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU14}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 4] ) 
 
iZU14 = 	 Parameter(name='iZU14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU14}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 4] ) 
 
rZU15 = 	 Parameter(name='rZU15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU15}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 5] ) 
 
iZU15 = 	 Parameter(name='iZU15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU15}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 5] ) 
 
rZU16 = 	 Parameter(name='rZU16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU16}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 6] ) 
 
iZU16 = 	 Parameter(name='iZU16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU16}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 6] ) 
 
rZU21 = 	 Parameter(name='rZU21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU21}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 1] ) 
 
iZU21 = 	 Parameter(name='iZU21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU21}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 1] ) 
 
rZU22 = 	 Parameter(name='rZU22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU22}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 2] ) 
 
iZU22 = 	 Parameter(name='iZU22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU22}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 2] ) 
 
rZU23 = 	 Parameter(name='rZU23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU23}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 3] ) 
 
iZU23 = 	 Parameter(name='iZU23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU23}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 3] ) 
 
rZU24 = 	 Parameter(name='rZU24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU24}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 4] ) 
 
iZU24 = 	 Parameter(name='iZU24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU24}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 4] ) 
 
rZU25 = 	 Parameter(name='rZU25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU25}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 5] ) 
 
iZU25 = 	 Parameter(name='iZU25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU25}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 5] ) 
 
rZU26 = 	 Parameter(name='rZU26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU26}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 6] ) 
 
iZU26 = 	 Parameter(name='iZU26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU26}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 6] ) 
 
rZU31 = 	 Parameter(name='rZU31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU31}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 1] ) 
 
iZU31 = 	 Parameter(name='iZU31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU31}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 1] ) 
 
rZU32 = 	 Parameter(name='rZU32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU32}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 2] ) 
 
iZU32 = 	 Parameter(name='iZU32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU32}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 2] ) 
 
rZU33 = 	 Parameter(name='rZU33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU33}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 3] ) 
 
iZU33 = 	 Parameter(name='iZU33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU33}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 3] ) 
 
rZU34 = 	 Parameter(name='rZU34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU34}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 4] ) 
 
iZU34 = 	 Parameter(name='iZU34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU34}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 4] ) 
 
rZU35 = 	 Parameter(name='rZU35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU35}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 5] ) 
 
iZU35 = 	 Parameter(name='iZU35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU35}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 5] ) 
 
rZU36 = 	 Parameter(name='rZU36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU36}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 6] ) 
 
iZU36 = 	 Parameter(name='iZU36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU36}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 6] ) 
 
rZU41 = 	 Parameter(name='rZU41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU41}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 1] ) 
 
iZU41 = 	 Parameter(name='iZU41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU41}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 1] ) 
 
rZU42 = 	 Parameter(name='rZU42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU42}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 2] ) 
 
iZU42 = 	 Parameter(name='iZU42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU42}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 2] ) 
 
rZU43 = 	 Parameter(name='rZU43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU43}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 3] ) 
 
iZU43 = 	 Parameter(name='iZU43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU43}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 3] ) 
 
rZU44 = 	 Parameter(name='rZU44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU44}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 4] ) 
 
iZU44 = 	 Parameter(name='iZU44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU44}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 4] ) 
 
rZU45 = 	 Parameter(name='rZU45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU45}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 5] ) 
 
iZU45 = 	 Parameter(name='iZU45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU45}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 5] ) 
 
rZU46 = 	 Parameter(name='rZU46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU46}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 6] ) 
 
iZU46 = 	 Parameter(name='iZU46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU46}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 6] ) 
 
rZU51 = 	 Parameter(name='rZU51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU51}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 1] ) 
 
iZU51 = 	 Parameter(name='iZU51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU51}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 1] ) 
 
rZU52 = 	 Parameter(name='rZU52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU52}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 2] ) 
 
iZU52 = 	 Parameter(name='iZU52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU52}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 2] ) 
 
rZU53 = 	 Parameter(name='rZU53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU53}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 3] ) 
 
iZU53 = 	 Parameter(name='iZU53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU53}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 3] ) 
 
rZU54 = 	 Parameter(name='rZU54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU54}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 4] ) 
 
iZU54 = 	 Parameter(name='iZU54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU54}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 4] ) 
 
rZU55 = 	 Parameter(name='rZU55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU55}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 5] ) 
 
iZU55 = 	 Parameter(name='iZU55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU55}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 5] ) 
 
rZU56 = 	 Parameter(name='rZU56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU56}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 6] ) 
 
iZU56 = 	 Parameter(name='iZU56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU56}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 6] ) 
 
rZU61 = 	 Parameter(name='rZU61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU61}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 1] ) 
 
iZU61 = 	 Parameter(name='iZU61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU61}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 1] ) 
 
rZU62 = 	 Parameter(name='rZU62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU62}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 2] ) 
 
iZU62 = 	 Parameter(name='iZU62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU62}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 2] ) 
 
rZU63 = 	 Parameter(name='rZU63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU63}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 3] ) 
 
iZU63 = 	 Parameter(name='iZU63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU63}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 3] ) 
 
rZU64 = 	 Parameter(name='rZU64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU64}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 4] ) 
 
iZU64 = 	 Parameter(name='iZU64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU64}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 4] ) 
 
rZU65 = 	 Parameter(name='rZU65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU65}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 5] ) 
 
iZU65 = 	 Parameter(name='iZU65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU65}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 5] ) 
 
rZU66 = 	 Parameter(name='rZU66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU66}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 6] ) 
 
iZU66 = 	 Parameter(name='iZU66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU66}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 6] ) 
 
ZH11 = 	 Parameter(name='ZH11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH11}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [1, 1] ) 
 
ZH12 = 	 Parameter(name='ZH12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH12}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [1, 2] ) 
 
ZH13 = 	 Parameter(name='ZH13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH13}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [1, 3] ) 
 
ZH14 = 	 Parameter(name='ZH14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH14}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [1, 4] ) 
 
ZH15 = 	 Parameter(name='ZH15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH15}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [1, 5] ) 
 
ZH21 = 	 Parameter(name='ZH21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH21}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [2, 1] ) 
 
ZH22 = 	 Parameter(name='ZH22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH22}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [2, 2] ) 
 
ZH23 = 	 Parameter(name='ZH23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH23}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [2, 3] ) 
 
ZH24 = 	 Parameter(name='ZH24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH24}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [2, 4] ) 
 
ZH25 = 	 Parameter(name='ZH25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH25}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [2, 5] ) 
 
ZH31 = 	 Parameter(name='ZH31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH31}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [3, 1] ) 
 
ZH32 = 	 Parameter(name='ZH32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH32}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [3, 2] ) 
 
ZH33 = 	 Parameter(name='ZH33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH33}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [3, 3] ) 
 
ZH34 = 	 Parameter(name='ZH34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH34}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [3, 4] ) 
 
ZH35 = 	 Parameter(name='ZH35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH35}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [3, 5] ) 
 
ZH41 = 	 Parameter(name='ZH41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH41}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [4, 1] ) 
 
ZH42 = 	 Parameter(name='ZH42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH42}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [4, 2] ) 
 
ZH43 = 	 Parameter(name='ZH43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH43}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [4, 3] ) 
 
ZH44 = 	 Parameter(name='ZH44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH44}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [4, 4] ) 
 
ZH45 = 	 Parameter(name='ZH45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH45}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [4, 5] ) 
 
ZH51 = 	 Parameter(name='ZH51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH51}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [5, 1] ) 
 
ZH52 = 	 Parameter(name='ZH52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH52}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [5, 2] ) 
 
ZH53 = 	 Parameter(name='ZH53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH53}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [5, 3] ) 
 
ZH54 = 	 Parameter(name='ZH54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH54}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [5, 4] ) 
 
ZH55 = 	 Parameter(name='ZH55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH55}', 
	 lhablock = 'SCALARMIX', 
	 lhacode = [5, 5] ) 
 
ZA11 = 	 Parameter(name='ZA11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA11}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [1, 1] ) 
 
ZA12 = 	 Parameter(name='ZA12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA12}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [1, 2] ) 
 
ZA13 = 	 Parameter(name='ZA13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA13}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [1, 3] ) 
 
ZA14 = 	 Parameter(name='ZA14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA14}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [1, 4] ) 
 
ZA15 = 	 Parameter(name='ZA15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA15}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [1, 5] ) 
 
ZA21 = 	 Parameter(name='ZA21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA21}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [2, 1] ) 
 
ZA22 = 	 Parameter(name='ZA22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA22}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [2, 2] ) 
 
ZA23 = 	 Parameter(name='ZA23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA23}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [2, 3] ) 
 
ZA24 = 	 Parameter(name='ZA24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA24}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [2, 4] ) 
 
ZA25 = 	 Parameter(name='ZA25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA25}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [2, 5] ) 
 
ZA31 = 	 Parameter(name='ZA31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA31}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [3, 1] ) 
 
ZA32 = 	 Parameter(name='ZA32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA32}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [3, 2] ) 
 
ZA33 = 	 Parameter(name='ZA33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA33}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [3, 3] ) 
 
ZA34 = 	 Parameter(name='ZA34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA34}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [3, 4] ) 
 
ZA35 = 	 Parameter(name='ZA35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA35}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [3, 5] ) 
 
ZA41 = 	 Parameter(name='ZA41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA41}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [4, 1] ) 
 
ZA42 = 	 Parameter(name='ZA42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA42}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [4, 2] ) 
 
ZA43 = 	 Parameter(name='ZA43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA43}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [4, 3] ) 
 
ZA44 = 	 Parameter(name='ZA44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA44}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [4, 4] ) 
 
ZA45 = 	 Parameter(name='ZA45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA45}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [4, 5] ) 
 
ZA51 = 	 Parameter(name='ZA51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA51}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [5, 1] ) 
 
ZA52 = 	 Parameter(name='ZA52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA52}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [5, 2] ) 
 
ZA53 = 	 Parameter(name='ZA53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA53}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [5, 3] ) 
 
ZA54 = 	 Parameter(name='ZA54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA54}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [5, 4] ) 
 
ZA55 = 	 Parameter(name='ZA55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA55}', 
	 lhablock = 'PSEUDOSCALARMIX', 
	 lhacode = [5, 5] ) 
 
rZP11 = 	 Parameter(name='rZP11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP11}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 1] ) 
 
iZP11 = 	 Parameter(name='iZP11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP11}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 1] ) 
 
rZP12 = 	 Parameter(name='rZP12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP12}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 2] ) 
 
iZP12 = 	 Parameter(name='iZP12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP12}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 2] ) 
 
rZP13 = 	 Parameter(name='rZP13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP13}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 3] ) 
 
iZP13 = 	 Parameter(name='iZP13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP13}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 3] ) 
 
rZP14 = 	 Parameter(name='rZP14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP14}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 4] ) 
 
iZP14 = 	 Parameter(name='iZP14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP14}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 4] ) 
 
rZP15 = 	 Parameter(name='rZP15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP15}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 5] ) 
 
iZP15 = 	 Parameter(name='iZP15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP15}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 5] ) 
 
rZP16 = 	 Parameter(name='rZP16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP16}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 6] ) 
 
iZP16 = 	 Parameter(name='iZP16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP16}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 6] ) 
 
rZP17 = 	 Parameter(name='rZP17', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP17}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 7] ) 
 
iZP17 = 	 Parameter(name='iZP17', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP17}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 7] ) 
 
rZP18 = 	 Parameter(name='rZP18', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP18}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [1, 8] ) 
 
iZP18 = 	 Parameter(name='iZP18', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP18}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [1, 8] ) 
 
rZP21 = 	 Parameter(name='rZP21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP21}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 1] ) 
 
iZP21 = 	 Parameter(name='iZP21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP21}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 1] ) 
 
rZP22 = 	 Parameter(name='rZP22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP22}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 2] ) 
 
iZP22 = 	 Parameter(name='iZP22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP22}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 2] ) 
 
rZP23 = 	 Parameter(name='rZP23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP23}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 3] ) 
 
iZP23 = 	 Parameter(name='iZP23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP23}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 3] ) 
 
rZP24 = 	 Parameter(name='rZP24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP24}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 4] ) 
 
iZP24 = 	 Parameter(name='iZP24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP24}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 4] ) 
 
rZP25 = 	 Parameter(name='rZP25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP25}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 5] ) 
 
iZP25 = 	 Parameter(name='iZP25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP25}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 5] ) 
 
rZP26 = 	 Parameter(name='rZP26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP26}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 6] ) 
 
iZP26 = 	 Parameter(name='iZP26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP26}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 6] ) 
 
rZP27 = 	 Parameter(name='rZP27', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP27}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 7] ) 
 
iZP27 = 	 Parameter(name='iZP27', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP27}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 7] ) 
 
rZP28 = 	 Parameter(name='rZP28', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP28}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [2, 8] ) 
 
iZP28 = 	 Parameter(name='iZP28', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP28}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [2, 8] ) 
 
rZP31 = 	 Parameter(name='rZP31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP31}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 1] ) 
 
iZP31 = 	 Parameter(name='iZP31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP31}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 1] ) 
 
rZP32 = 	 Parameter(name='rZP32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP32}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 2] ) 
 
iZP32 = 	 Parameter(name='iZP32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP32}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 2] ) 
 
rZP33 = 	 Parameter(name='rZP33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP33}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 3] ) 
 
iZP33 = 	 Parameter(name='iZP33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP33}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 3] ) 
 
rZP34 = 	 Parameter(name='rZP34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP34}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 4] ) 
 
iZP34 = 	 Parameter(name='iZP34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP34}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 4] ) 
 
rZP35 = 	 Parameter(name='rZP35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP35}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 5] ) 
 
iZP35 = 	 Parameter(name='iZP35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP35}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 5] ) 
 
rZP36 = 	 Parameter(name='rZP36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP36}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 6] ) 
 
iZP36 = 	 Parameter(name='iZP36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP36}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 6] ) 
 
rZP37 = 	 Parameter(name='rZP37', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP37}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 7] ) 
 
iZP37 = 	 Parameter(name='iZP37', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP37}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 7] ) 
 
rZP38 = 	 Parameter(name='rZP38', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP38}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [3, 8] ) 
 
iZP38 = 	 Parameter(name='iZP38', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP38}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [3, 8] ) 
 
rZP41 = 	 Parameter(name='rZP41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP41}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 1] ) 
 
iZP41 = 	 Parameter(name='iZP41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP41}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 1] ) 
 
rZP42 = 	 Parameter(name='rZP42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP42}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 2] ) 
 
iZP42 = 	 Parameter(name='iZP42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP42}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 2] ) 
 
rZP43 = 	 Parameter(name='rZP43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP43}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 3] ) 
 
iZP43 = 	 Parameter(name='iZP43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP43}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 3] ) 
 
rZP44 = 	 Parameter(name='rZP44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP44}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 4] ) 
 
iZP44 = 	 Parameter(name='iZP44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP44}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 4] ) 
 
rZP45 = 	 Parameter(name='rZP45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP45}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 5] ) 
 
iZP45 = 	 Parameter(name='iZP45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP45}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 5] ) 
 
rZP46 = 	 Parameter(name='rZP46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP46}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 6] ) 
 
iZP46 = 	 Parameter(name='iZP46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP46}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 6] ) 
 
rZP47 = 	 Parameter(name='rZP47', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP47}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 7] ) 
 
iZP47 = 	 Parameter(name='iZP47', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP47}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 7] ) 
 
rZP48 = 	 Parameter(name='rZP48', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP48}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [4, 8] ) 
 
iZP48 = 	 Parameter(name='iZP48', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP48}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [4, 8] ) 
 
rZP51 = 	 Parameter(name='rZP51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP51}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 1] ) 
 
iZP51 = 	 Parameter(name='iZP51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP51}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 1] ) 
 
rZP52 = 	 Parameter(name='rZP52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP52}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 2] ) 
 
iZP52 = 	 Parameter(name='iZP52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP52}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 2] ) 
 
rZP53 = 	 Parameter(name='rZP53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP53}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 3] ) 
 
iZP53 = 	 Parameter(name='iZP53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP53}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 3] ) 
 
rZP54 = 	 Parameter(name='rZP54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP54}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 4] ) 
 
iZP54 = 	 Parameter(name='iZP54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP54}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 4] ) 
 
rZP55 = 	 Parameter(name='rZP55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP55}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 5] ) 
 
iZP55 = 	 Parameter(name='iZP55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP55}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 5] ) 
 
rZP56 = 	 Parameter(name='rZP56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP56}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 6] ) 
 
iZP56 = 	 Parameter(name='iZP56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP56}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 6] ) 
 
rZP57 = 	 Parameter(name='rZP57', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP57}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 7] ) 
 
iZP57 = 	 Parameter(name='iZP57', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP57}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 7] ) 
 
rZP58 = 	 Parameter(name='rZP58', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP58}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [5, 8] ) 
 
iZP58 = 	 Parameter(name='iZP58', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP58}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [5, 8] ) 
 
rZP61 = 	 Parameter(name='rZP61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP61}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 1] ) 
 
iZP61 = 	 Parameter(name='iZP61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP61}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 1] ) 
 
rZP62 = 	 Parameter(name='rZP62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP62}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 2] ) 
 
iZP62 = 	 Parameter(name='iZP62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP62}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 2] ) 
 
rZP63 = 	 Parameter(name='rZP63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP63}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 3] ) 
 
iZP63 = 	 Parameter(name='iZP63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP63}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 3] ) 
 
rZP64 = 	 Parameter(name='rZP64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP64}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 4] ) 
 
iZP64 = 	 Parameter(name='iZP64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP64}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 4] ) 
 
rZP65 = 	 Parameter(name='rZP65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP65}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 5] ) 
 
iZP65 = 	 Parameter(name='iZP65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP65}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 5] ) 
 
rZP66 = 	 Parameter(name='rZP66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP66}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 6] ) 
 
iZP66 = 	 Parameter(name='iZP66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP66}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 6] ) 
 
rZP67 = 	 Parameter(name='rZP67', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP67}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 7] ) 
 
iZP67 = 	 Parameter(name='iZP67', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP67}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 7] ) 
 
rZP68 = 	 Parameter(name='rZP68', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP68}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [6, 8] ) 
 
iZP68 = 	 Parameter(name='iZP68', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP68}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [6, 8] ) 
 
rZP71 = 	 Parameter(name='rZP71', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP71}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 1] ) 
 
iZP71 = 	 Parameter(name='iZP71', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP71}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 1] ) 
 
rZP72 = 	 Parameter(name='rZP72', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP72}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 2] ) 
 
iZP72 = 	 Parameter(name='iZP72', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP72}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 2] ) 
 
rZP73 = 	 Parameter(name='rZP73', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP73}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 3] ) 
 
iZP73 = 	 Parameter(name='iZP73', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP73}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 3] ) 
 
rZP74 = 	 Parameter(name='rZP74', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP74}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 4] ) 
 
iZP74 = 	 Parameter(name='iZP74', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP74}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 4] ) 
 
rZP75 = 	 Parameter(name='rZP75', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP75}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 5] ) 
 
iZP75 = 	 Parameter(name='iZP75', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP75}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 5] ) 
 
rZP76 = 	 Parameter(name='rZP76', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP76}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 6] ) 
 
iZP76 = 	 Parameter(name='iZP76', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP76}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 6] ) 
 
rZP77 = 	 Parameter(name='rZP77', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP77}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 7] ) 
 
iZP77 = 	 Parameter(name='iZP77', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP77}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 7] ) 
 
rZP78 = 	 Parameter(name='rZP78', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP78}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [7, 8] ) 
 
iZP78 = 	 Parameter(name='iZP78', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP78}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [7, 8] ) 
 
rZP81 = 	 Parameter(name='rZP81', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP81}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 1] ) 
 
iZP81 = 	 Parameter(name='iZP81', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP81}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 1] ) 
 
rZP82 = 	 Parameter(name='rZP82', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP82}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 2] ) 
 
iZP82 = 	 Parameter(name='iZP82', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP82}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 2] ) 
 
rZP83 = 	 Parameter(name='rZP83', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP83}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 3] ) 
 
iZP83 = 	 Parameter(name='iZP83', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP83}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 3] ) 
 
rZP84 = 	 Parameter(name='rZP84', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP84}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 4] ) 
 
iZP84 = 	 Parameter(name='iZP84', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP84}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 4] ) 
 
rZP85 = 	 Parameter(name='rZP85', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP85}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 5] ) 
 
iZP85 = 	 Parameter(name='iZP85', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP85}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 5] ) 
 
rZP86 = 	 Parameter(name='rZP86', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP86}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 6] ) 
 
iZP86 = 	 Parameter(name='iZP86', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP86}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 6] ) 
 
rZP87 = 	 Parameter(name='rZP87', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP87}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 7] ) 
 
iZP87 = 	 Parameter(name='iZP87', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP87}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 7] ) 
 
rZP88 = 	 Parameter(name='rZP88', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP88}', 
	 lhablock = 'CHARGEMIX', 
	 lhacode = [8, 8] ) 
 
iZP88 = 	 Parameter(name='iZP88', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP88}', 
	 lhablock = 'IMCHARGEMIX', 
	 lhacode = [8, 8] ) 
 
rUV11 = 	 Parameter(name='rUV11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV11}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 1] ) 
 
iUV11 = 	 Parameter(name='iUV11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV11}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 1] ) 
 
rUV12 = 	 Parameter(name='rUV12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV12}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 2] ) 
 
iUV12 = 	 Parameter(name='iUV12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV12}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 2] ) 
 
rUV13 = 	 Parameter(name='rUV13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV13}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 3] ) 
 
iUV13 = 	 Parameter(name='iUV13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV13}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 3] ) 
 
rUV14 = 	 Parameter(name='rUV14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV14}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 4] ) 
 
iUV14 = 	 Parameter(name='iUV14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV14}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 4] ) 
 
rUV15 = 	 Parameter(name='rUV15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV15}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 5] ) 
 
iUV15 = 	 Parameter(name='iUV15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV15}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 5] ) 
 
rUV16 = 	 Parameter(name='rUV16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV16}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 6] ) 
 
iUV16 = 	 Parameter(name='iUV16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV16}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 6] ) 
 
rUV17 = 	 Parameter(name='rUV17', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV17}', 
	 lhablock = 'UVMIX', 
	 lhacode = [1, 7] ) 
 
iUV17 = 	 Parameter(name='iUV17', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV17}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [1, 7] ) 
 
rUV21 = 	 Parameter(name='rUV21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV21}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 1] ) 
 
iUV21 = 	 Parameter(name='iUV21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV21}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 1] ) 
 
rUV22 = 	 Parameter(name='rUV22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV22}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 2] ) 
 
iUV22 = 	 Parameter(name='iUV22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV22}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 2] ) 
 
rUV23 = 	 Parameter(name='rUV23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV23}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 3] ) 
 
iUV23 = 	 Parameter(name='iUV23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV23}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 3] ) 
 
rUV24 = 	 Parameter(name='rUV24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV24}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 4] ) 
 
iUV24 = 	 Parameter(name='iUV24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV24}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 4] ) 
 
rUV25 = 	 Parameter(name='rUV25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV25}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 5] ) 
 
iUV25 = 	 Parameter(name='iUV25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV25}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 5] ) 
 
rUV26 = 	 Parameter(name='rUV26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV26}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 6] ) 
 
iUV26 = 	 Parameter(name='iUV26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV26}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 6] ) 
 
rUV27 = 	 Parameter(name='rUV27', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV27}', 
	 lhablock = 'UVMIX', 
	 lhacode = [2, 7] ) 
 
iUV27 = 	 Parameter(name='iUV27', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV27}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [2, 7] ) 
 
rUV31 = 	 Parameter(name='rUV31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV31}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 1] ) 
 
iUV31 = 	 Parameter(name='iUV31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV31}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 1] ) 
 
rUV32 = 	 Parameter(name='rUV32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV32}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 2] ) 
 
iUV32 = 	 Parameter(name='iUV32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV32}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 2] ) 
 
rUV33 = 	 Parameter(name='rUV33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV33}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 3] ) 
 
iUV33 = 	 Parameter(name='iUV33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV33}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 3] ) 
 
rUV34 = 	 Parameter(name='rUV34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV34}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 4] ) 
 
iUV34 = 	 Parameter(name='iUV34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV34}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 4] ) 
 
rUV35 = 	 Parameter(name='rUV35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV35}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 5] ) 
 
iUV35 = 	 Parameter(name='iUV35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV35}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 5] ) 
 
rUV36 = 	 Parameter(name='rUV36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV36}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 6] ) 
 
iUV36 = 	 Parameter(name='iUV36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV36}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 6] ) 
 
rUV37 = 	 Parameter(name='rUV37', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV37}', 
	 lhablock = 'UVMIX', 
	 lhacode = [3, 7] ) 
 
iUV37 = 	 Parameter(name='iUV37', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV37}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [3, 7] ) 
 
rUV41 = 	 Parameter(name='rUV41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV41}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 1] ) 
 
iUV41 = 	 Parameter(name='iUV41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV41}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 1] ) 
 
rUV42 = 	 Parameter(name='rUV42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV42}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 2] ) 
 
iUV42 = 	 Parameter(name='iUV42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV42}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 2] ) 
 
rUV43 = 	 Parameter(name='rUV43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV43}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 3] ) 
 
iUV43 = 	 Parameter(name='iUV43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV43}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 3] ) 
 
rUV44 = 	 Parameter(name='rUV44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV44}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 4] ) 
 
iUV44 = 	 Parameter(name='iUV44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV44}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 4] ) 
 
rUV45 = 	 Parameter(name='rUV45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV45}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 5] ) 
 
iUV45 = 	 Parameter(name='iUV45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV45}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 5] ) 
 
rUV46 = 	 Parameter(name='rUV46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV46}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 6] ) 
 
iUV46 = 	 Parameter(name='iUV46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV46}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 6] ) 
 
rUV47 = 	 Parameter(name='rUV47', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV47}', 
	 lhablock = 'UVMIX', 
	 lhacode = [4, 7] ) 
 
iUV47 = 	 Parameter(name='iUV47', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV47}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [4, 7] ) 
 
rUV51 = 	 Parameter(name='rUV51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV51}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 1] ) 
 
iUV51 = 	 Parameter(name='iUV51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV51}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 1] ) 
 
rUV52 = 	 Parameter(name='rUV52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV52}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 2] ) 
 
iUV52 = 	 Parameter(name='iUV52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV52}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 2] ) 
 
rUV53 = 	 Parameter(name='rUV53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV53}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 3] ) 
 
iUV53 = 	 Parameter(name='iUV53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV53}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 3] ) 
 
rUV54 = 	 Parameter(name='rUV54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV54}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 4] ) 
 
iUV54 = 	 Parameter(name='iUV54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV54}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 4] ) 
 
rUV55 = 	 Parameter(name='rUV55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV55}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 5] ) 
 
iUV55 = 	 Parameter(name='iUV55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV55}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 5] ) 
 
rUV56 = 	 Parameter(name='rUV56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV56}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 6] ) 
 
iUV56 = 	 Parameter(name='iUV56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV56}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 6] ) 
 
rUV57 = 	 Parameter(name='rUV57', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV57}', 
	 lhablock = 'UVMIX', 
	 lhacode = [5, 7] ) 
 
iUV57 = 	 Parameter(name='iUV57', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV57}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [5, 7] ) 
 
rUV61 = 	 Parameter(name='rUV61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV61}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 1] ) 
 
iUV61 = 	 Parameter(name='iUV61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV61}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 1] ) 
 
rUV62 = 	 Parameter(name='rUV62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV62}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 2] ) 
 
iUV62 = 	 Parameter(name='iUV62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV62}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 2] ) 
 
rUV63 = 	 Parameter(name='rUV63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV63}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 3] ) 
 
iUV63 = 	 Parameter(name='iUV63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV63}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 3] ) 
 
rUV64 = 	 Parameter(name='rUV64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV64}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 4] ) 
 
iUV64 = 	 Parameter(name='iUV64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV64}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 4] ) 
 
rUV65 = 	 Parameter(name='rUV65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV65}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 5] ) 
 
iUV65 = 	 Parameter(name='iUV65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV65}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 5] ) 
 
rUV66 = 	 Parameter(name='rUV66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV66}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 6] ) 
 
iUV66 = 	 Parameter(name='iUV66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV66}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 6] ) 
 
rUV67 = 	 Parameter(name='rUV67', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV67}', 
	 lhablock = 'UVMIX', 
	 lhacode = [6, 7] ) 
 
iUV67 = 	 Parameter(name='iUV67', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV67}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [6, 7] ) 
 
rUV71 = 	 Parameter(name='rUV71', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV71}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 1] ) 
 
iUV71 = 	 Parameter(name='iUV71', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV71}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 1] ) 
 
rUV72 = 	 Parameter(name='rUV72', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV72}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 2] ) 
 
iUV72 = 	 Parameter(name='iUV72', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV72}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 2] ) 
 
rUV73 = 	 Parameter(name='rUV73', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV73}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 3] ) 
 
iUV73 = 	 Parameter(name='iUV73', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV73}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 3] ) 
 
rUV74 = 	 Parameter(name='rUV74', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV74}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 4] ) 
 
iUV74 = 	 Parameter(name='iUV74', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV74}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 4] ) 
 
rUV75 = 	 Parameter(name='rUV75', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV75}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 5] ) 
 
iUV75 = 	 Parameter(name='iUV75', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV75}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 5] ) 
 
rUV76 = 	 Parameter(name='rUV76', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV76}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 6] ) 
 
iUV76 = 	 Parameter(name='iUV76', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV76}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 6] ) 
 
rUV77 = 	 Parameter(name='rUV77', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV77}', 
	 lhablock = 'UVMIX', 
	 lhacode = [7, 7] ) 
 
iUV77 = 	 Parameter(name='iUV77', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UV77}', 
	 lhablock = 'IMUVMIX', 
	 lhacode = [7, 7] ) 
 
rZEL11 = 	 Parameter(name='rZEL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL11}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 1] ) 
 
iZEL11 = 	 Parameter(name='iZEL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL11}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 1] ) 
 
rZEL12 = 	 Parameter(name='rZEL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL12}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 2] ) 
 
iZEL12 = 	 Parameter(name='iZEL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL12}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 2] ) 
 
rZEL13 = 	 Parameter(name='rZEL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL13}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 3] ) 
 
iZEL13 = 	 Parameter(name='iZEL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL13}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 3] ) 
 
rZEL14 = 	 Parameter(name='rZEL14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL14}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 4] ) 
 
iZEL14 = 	 Parameter(name='iZEL14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL14}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 4] ) 
 
rZEL15 = 	 Parameter(name='rZEL15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL15}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 5] ) 
 
iZEL15 = 	 Parameter(name='iZEL15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL15}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 5] ) 
 
rZEL21 = 	 Parameter(name='rZEL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL21}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 1] ) 
 
iZEL21 = 	 Parameter(name='iZEL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL21}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 1] ) 
 
rZEL22 = 	 Parameter(name='rZEL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL22}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 2] ) 
 
iZEL22 = 	 Parameter(name='iZEL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL22}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 2] ) 
 
rZEL23 = 	 Parameter(name='rZEL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL23}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 3] ) 
 
iZEL23 = 	 Parameter(name='iZEL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL23}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 3] ) 
 
rZEL24 = 	 Parameter(name='rZEL24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL24}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 4] ) 
 
iZEL24 = 	 Parameter(name='iZEL24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL24}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 4] ) 
 
rZEL25 = 	 Parameter(name='rZEL25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL25}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 5] ) 
 
iZEL25 = 	 Parameter(name='iZEL25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL25}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 5] ) 
 
rZEL31 = 	 Parameter(name='rZEL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL31}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 1] ) 
 
iZEL31 = 	 Parameter(name='iZEL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL31}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 1] ) 
 
rZEL32 = 	 Parameter(name='rZEL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL32}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 2] ) 
 
iZEL32 = 	 Parameter(name='iZEL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL32}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 2] ) 
 
rZEL33 = 	 Parameter(name='rZEL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL33}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 3] ) 
 
iZEL33 = 	 Parameter(name='iZEL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL33}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 3] ) 
 
rZEL34 = 	 Parameter(name='rZEL34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL34}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 4] ) 
 
iZEL34 = 	 Parameter(name='iZEL34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL34}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 4] ) 
 
rZEL35 = 	 Parameter(name='rZEL35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL35}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 5] ) 
 
iZEL35 = 	 Parameter(name='iZEL35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL35}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 5] ) 
 
rZEL41 = 	 Parameter(name='rZEL41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL41}', 
	 lhablock = 'UELMIX', 
	 lhacode = [4, 1] ) 
 
iZEL41 = 	 Parameter(name='iZEL41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL41}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [4, 1] ) 
 
rZEL42 = 	 Parameter(name='rZEL42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL42}', 
	 lhablock = 'UELMIX', 
	 lhacode = [4, 2] ) 
 
iZEL42 = 	 Parameter(name='iZEL42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL42}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [4, 2] ) 
 
rZEL43 = 	 Parameter(name='rZEL43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL43}', 
	 lhablock = 'UELMIX', 
	 lhacode = [4, 3] ) 
 
iZEL43 = 	 Parameter(name='iZEL43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL43}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [4, 3] ) 
 
rZEL44 = 	 Parameter(name='rZEL44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL44}', 
	 lhablock = 'UELMIX', 
	 lhacode = [4, 4] ) 
 
iZEL44 = 	 Parameter(name='iZEL44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL44}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [4, 4] ) 
 
rZEL45 = 	 Parameter(name='rZEL45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL45}', 
	 lhablock = 'UELMIX', 
	 lhacode = [4, 5] ) 
 
iZEL45 = 	 Parameter(name='iZEL45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL45}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [4, 5] ) 
 
rZEL51 = 	 Parameter(name='rZEL51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL51}', 
	 lhablock = 'UELMIX', 
	 lhacode = [5, 1] ) 
 
iZEL51 = 	 Parameter(name='iZEL51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL51}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [5, 1] ) 
 
rZEL52 = 	 Parameter(name='rZEL52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL52}', 
	 lhablock = 'UELMIX', 
	 lhacode = [5, 2] ) 
 
iZEL52 = 	 Parameter(name='iZEL52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL52}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [5, 2] ) 
 
rZEL53 = 	 Parameter(name='rZEL53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL53}', 
	 lhablock = 'UELMIX', 
	 lhacode = [5, 3] ) 
 
iZEL53 = 	 Parameter(name='iZEL53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL53}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [5, 3] ) 
 
rZEL54 = 	 Parameter(name='rZEL54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL54}', 
	 lhablock = 'UELMIX', 
	 lhacode = [5, 4] ) 
 
iZEL54 = 	 Parameter(name='iZEL54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL54}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [5, 4] ) 
 
rZEL55 = 	 Parameter(name='rZEL55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL55}', 
	 lhablock = 'UELMIX', 
	 lhacode = [5, 5] ) 
 
iZEL55 = 	 Parameter(name='iZEL55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL55}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [5, 5] ) 
 
rZER11 = 	 Parameter(name='rZER11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER11}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 1] ) 
 
iZER11 = 	 Parameter(name='iZER11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER11}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 1] ) 
 
rZER12 = 	 Parameter(name='rZER12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER12}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 2] ) 
 
iZER12 = 	 Parameter(name='iZER12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER12}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 2] ) 
 
rZER13 = 	 Parameter(name='rZER13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER13}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 3] ) 
 
iZER13 = 	 Parameter(name='iZER13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER13}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 3] ) 
 
rZER14 = 	 Parameter(name='rZER14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER14}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 4] ) 
 
iZER14 = 	 Parameter(name='iZER14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER14}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 4] ) 
 
rZER15 = 	 Parameter(name='rZER15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER15}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 5] ) 
 
iZER15 = 	 Parameter(name='iZER15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER15}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 5] ) 
 
rZER21 = 	 Parameter(name='rZER21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER21}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 1] ) 
 
iZER21 = 	 Parameter(name='iZER21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER21}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 1] ) 
 
rZER22 = 	 Parameter(name='rZER22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER22}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 2] ) 
 
iZER22 = 	 Parameter(name='iZER22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER22}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 2] ) 
 
rZER23 = 	 Parameter(name='rZER23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER23}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 3] ) 
 
iZER23 = 	 Parameter(name='iZER23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER23}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 3] ) 
 
rZER24 = 	 Parameter(name='rZER24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER24}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 4] ) 
 
iZER24 = 	 Parameter(name='iZER24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER24}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 4] ) 
 
rZER25 = 	 Parameter(name='rZER25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER25}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 5] ) 
 
iZER25 = 	 Parameter(name='iZER25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER25}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 5] ) 
 
rZER31 = 	 Parameter(name='rZER31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER31}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 1] ) 
 
iZER31 = 	 Parameter(name='iZER31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER31}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 1] ) 
 
rZER32 = 	 Parameter(name='rZER32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER32}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 2] ) 
 
iZER32 = 	 Parameter(name='iZER32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER32}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 2] ) 
 
rZER33 = 	 Parameter(name='rZER33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER33}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 3] ) 
 
iZER33 = 	 Parameter(name='iZER33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER33}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 3] ) 
 
rZER34 = 	 Parameter(name='rZER34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER34}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 4] ) 
 
iZER34 = 	 Parameter(name='iZER34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER34}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 4] ) 
 
rZER35 = 	 Parameter(name='rZER35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER35}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 5] ) 
 
iZER35 = 	 Parameter(name='iZER35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER35}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 5] ) 
 
rZER41 = 	 Parameter(name='rZER41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER41}', 
	 lhablock = 'UERMIX', 
	 lhacode = [4, 1] ) 
 
iZER41 = 	 Parameter(name='iZER41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER41}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [4, 1] ) 
 
rZER42 = 	 Parameter(name='rZER42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER42}', 
	 lhablock = 'UERMIX', 
	 lhacode = [4, 2] ) 
 
iZER42 = 	 Parameter(name='iZER42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER42}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [4, 2] ) 
 
rZER43 = 	 Parameter(name='rZER43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER43}', 
	 lhablock = 'UERMIX', 
	 lhacode = [4, 3] ) 
 
iZER43 = 	 Parameter(name='iZER43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER43}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [4, 3] ) 
 
rZER44 = 	 Parameter(name='rZER44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER44}', 
	 lhablock = 'UERMIX', 
	 lhacode = [4, 4] ) 
 
iZER44 = 	 Parameter(name='iZER44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER44}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [4, 4] ) 
 
rZER45 = 	 Parameter(name='rZER45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER45}', 
	 lhablock = 'UERMIX', 
	 lhacode = [4, 5] ) 
 
iZER45 = 	 Parameter(name='iZER45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER45}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [4, 5] ) 
 
rZER51 = 	 Parameter(name='rZER51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER51}', 
	 lhablock = 'UERMIX', 
	 lhacode = [5, 1] ) 
 
iZER51 = 	 Parameter(name='iZER51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER51}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [5, 1] ) 
 
rZER52 = 	 Parameter(name='rZER52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER52}', 
	 lhablock = 'UERMIX', 
	 lhacode = [5, 2] ) 
 
iZER52 = 	 Parameter(name='iZER52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER52}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [5, 2] ) 
 
rZER53 = 	 Parameter(name='rZER53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER53}', 
	 lhablock = 'UERMIX', 
	 lhacode = [5, 3] ) 
 
iZER53 = 	 Parameter(name='iZER53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER53}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [5, 3] ) 
 
rZER54 = 	 Parameter(name='rZER54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER54}', 
	 lhablock = 'UERMIX', 
	 lhacode = [5, 4] ) 
 
iZER54 = 	 Parameter(name='iZER54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER54}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [5, 4] ) 
 
rZER55 = 	 Parameter(name='rZER55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER55}', 
	 lhablock = 'UERMIX', 
	 lhacode = [5, 5] ) 
 
iZER55 = 	 Parameter(name='iZER55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER55}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [5, 5] ) 
 
rZDL11 = 	 Parameter(name='rZDL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL11}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 1] ) 
 
iZDL11 = 	 Parameter(name='iZDL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL11}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 1] ) 
 
rZDL12 = 	 Parameter(name='rZDL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL12}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 2] ) 
 
iZDL12 = 	 Parameter(name='iZDL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL12}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 2] ) 
 
rZDL13 = 	 Parameter(name='rZDL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL13}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 3] ) 
 
iZDL13 = 	 Parameter(name='iZDL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL13}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 3] ) 
 
rZDL21 = 	 Parameter(name='rZDL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL21}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 1] ) 
 
iZDL21 = 	 Parameter(name='iZDL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL21}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 1] ) 
 
rZDL22 = 	 Parameter(name='rZDL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL22}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 2] ) 
 
iZDL22 = 	 Parameter(name='iZDL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL22}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 2] ) 
 
rZDL23 = 	 Parameter(name='rZDL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL23}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 3] ) 
 
iZDL23 = 	 Parameter(name='iZDL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL23}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 3] ) 
 
rZDL31 = 	 Parameter(name='rZDL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL31}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 1] ) 
 
iZDL31 = 	 Parameter(name='iZDL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL31}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 1] ) 
 
rZDL32 = 	 Parameter(name='rZDL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL32}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 2] ) 
 
iZDL32 = 	 Parameter(name='iZDL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL32}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 2] ) 
 
rZDL33 = 	 Parameter(name='rZDL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL33}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 3] ) 
 
iZDL33 = 	 Parameter(name='iZDL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL33}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 3] ) 
 
rZDR11 = 	 Parameter(name='rZDR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR11}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 1] ) 
 
iZDR11 = 	 Parameter(name='iZDR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR11}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 1] ) 
 
rZDR12 = 	 Parameter(name='rZDR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR12}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 2] ) 
 
iZDR12 = 	 Parameter(name='iZDR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR12}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 2] ) 
 
rZDR13 = 	 Parameter(name='rZDR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR13}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 3] ) 
 
iZDR13 = 	 Parameter(name='iZDR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR13}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 3] ) 
 
rZDR21 = 	 Parameter(name='rZDR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR21}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 1] ) 
 
iZDR21 = 	 Parameter(name='iZDR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR21}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 1] ) 
 
rZDR22 = 	 Parameter(name='rZDR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR22}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 2] ) 
 
iZDR22 = 	 Parameter(name='iZDR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR22}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 2] ) 
 
rZDR23 = 	 Parameter(name='rZDR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR23}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 3] ) 
 
iZDR23 = 	 Parameter(name='iZDR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR23}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 3] ) 
 
rZDR31 = 	 Parameter(name='rZDR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR31}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 1] ) 
 
iZDR31 = 	 Parameter(name='iZDR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR31}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 1] ) 
 
rZDR32 = 	 Parameter(name='rZDR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR32}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 2] ) 
 
iZDR32 = 	 Parameter(name='iZDR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR32}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 2] ) 
 
rZDR33 = 	 Parameter(name='rZDR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR33}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 3] ) 
 
iZDR33 = 	 Parameter(name='iZDR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR33}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 3] ) 
 
rZUL11 = 	 Parameter(name='rZUL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL11}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 1] ) 
 
iZUL11 = 	 Parameter(name='iZUL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL11}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 1] ) 
 
rZUL12 = 	 Parameter(name='rZUL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL12}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 2] ) 
 
iZUL12 = 	 Parameter(name='iZUL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL12}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 2] ) 
 
rZUL13 = 	 Parameter(name='rZUL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL13}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 3] ) 
 
iZUL13 = 	 Parameter(name='iZUL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL13}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 3] ) 
 
rZUL21 = 	 Parameter(name='rZUL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL21}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 1] ) 
 
iZUL21 = 	 Parameter(name='iZUL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL21}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 1] ) 
 
rZUL22 = 	 Parameter(name='rZUL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL22}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 2] ) 
 
iZUL22 = 	 Parameter(name='iZUL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL22}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 2] ) 
 
rZUL23 = 	 Parameter(name='rZUL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL23}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 3] ) 
 
iZUL23 = 	 Parameter(name='iZUL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL23}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 3] ) 
 
rZUL31 = 	 Parameter(name='rZUL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL31}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 1] ) 
 
iZUL31 = 	 Parameter(name='iZUL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL31}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 1] ) 
 
rZUL32 = 	 Parameter(name='rZUL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL32}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 2] ) 
 
iZUL32 = 	 Parameter(name='iZUL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL32}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 2] ) 
 
rZUL33 = 	 Parameter(name='rZUL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL33}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 3] ) 
 
iZUL33 = 	 Parameter(name='iZUL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL33}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 3] ) 
 
rZUR11 = 	 Parameter(name='rZUR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR11}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 1] ) 
 
iZUR11 = 	 Parameter(name='iZUR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR11}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 1] ) 
 
rZUR12 = 	 Parameter(name='rZUR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR12}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 2] ) 
 
iZUR12 = 	 Parameter(name='iZUR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR12}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 2] ) 
 
rZUR13 = 	 Parameter(name='rZUR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR13}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 3] ) 
 
iZUR13 = 	 Parameter(name='iZUR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR13}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 3] ) 
 
rZUR21 = 	 Parameter(name='rZUR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR21}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 1] ) 
 
iZUR21 = 	 Parameter(name='iZUR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR21}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 1] ) 
 
rZUR22 = 	 Parameter(name='rZUR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR22}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 2] ) 
 
iZUR22 = 	 Parameter(name='iZUR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR22}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 2] ) 
 
rZUR23 = 	 Parameter(name='rZUR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR23}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 3] ) 
 
iZUR23 = 	 Parameter(name='iZUR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR23}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 3] ) 
 
rZUR31 = 	 Parameter(name='rZUR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR31}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 1] ) 
 
iZUR31 = 	 Parameter(name='iZUR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR31}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 1] ) 
 
rZUR32 = 	 Parameter(name='rZUR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR32}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 2] ) 
 
iZUR32 = 	 Parameter(name='iZUR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR32}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 2] ) 
 
rZUR33 = 	 Parameter(name='rZUR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR33}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 3] ) 
 
iZUR33 = 	 Parameter(name='iZUR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR33}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 3] ) 
 
aS = 	 Parameter(name='aS', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.119, 
	 texname = '\\text{aS}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [3] ) 
 
aEWM1 = 	 Parameter(name='aEWM1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 137.035999679, 
	 texname = '\\text{aEWM1}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [1] ) 
 
Gf = 	 Parameter(name='Gf', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0000116639, 
	 texname = '\\text{Gf}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [2] ) 
 
Mu = 	 Parameter(name='Mu', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rMu + complex(0,1)*iMu', 
	 texname = '\\text{Mu}' ) 
 
REps1 = 	 Parameter(name='REps1', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rREps1 + complex(0,1)*iREps1', 
	 texname = '\\text{REps1}' ) 
 
REps2 = 	 Parameter(name='REps2', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rREps2 + complex(0,1)*iREps2', 
	 texname = '\\text{REps2}' ) 
 
REps3 = 	 Parameter(name='REps3', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rREps3 + complex(0,1)*iREps3', 
	 texname = '\\text{REps3}' ) 
 
Yd11 = 	 Parameter(name='Yd11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd11 + complex(0,1)*iYd11', 
	 texname = '\\text{Yd11}' ) 
 
Yd12 = 	 Parameter(name='Yd12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd12 + complex(0,1)*iYd12', 
	 texname = '\\text{Yd12}' ) 
 
Yd13 = 	 Parameter(name='Yd13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd13 + complex(0,1)*iYd13', 
	 texname = '\\text{Yd13}' ) 
 
Yd21 = 	 Parameter(name='Yd21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd21 + complex(0,1)*iYd21', 
	 texname = '\\text{Yd21}' ) 
 
Yd22 = 	 Parameter(name='Yd22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd22 + complex(0,1)*iYd22', 
	 texname = '\\text{Yd22}' ) 
 
Yd23 = 	 Parameter(name='Yd23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd23 + complex(0,1)*iYd23', 
	 texname = '\\text{Yd23}' ) 
 
Yd31 = 	 Parameter(name='Yd31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd31 + complex(0,1)*iYd31', 
	 texname = '\\text{Yd31}' ) 
 
Yd32 = 	 Parameter(name='Yd32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd32 + complex(0,1)*iYd32', 
	 texname = '\\text{Yd32}' ) 
 
Yd33 = 	 Parameter(name='Yd33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd33 + complex(0,1)*iYd33', 
	 texname = '\\text{Yd33}' ) 
 
Td11 = 	 Parameter(name='Td11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd11 + complex(0,1)*iTd11', 
	 texname = '\\text{Td11}' ) 
 
Td12 = 	 Parameter(name='Td12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd12 + complex(0,1)*iTd12', 
	 texname = '\\text{Td12}' ) 
 
Td13 = 	 Parameter(name='Td13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd13 + complex(0,1)*iTd13', 
	 texname = '\\text{Td13}' ) 
 
Td21 = 	 Parameter(name='Td21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd21 + complex(0,1)*iTd21', 
	 texname = '\\text{Td21}' ) 
 
Td22 = 	 Parameter(name='Td22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd22 + complex(0,1)*iTd22', 
	 texname = '\\text{Td22}' ) 
 
Td23 = 	 Parameter(name='Td23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd23 + complex(0,1)*iTd23', 
	 texname = '\\text{Td23}' ) 
 
Td31 = 	 Parameter(name='Td31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd31 + complex(0,1)*iTd31', 
	 texname = '\\text{Td31}' ) 
 
Td32 = 	 Parameter(name='Td32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd32 + complex(0,1)*iTd32', 
	 texname = '\\text{Td32}' ) 
 
Td33 = 	 Parameter(name='Td33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd33 + complex(0,1)*iTd33', 
	 texname = '\\text{Td33}' ) 
 
Te11 = 	 Parameter(name='Te11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe11 + complex(0,1)*iTe11', 
	 texname = '\\text{Te11}' ) 
 
Te12 = 	 Parameter(name='Te12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe12 + complex(0,1)*iTe12', 
	 texname = '\\text{Te12}' ) 
 
Te13 = 	 Parameter(name='Te13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe13 + complex(0,1)*iTe13', 
	 texname = '\\text{Te13}' ) 
 
Te21 = 	 Parameter(name='Te21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe21 + complex(0,1)*iTe21', 
	 texname = '\\text{Te21}' ) 
 
Te22 = 	 Parameter(name='Te22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe22 + complex(0,1)*iTe22', 
	 texname = '\\text{Te22}' ) 
 
Te23 = 	 Parameter(name='Te23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe23 + complex(0,1)*iTe23', 
	 texname = '\\text{Te23}' ) 
 
Te31 = 	 Parameter(name='Te31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe31 + complex(0,1)*iTe31', 
	 texname = '\\text{Te31}' ) 
 
Te32 = 	 Parameter(name='Te32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe32 + complex(0,1)*iTe32', 
	 texname = '\\text{Te32}' ) 
 
Te33 = 	 Parameter(name='Te33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe33 + complex(0,1)*iTe33', 
	 texname = '\\text{Te33}' ) 
 
Yu11 = 	 Parameter(name='Yu11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu11 + complex(0,1)*iYu11', 
	 texname = '\\text{Yu11}' ) 
 
Yu12 = 	 Parameter(name='Yu12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu12 + complex(0,1)*iYu12', 
	 texname = '\\text{Yu12}' ) 
 
Yu13 = 	 Parameter(name='Yu13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu13 + complex(0,1)*iYu13', 
	 texname = '\\text{Yu13}' ) 
 
Yu21 = 	 Parameter(name='Yu21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu21 + complex(0,1)*iYu21', 
	 texname = '\\text{Yu21}' ) 
 
Yu22 = 	 Parameter(name='Yu22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu22 + complex(0,1)*iYu22', 
	 texname = '\\text{Yu22}' ) 
 
Yu23 = 	 Parameter(name='Yu23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu23 + complex(0,1)*iYu23', 
	 texname = '\\text{Yu23}' ) 
 
Yu31 = 	 Parameter(name='Yu31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu31 + complex(0,1)*iYu31', 
	 texname = '\\text{Yu31}' ) 
 
Yu32 = 	 Parameter(name='Yu32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu32 + complex(0,1)*iYu32', 
	 texname = '\\text{Yu32}' ) 
 
Yu33 = 	 Parameter(name='Yu33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu33 + complex(0,1)*iYu33', 
	 texname = '\\text{Yu33}' ) 
 
Tu11 = 	 Parameter(name='Tu11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu11 + complex(0,1)*iTu11', 
	 texname = '\\text{Tu11}' ) 
 
Tu12 = 	 Parameter(name='Tu12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu12 + complex(0,1)*iTu12', 
	 texname = '\\text{Tu12}' ) 
 
Tu13 = 	 Parameter(name='Tu13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu13 + complex(0,1)*iTu13', 
	 texname = '\\text{Tu13}' ) 
 
Tu21 = 	 Parameter(name='Tu21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu21 + complex(0,1)*iTu21', 
	 texname = '\\text{Tu21}' ) 
 
Tu22 = 	 Parameter(name='Tu22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu22 + complex(0,1)*iTu22', 
	 texname = '\\text{Tu22}' ) 
 
Tu23 = 	 Parameter(name='Tu23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu23 + complex(0,1)*iTu23', 
	 texname = '\\text{Tu23}' ) 
 
Tu31 = 	 Parameter(name='Tu31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu31 + complex(0,1)*iTu31', 
	 texname = '\\text{Tu31}' ) 
 
Tu32 = 	 Parameter(name='Tu32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu32 + complex(0,1)*iTu32', 
	 texname = '\\text{Tu32}' ) 
 
Tu33 = 	 Parameter(name='Tu33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu33 + complex(0,1)*iTu33', 
	 texname = '\\text{Tu33}' ) 
 
pG = 	 Parameter(name='pG', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rpG + complex(0,1)*ipG', 
	 texname = '\\text{pG}' ) 
 
ZD11 = 	 Parameter(name='ZD11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD11 + complex(0,1)*iZD11', 
	 texname = '\\text{ZD11}' ) 
 
ZD12 = 	 Parameter(name='ZD12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD12 + complex(0,1)*iZD12', 
	 texname = '\\text{ZD12}' ) 
 
ZD13 = 	 Parameter(name='ZD13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD13 + complex(0,1)*iZD13', 
	 texname = '\\text{ZD13}' ) 
 
ZD14 = 	 Parameter(name='ZD14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD14 + complex(0,1)*iZD14', 
	 texname = '\\text{ZD14}' ) 
 
ZD15 = 	 Parameter(name='ZD15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD15 + complex(0,1)*iZD15', 
	 texname = '\\text{ZD15}' ) 
 
ZD16 = 	 Parameter(name='ZD16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD16 + complex(0,1)*iZD16', 
	 texname = '\\text{ZD16}' ) 
 
ZD21 = 	 Parameter(name='ZD21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD21 + complex(0,1)*iZD21', 
	 texname = '\\text{ZD21}' ) 
 
ZD22 = 	 Parameter(name='ZD22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD22 + complex(0,1)*iZD22', 
	 texname = '\\text{ZD22}' ) 
 
ZD23 = 	 Parameter(name='ZD23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD23 + complex(0,1)*iZD23', 
	 texname = '\\text{ZD23}' ) 
 
ZD24 = 	 Parameter(name='ZD24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD24 + complex(0,1)*iZD24', 
	 texname = '\\text{ZD24}' ) 
 
ZD25 = 	 Parameter(name='ZD25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD25 + complex(0,1)*iZD25', 
	 texname = '\\text{ZD25}' ) 
 
ZD26 = 	 Parameter(name='ZD26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD26 + complex(0,1)*iZD26', 
	 texname = '\\text{ZD26}' ) 
 
ZD31 = 	 Parameter(name='ZD31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD31 + complex(0,1)*iZD31', 
	 texname = '\\text{ZD31}' ) 
 
ZD32 = 	 Parameter(name='ZD32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD32 + complex(0,1)*iZD32', 
	 texname = '\\text{ZD32}' ) 
 
ZD33 = 	 Parameter(name='ZD33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD33 + complex(0,1)*iZD33', 
	 texname = '\\text{ZD33}' ) 
 
ZD34 = 	 Parameter(name='ZD34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD34 + complex(0,1)*iZD34', 
	 texname = '\\text{ZD34}' ) 
 
ZD35 = 	 Parameter(name='ZD35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD35 + complex(0,1)*iZD35', 
	 texname = '\\text{ZD35}' ) 
 
ZD36 = 	 Parameter(name='ZD36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD36 + complex(0,1)*iZD36', 
	 texname = '\\text{ZD36}' ) 
 
ZD41 = 	 Parameter(name='ZD41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD41 + complex(0,1)*iZD41', 
	 texname = '\\text{ZD41}' ) 
 
ZD42 = 	 Parameter(name='ZD42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD42 + complex(0,1)*iZD42', 
	 texname = '\\text{ZD42}' ) 
 
ZD43 = 	 Parameter(name='ZD43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD43 + complex(0,1)*iZD43', 
	 texname = '\\text{ZD43}' ) 
 
ZD44 = 	 Parameter(name='ZD44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD44 + complex(0,1)*iZD44', 
	 texname = '\\text{ZD44}' ) 
 
ZD45 = 	 Parameter(name='ZD45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD45 + complex(0,1)*iZD45', 
	 texname = '\\text{ZD45}' ) 
 
ZD46 = 	 Parameter(name='ZD46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD46 + complex(0,1)*iZD46', 
	 texname = '\\text{ZD46}' ) 
 
ZD51 = 	 Parameter(name='ZD51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD51 + complex(0,1)*iZD51', 
	 texname = '\\text{ZD51}' ) 
 
ZD52 = 	 Parameter(name='ZD52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD52 + complex(0,1)*iZD52', 
	 texname = '\\text{ZD52}' ) 
 
ZD53 = 	 Parameter(name='ZD53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD53 + complex(0,1)*iZD53', 
	 texname = '\\text{ZD53}' ) 
 
ZD54 = 	 Parameter(name='ZD54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD54 + complex(0,1)*iZD54', 
	 texname = '\\text{ZD54}' ) 
 
ZD55 = 	 Parameter(name='ZD55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD55 + complex(0,1)*iZD55', 
	 texname = '\\text{ZD55}' ) 
 
ZD56 = 	 Parameter(name='ZD56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD56 + complex(0,1)*iZD56', 
	 texname = '\\text{ZD56}' ) 
 
ZD61 = 	 Parameter(name='ZD61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD61 + complex(0,1)*iZD61', 
	 texname = '\\text{ZD61}' ) 
 
ZD62 = 	 Parameter(name='ZD62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD62 + complex(0,1)*iZD62', 
	 texname = '\\text{ZD62}' ) 
 
ZD63 = 	 Parameter(name='ZD63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD63 + complex(0,1)*iZD63', 
	 texname = '\\text{ZD63}' ) 
 
ZD64 = 	 Parameter(name='ZD64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD64 + complex(0,1)*iZD64', 
	 texname = '\\text{ZD64}' ) 
 
ZD65 = 	 Parameter(name='ZD65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD65 + complex(0,1)*iZD65', 
	 texname = '\\text{ZD65}' ) 
 
ZD66 = 	 Parameter(name='ZD66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD66 + complex(0,1)*iZD66', 
	 texname = '\\text{ZD66}' ) 
 
ZU11 = 	 Parameter(name='ZU11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU11 + complex(0,1)*iZU11', 
	 texname = '\\text{ZU11}' ) 
 
ZU12 = 	 Parameter(name='ZU12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU12 + complex(0,1)*iZU12', 
	 texname = '\\text{ZU12}' ) 
 
ZU13 = 	 Parameter(name='ZU13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU13 + complex(0,1)*iZU13', 
	 texname = '\\text{ZU13}' ) 
 
ZU14 = 	 Parameter(name='ZU14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU14 + complex(0,1)*iZU14', 
	 texname = '\\text{ZU14}' ) 
 
ZU15 = 	 Parameter(name='ZU15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU15 + complex(0,1)*iZU15', 
	 texname = '\\text{ZU15}' ) 
 
ZU16 = 	 Parameter(name='ZU16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU16 + complex(0,1)*iZU16', 
	 texname = '\\text{ZU16}' ) 
 
ZU21 = 	 Parameter(name='ZU21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU21 + complex(0,1)*iZU21', 
	 texname = '\\text{ZU21}' ) 
 
ZU22 = 	 Parameter(name='ZU22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU22 + complex(0,1)*iZU22', 
	 texname = '\\text{ZU22}' ) 
 
ZU23 = 	 Parameter(name='ZU23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU23 + complex(0,1)*iZU23', 
	 texname = '\\text{ZU23}' ) 
 
ZU24 = 	 Parameter(name='ZU24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU24 + complex(0,1)*iZU24', 
	 texname = '\\text{ZU24}' ) 
 
ZU25 = 	 Parameter(name='ZU25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU25 + complex(0,1)*iZU25', 
	 texname = '\\text{ZU25}' ) 
 
ZU26 = 	 Parameter(name='ZU26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU26 + complex(0,1)*iZU26', 
	 texname = '\\text{ZU26}' ) 
 
ZU31 = 	 Parameter(name='ZU31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU31 + complex(0,1)*iZU31', 
	 texname = '\\text{ZU31}' ) 
 
ZU32 = 	 Parameter(name='ZU32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU32 + complex(0,1)*iZU32', 
	 texname = '\\text{ZU32}' ) 
 
ZU33 = 	 Parameter(name='ZU33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU33 + complex(0,1)*iZU33', 
	 texname = '\\text{ZU33}' ) 
 
ZU34 = 	 Parameter(name='ZU34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU34 + complex(0,1)*iZU34', 
	 texname = '\\text{ZU34}' ) 
 
ZU35 = 	 Parameter(name='ZU35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU35 + complex(0,1)*iZU35', 
	 texname = '\\text{ZU35}' ) 
 
ZU36 = 	 Parameter(name='ZU36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU36 + complex(0,1)*iZU36', 
	 texname = '\\text{ZU36}' ) 
 
ZU41 = 	 Parameter(name='ZU41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU41 + complex(0,1)*iZU41', 
	 texname = '\\text{ZU41}' ) 
 
ZU42 = 	 Parameter(name='ZU42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU42 + complex(0,1)*iZU42', 
	 texname = '\\text{ZU42}' ) 
 
ZU43 = 	 Parameter(name='ZU43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU43 + complex(0,1)*iZU43', 
	 texname = '\\text{ZU43}' ) 
 
ZU44 = 	 Parameter(name='ZU44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU44 + complex(0,1)*iZU44', 
	 texname = '\\text{ZU44}' ) 
 
ZU45 = 	 Parameter(name='ZU45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU45 + complex(0,1)*iZU45', 
	 texname = '\\text{ZU45}' ) 
 
ZU46 = 	 Parameter(name='ZU46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU46 + complex(0,1)*iZU46', 
	 texname = '\\text{ZU46}' ) 
 
ZU51 = 	 Parameter(name='ZU51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU51 + complex(0,1)*iZU51', 
	 texname = '\\text{ZU51}' ) 
 
ZU52 = 	 Parameter(name='ZU52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU52 + complex(0,1)*iZU52', 
	 texname = '\\text{ZU52}' ) 
 
ZU53 = 	 Parameter(name='ZU53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU53 + complex(0,1)*iZU53', 
	 texname = '\\text{ZU53}' ) 
 
ZU54 = 	 Parameter(name='ZU54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU54 + complex(0,1)*iZU54', 
	 texname = '\\text{ZU54}' ) 
 
ZU55 = 	 Parameter(name='ZU55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU55 + complex(0,1)*iZU55', 
	 texname = '\\text{ZU55}' ) 
 
ZU56 = 	 Parameter(name='ZU56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU56 + complex(0,1)*iZU56', 
	 texname = '\\text{ZU56}' ) 
 
ZU61 = 	 Parameter(name='ZU61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU61 + complex(0,1)*iZU61', 
	 texname = '\\text{ZU61}' ) 
 
ZU62 = 	 Parameter(name='ZU62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU62 + complex(0,1)*iZU62', 
	 texname = '\\text{ZU62}' ) 
 
ZU63 = 	 Parameter(name='ZU63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU63 + complex(0,1)*iZU63', 
	 texname = '\\text{ZU63}' ) 
 
ZU64 = 	 Parameter(name='ZU64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU64 + complex(0,1)*iZU64', 
	 texname = '\\text{ZU64}' ) 
 
ZU65 = 	 Parameter(name='ZU65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU65 + complex(0,1)*iZU65', 
	 texname = '\\text{ZU65}' ) 
 
ZU66 = 	 Parameter(name='ZU66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU66 + complex(0,1)*iZU66', 
	 texname = '\\text{ZU66}' ) 
 
ZP11 = 	 Parameter(name='ZP11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP11 + complex(0,1)*iZP11', 
	 texname = '\\text{ZP11}' ) 
 
ZP12 = 	 Parameter(name='ZP12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP12 + complex(0,1)*iZP12', 
	 texname = '\\text{ZP12}' ) 
 
ZP13 = 	 Parameter(name='ZP13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP13 + complex(0,1)*iZP13', 
	 texname = '\\text{ZP13}' ) 
 
ZP14 = 	 Parameter(name='ZP14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP14 + complex(0,1)*iZP14', 
	 texname = '\\text{ZP14}' ) 
 
ZP15 = 	 Parameter(name='ZP15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP15 + complex(0,1)*iZP15', 
	 texname = '\\text{ZP15}' ) 
 
ZP16 = 	 Parameter(name='ZP16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP16 + complex(0,1)*iZP16', 
	 texname = '\\text{ZP16}' ) 
 
ZP17 = 	 Parameter(name='ZP17', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP17 + complex(0,1)*iZP17', 
	 texname = '\\text{ZP17}' ) 
 
ZP18 = 	 Parameter(name='ZP18', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP18 + complex(0,1)*iZP18', 
	 texname = '\\text{ZP18}' ) 
 
ZP21 = 	 Parameter(name='ZP21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP21 + complex(0,1)*iZP21', 
	 texname = '\\text{ZP21}' ) 
 
ZP22 = 	 Parameter(name='ZP22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP22 + complex(0,1)*iZP22', 
	 texname = '\\text{ZP22}' ) 
 
ZP23 = 	 Parameter(name='ZP23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP23 + complex(0,1)*iZP23', 
	 texname = '\\text{ZP23}' ) 
 
ZP24 = 	 Parameter(name='ZP24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP24 + complex(0,1)*iZP24', 
	 texname = '\\text{ZP24}' ) 
 
ZP25 = 	 Parameter(name='ZP25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP25 + complex(0,1)*iZP25', 
	 texname = '\\text{ZP25}' ) 
 
ZP26 = 	 Parameter(name='ZP26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP26 + complex(0,1)*iZP26', 
	 texname = '\\text{ZP26}' ) 
 
ZP27 = 	 Parameter(name='ZP27', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP27 + complex(0,1)*iZP27', 
	 texname = '\\text{ZP27}' ) 
 
ZP28 = 	 Parameter(name='ZP28', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP28 + complex(0,1)*iZP28', 
	 texname = '\\text{ZP28}' ) 
 
ZP31 = 	 Parameter(name='ZP31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP31 + complex(0,1)*iZP31', 
	 texname = '\\text{ZP31}' ) 
 
ZP32 = 	 Parameter(name='ZP32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP32 + complex(0,1)*iZP32', 
	 texname = '\\text{ZP32}' ) 
 
ZP33 = 	 Parameter(name='ZP33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP33 + complex(0,1)*iZP33', 
	 texname = '\\text{ZP33}' ) 
 
ZP34 = 	 Parameter(name='ZP34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP34 + complex(0,1)*iZP34', 
	 texname = '\\text{ZP34}' ) 
 
ZP35 = 	 Parameter(name='ZP35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP35 + complex(0,1)*iZP35', 
	 texname = '\\text{ZP35}' ) 
 
ZP36 = 	 Parameter(name='ZP36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP36 + complex(0,1)*iZP36', 
	 texname = '\\text{ZP36}' ) 
 
ZP37 = 	 Parameter(name='ZP37', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP37 + complex(0,1)*iZP37', 
	 texname = '\\text{ZP37}' ) 
 
ZP38 = 	 Parameter(name='ZP38', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP38 + complex(0,1)*iZP38', 
	 texname = '\\text{ZP38}' ) 
 
ZP41 = 	 Parameter(name='ZP41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP41 + complex(0,1)*iZP41', 
	 texname = '\\text{ZP41}' ) 
 
ZP42 = 	 Parameter(name='ZP42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP42 + complex(0,1)*iZP42', 
	 texname = '\\text{ZP42}' ) 
 
ZP43 = 	 Parameter(name='ZP43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP43 + complex(0,1)*iZP43', 
	 texname = '\\text{ZP43}' ) 
 
ZP44 = 	 Parameter(name='ZP44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP44 + complex(0,1)*iZP44', 
	 texname = '\\text{ZP44}' ) 
 
ZP45 = 	 Parameter(name='ZP45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP45 + complex(0,1)*iZP45', 
	 texname = '\\text{ZP45}' ) 
 
ZP46 = 	 Parameter(name='ZP46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP46 + complex(0,1)*iZP46', 
	 texname = '\\text{ZP46}' ) 
 
ZP47 = 	 Parameter(name='ZP47', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP47 + complex(0,1)*iZP47', 
	 texname = '\\text{ZP47}' ) 
 
ZP48 = 	 Parameter(name='ZP48', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP48 + complex(0,1)*iZP48', 
	 texname = '\\text{ZP48}' ) 
 
ZP51 = 	 Parameter(name='ZP51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP51 + complex(0,1)*iZP51', 
	 texname = '\\text{ZP51}' ) 
 
ZP52 = 	 Parameter(name='ZP52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP52 + complex(0,1)*iZP52', 
	 texname = '\\text{ZP52}' ) 
 
ZP53 = 	 Parameter(name='ZP53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP53 + complex(0,1)*iZP53', 
	 texname = '\\text{ZP53}' ) 
 
ZP54 = 	 Parameter(name='ZP54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP54 + complex(0,1)*iZP54', 
	 texname = '\\text{ZP54}' ) 
 
ZP55 = 	 Parameter(name='ZP55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP55 + complex(0,1)*iZP55', 
	 texname = '\\text{ZP55}' ) 
 
ZP56 = 	 Parameter(name='ZP56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP56 + complex(0,1)*iZP56', 
	 texname = '\\text{ZP56}' ) 
 
ZP57 = 	 Parameter(name='ZP57', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP57 + complex(0,1)*iZP57', 
	 texname = '\\text{ZP57}' ) 
 
ZP58 = 	 Parameter(name='ZP58', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP58 + complex(0,1)*iZP58', 
	 texname = '\\text{ZP58}' ) 
 
ZP61 = 	 Parameter(name='ZP61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP61 + complex(0,1)*iZP61', 
	 texname = '\\text{ZP61}' ) 
 
ZP62 = 	 Parameter(name='ZP62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP62 + complex(0,1)*iZP62', 
	 texname = '\\text{ZP62}' ) 
 
ZP63 = 	 Parameter(name='ZP63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP63 + complex(0,1)*iZP63', 
	 texname = '\\text{ZP63}' ) 
 
ZP64 = 	 Parameter(name='ZP64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP64 + complex(0,1)*iZP64', 
	 texname = '\\text{ZP64}' ) 
 
ZP65 = 	 Parameter(name='ZP65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP65 + complex(0,1)*iZP65', 
	 texname = '\\text{ZP65}' ) 
 
ZP66 = 	 Parameter(name='ZP66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP66 + complex(0,1)*iZP66', 
	 texname = '\\text{ZP66}' ) 
 
ZP67 = 	 Parameter(name='ZP67', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP67 + complex(0,1)*iZP67', 
	 texname = '\\text{ZP67}' ) 
 
ZP68 = 	 Parameter(name='ZP68', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP68 + complex(0,1)*iZP68', 
	 texname = '\\text{ZP68}' ) 
 
ZP71 = 	 Parameter(name='ZP71', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP71 + complex(0,1)*iZP71', 
	 texname = '\\text{ZP71}' ) 
 
ZP72 = 	 Parameter(name='ZP72', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP72 + complex(0,1)*iZP72', 
	 texname = '\\text{ZP72}' ) 
 
ZP73 = 	 Parameter(name='ZP73', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP73 + complex(0,1)*iZP73', 
	 texname = '\\text{ZP73}' ) 
 
ZP74 = 	 Parameter(name='ZP74', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP74 + complex(0,1)*iZP74', 
	 texname = '\\text{ZP74}' ) 
 
ZP75 = 	 Parameter(name='ZP75', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP75 + complex(0,1)*iZP75', 
	 texname = '\\text{ZP75}' ) 
 
ZP76 = 	 Parameter(name='ZP76', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP76 + complex(0,1)*iZP76', 
	 texname = '\\text{ZP76}' ) 
 
ZP77 = 	 Parameter(name='ZP77', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP77 + complex(0,1)*iZP77', 
	 texname = '\\text{ZP77}' ) 
 
ZP78 = 	 Parameter(name='ZP78', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP78 + complex(0,1)*iZP78', 
	 texname = '\\text{ZP78}' ) 
 
ZP81 = 	 Parameter(name='ZP81', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP81 + complex(0,1)*iZP81', 
	 texname = '\\text{ZP81}' ) 
 
ZP82 = 	 Parameter(name='ZP82', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP82 + complex(0,1)*iZP82', 
	 texname = '\\text{ZP82}' ) 
 
ZP83 = 	 Parameter(name='ZP83', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP83 + complex(0,1)*iZP83', 
	 texname = '\\text{ZP83}' ) 
 
ZP84 = 	 Parameter(name='ZP84', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP84 + complex(0,1)*iZP84', 
	 texname = '\\text{ZP84}' ) 
 
ZP85 = 	 Parameter(name='ZP85', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP85 + complex(0,1)*iZP85', 
	 texname = '\\text{ZP85}' ) 
 
ZP86 = 	 Parameter(name='ZP86', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP86 + complex(0,1)*iZP86', 
	 texname = '\\text{ZP86}' ) 
 
ZP87 = 	 Parameter(name='ZP87', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP87 + complex(0,1)*iZP87', 
	 texname = '\\text{ZP87}' ) 
 
ZP88 = 	 Parameter(name='ZP88', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP88 + complex(0,1)*iZP88', 
	 texname = '\\text{ZP88}' ) 
 
UV11 = 	 Parameter(name='UV11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV11 + complex(0,1)*iUV11', 
	 texname = '\\text{UV11}' ) 
 
UV12 = 	 Parameter(name='UV12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV12 + complex(0,1)*iUV12', 
	 texname = '\\text{UV12}' ) 
 
UV13 = 	 Parameter(name='UV13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV13 + complex(0,1)*iUV13', 
	 texname = '\\text{UV13}' ) 
 
UV14 = 	 Parameter(name='UV14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV14 + complex(0,1)*iUV14', 
	 texname = '\\text{UV14}' ) 
 
UV15 = 	 Parameter(name='UV15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV15 + complex(0,1)*iUV15', 
	 texname = '\\text{UV15}' ) 
 
UV16 = 	 Parameter(name='UV16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV16 + complex(0,1)*iUV16', 
	 texname = '\\text{UV16}' ) 
 
UV17 = 	 Parameter(name='UV17', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV17 + complex(0,1)*iUV17', 
	 texname = '\\text{UV17}' ) 
 
UV21 = 	 Parameter(name='UV21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV21 + complex(0,1)*iUV21', 
	 texname = '\\text{UV21}' ) 
 
UV22 = 	 Parameter(name='UV22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV22 + complex(0,1)*iUV22', 
	 texname = '\\text{UV22}' ) 
 
UV23 = 	 Parameter(name='UV23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV23 + complex(0,1)*iUV23', 
	 texname = '\\text{UV23}' ) 
 
UV24 = 	 Parameter(name='UV24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV24 + complex(0,1)*iUV24', 
	 texname = '\\text{UV24}' ) 
 
UV25 = 	 Parameter(name='UV25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV25 + complex(0,1)*iUV25', 
	 texname = '\\text{UV25}' ) 
 
UV26 = 	 Parameter(name='UV26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV26 + complex(0,1)*iUV26', 
	 texname = '\\text{UV26}' ) 
 
UV27 = 	 Parameter(name='UV27', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV27 + complex(0,1)*iUV27', 
	 texname = '\\text{UV27}' ) 
 
UV31 = 	 Parameter(name='UV31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV31 + complex(0,1)*iUV31', 
	 texname = '\\text{UV31}' ) 
 
UV32 = 	 Parameter(name='UV32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV32 + complex(0,1)*iUV32', 
	 texname = '\\text{UV32}' ) 
 
UV33 = 	 Parameter(name='UV33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV33 + complex(0,1)*iUV33', 
	 texname = '\\text{UV33}' ) 
 
UV34 = 	 Parameter(name='UV34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV34 + complex(0,1)*iUV34', 
	 texname = '\\text{UV34}' ) 
 
UV35 = 	 Parameter(name='UV35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV35 + complex(0,1)*iUV35', 
	 texname = '\\text{UV35}' ) 
 
UV36 = 	 Parameter(name='UV36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV36 + complex(0,1)*iUV36', 
	 texname = '\\text{UV36}' ) 
 
UV37 = 	 Parameter(name='UV37', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV37 + complex(0,1)*iUV37', 
	 texname = '\\text{UV37}' ) 
 
UV41 = 	 Parameter(name='UV41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV41 + complex(0,1)*iUV41', 
	 texname = '\\text{UV41}' ) 
 
UV42 = 	 Parameter(name='UV42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV42 + complex(0,1)*iUV42', 
	 texname = '\\text{UV42}' ) 
 
UV43 = 	 Parameter(name='UV43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV43 + complex(0,1)*iUV43', 
	 texname = '\\text{UV43}' ) 
 
UV44 = 	 Parameter(name='UV44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV44 + complex(0,1)*iUV44', 
	 texname = '\\text{UV44}' ) 
 
UV45 = 	 Parameter(name='UV45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV45 + complex(0,1)*iUV45', 
	 texname = '\\text{UV45}' ) 
 
UV46 = 	 Parameter(name='UV46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV46 + complex(0,1)*iUV46', 
	 texname = '\\text{UV46}' ) 
 
UV47 = 	 Parameter(name='UV47', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV47 + complex(0,1)*iUV47', 
	 texname = '\\text{UV47}' ) 
 
UV51 = 	 Parameter(name='UV51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV51 + complex(0,1)*iUV51', 
	 texname = '\\text{UV51}' ) 
 
UV52 = 	 Parameter(name='UV52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV52 + complex(0,1)*iUV52', 
	 texname = '\\text{UV52}' ) 
 
UV53 = 	 Parameter(name='UV53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV53 + complex(0,1)*iUV53', 
	 texname = '\\text{UV53}' ) 
 
UV54 = 	 Parameter(name='UV54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV54 + complex(0,1)*iUV54', 
	 texname = '\\text{UV54}' ) 
 
UV55 = 	 Parameter(name='UV55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV55 + complex(0,1)*iUV55', 
	 texname = '\\text{UV55}' ) 
 
UV56 = 	 Parameter(name='UV56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV56 + complex(0,1)*iUV56', 
	 texname = '\\text{UV56}' ) 
 
UV57 = 	 Parameter(name='UV57', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV57 + complex(0,1)*iUV57', 
	 texname = '\\text{UV57}' ) 
 
UV61 = 	 Parameter(name='UV61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV61 + complex(0,1)*iUV61', 
	 texname = '\\text{UV61}' ) 
 
UV62 = 	 Parameter(name='UV62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV62 + complex(0,1)*iUV62', 
	 texname = '\\text{UV62}' ) 
 
UV63 = 	 Parameter(name='UV63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV63 + complex(0,1)*iUV63', 
	 texname = '\\text{UV63}' ) 
 
UV64 = 	 Parameter(name='UV64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV64 + complex(0,1)*iUV64', 
	 texname = '\\text{UV64}' ) 
 
UV65 = 	 Parameter(name='UV65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV65 + complex(0,1)*iUV65', 
	 texname = '\\text{UV65}' ) 
 
UV66 = 	 Parameter(name='UV66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV66 + complex(0,1)*iUV66', 
	 texname = '\\text{UV66}' ) 
 
UV67 = 	 Parameter(name='UV67', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV67 + complex(0,1)*iUV67', 
	 texname = '\\text{UV67}' ) 
 
UV71 = 	 Parameter(name='UV71', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV71 + complex(0,1)*iUV71', 
	 texname = '\\text{UV71}' ) 
 
UV72 = 	 Parameter(name='UV72', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV72 + complex(0,1)*iUV72', 
	 texname = '\\text{UV72}' ) 
 
UV73 = 	 Parameter(name='UV73', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV73 + complex(0,1)*iUV73', 
	 texname = '\\text{UV73}' ) 
 
UV74 = 	 Parameter(name='UV74', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV74 + complex(0,1)*iUV74', 
	 texname = '\\text{UV74}' ) 
 
UV75 = 	 Parameter(name='UV75', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV75 + complex(0,1)*iUV75', 
	 texname = '\\text{UV75}' ) 
 
UV76 = 	 Parameter(name='UV76', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV76 + complex(0,1)*iUV76', 
	 texname = '\\text{UV76}' ) 
 
UV77 = 	 Parameter(name='UV77', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUV77 + complex(0,1)*iUV77', 
	 texname = '\\text{UV77}' ) 
 
ZEL11 = 	 Parameter(name='ZEL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL11 + complex(0,1)*iZEL11', 
	 texname = '\\text{ZEL11}' ) 
 
ZEL12 = 	 Parameter(name='ZEL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL12 + complex(0,1)*iZEL12', 
	 texname = '\\text{ZEL12}' ) 
 
ZEL13 = 	 Parameter(name='ZEL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL13 + complex(0,1)*iZEL13', 
	 texname = '\\text{ZEL13}' ) 
 
ZEL14 = 	 Parameter(name='ZEL14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL14 + complex(0,1)*iZEL14', 
	 texname = '\\text{ZEL14}' ) 
 
ZEL15 = 	 Parameter(name='ZEL15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL15 + complex(0,1)*iZEL15', 
	 texname = '\\text{ZEL15}' ) 
 
ZEL21 = 	 Parameter(name='ZEL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL21 + complex(0,1)*iZEL21', 
	 texname = '\\text{ZEL21}' ) 
 
ZEL22 = 	 Parameter(name='ZEL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL22 + complex(0,1)*iZEL22', 
	 texname = '\\text{ZEL22}' ) 
 
ZEL23 = 	 Parameter(name='ZEL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL23 + complex(0,1)*iZEL23', 
	 texname = '\\text{ZEL23}' ) 
 
ZEL24 = 	 Parameter(name='ZEL24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL24 + complex(0,1)*iZEL24', 
	 texname = '\\text{ZEL24}' ) 
 
ZEL25 = 	 Parameter(name='ZEL25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL25 + complex(0,1)*iZEL25', 
	 texname = '\\text{ZEL25}' ) 
 
ZEL31 = 	 Parameter(name='ZEL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL31 + complex(0,1)*iZEL31', 
	 texname = '\\text{ZEL31}' ) 
 
ZEL32 = 	 Parameter(name='ZEL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL32 + complex(0,1)*iZEL32', 
	 texname = '\\text{ZEL32}' ) 
 
ZEL33 = 	 Parameter(name='ZEL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL33 + complex(0,1)*iZEL33', 
	 texname = '\\text{ZEL33}' ) 
 
ZEL34 = 	 Parameter(name='ZEL34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL34 + complex(0,1)*iZEL34', 
	 texname = '\\text{ZEL34}' ) 
 
ZEL35 = 	 Parameter(name='ZEL35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL35 + complex(0,1)*iZEL35', 
	 texname = '\\text{ZEL35}' ) 
 
ZEL41 = 	 Parameter(name='ZEL41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL41 + complex(0,1)*iZEL41', 
	 texname = '\\text{ZEL41}' ) 
 
ZEL42 = 	 Parameter(name='ZEL42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL42 + complex(0,1)*iZEL42', 
	 texname = '\\text{ZEL42}' ) 
 
ZEL43 = 	 Parameter(name='ZEL43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL43 + complex(0,1)*iZEL43', 
	 texname = '\\text{ZEL43}' ) 
 
ZEL44 = 	 Parameter(name='ZEL44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL44 + complex(0,1)*iZEL44', 
	 texname = '\\text{ZEL44}' ) 
 
ZEL45 = 	 Parameter(name='ZEL45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL45 + complex(0,1)*iZEL45', 
	 texname = '\\text{ZEL45}' ) 
 
ZEL51 = 	 Parameter(name='ZEL51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL51 + complex(0,1)*iZEL51', 
	 texname = '\\text{ZEL51}' ) 
 
ZEL52 = 	 Parameter(name='ZEL52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL52 + complex(0,1)*iZEL52', 
	 texname = '\\text{ZEL52}' ) 
 
ZEL53 = 	 Parameter(name='ZEL53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL53 + complex(0,1)*iZEL53', 
	 texname = '\\text{ZEL53}' ) 
 
ZEL54 = 	 Parameter(name='ZEL54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL54 + complex(0,1)*iZEL54', 
	 texname = '\\text{ZEL54}' ) 
 
ZEL55 = 	 Parameter(name='ZEL55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL55 + complex(0,1)*iZEL55', 
	 texname = '\\text{ZEL55}' ) 
 
ZER11 = 	 Parameter(name='ZER11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER11 + complex(0,1)*iZER11', 
	 texname = '\\text{ZER11}' ) 
 
ZER12 = 	 Parameter(name='ZER12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER12 + complex(0,1)*iZER12', 
	 texname = '\\text{ZER12}' ) 
 
ZER13 = 	 Parameter(name='ZER13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER13 + complex(0,1)*iZER13', 
	 texname = '\\text{ZER13}' ) 
 
ZER14 = 	 Parameter(name='ZER14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER14 + complex(0,1)*iZER14', 
	 texname = '\\text{ZER14}' ) 
 
ZER15 = 	 Parameter(name='ZER15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER15 + complex(0,1)*iZER15', 
	 texname = '\\text{ZER15}' ) 
 
ZER21 = 	 Parameter(name='ZER21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER21 + complex(0,1)*iZER21', 
	 texname = '\\text{ZER21}' ) 
 
ZER22 = 	 Parameter(name='ZER22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER22 + complex(0,1)*iZER22', 
	 texname = '\\text{ZER22}' ) 
 
ZER23 = 	 Parameter(name='ZER23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER23 + complex(0,1)*iZER23', 
	 texname = '\\text{ZER23}' ) 
 
ZER24 = 	 Parameter(name='ZER24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER24 + complex(0,1)*iZER24', 
	 texname = '\\text{ZER24}' ) 
 
ZER25 = 	 Parameter(name='ZER25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER25 + complex(0,1)*iZER25', 
	 texname = '\\text{ZER25}' ) 
 
ZER31 = 	 Parameter(name='ZER31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER31 + complex(0,1)*iZER31', 
	 texname = '\\text{ZER31}' ) 
 
ZER32 = 	 Parameter(name='ZER32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER32 + complex(0,1)*iZER32', 
	 texname = '\\text{ZER32}' ) 
 
ZER33 = 	 Parameter(name='ZER33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER33 + complex(0,1)*iZER33', 
	 texname = '\\text{ZER33}' ) 
 
ZER34 = 	 Parameter(name='ZER34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER34 + complex(0,1)*iZER34', 
	 texname = '\\text{ZER34}' ) 
 
ZER35 = 	 Parameter(name='ZER35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER35 + complex(0,1)*iZER35', 
	 texname = '\\text{ZER35}' ) 
 
ZER41 = 	 Parameter(name='ZER41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER41 + complex(0,1)*iZER41', 
	 texname = '\\text{ZER41}' ) 
 
ZER42 = 	 Parameter(name='ZER42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER42 + complex(0,1)*iZER42', 
	 texname = '\\text{ZER42}' ) 
 
ZER43 = 	 Parameter(name='ZER43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER43 + complex(0,1)*iZER43', 
	 texname = '\\text{ZER43}' ) 
 
ZER44 = 	 Parameter(name='ZER44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER44 + complex(0,1)*iZER44', 
	 texname = '\\text{ZER44}' ) 
 
ZER45 = 	 Parameter(name='ZER45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER45 + complex(0,1)*iZER45', 
	 texname = '\\text{ZER45}' ) 
 
ZER51 = 	 Parameter(name='ZER51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER51 + complex(0,1)*iZER51', 
	 texname = '\\text{ZER51}' ) 
 
ZER52 = 	 Parameter(name='ZER52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER52 + complex(0,1)*iZER52', 
	 texname = '\\text{ZER52}' ) 
 
ZER53 = 	 Parameter(name='ZER53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER53 + complex(0,1)*iZER53', 
	 texname = '\\text{ZER53}' ) 
 
ZER54 = 	 Parameter(name='ZER54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER54 + complex(0,1)*iZER54', 
	 texname = '\\text{ZER54}' ) 
 
ZER55 = 	 Parameter(name='ZER55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER55 + complex(0,1)*iZER55', 
	 texname = '\\text{ZER55}' ) 
 
ZDL11 = 	 Parameter(name='ZDL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL11 + complex(0,1)*iZDL11', 
	 texname = '\\text{ZDL11}' ) 
 
ZDL12 = 	 Parameter(name='ZDL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL12 + complex(0,1)*iZDL12', 
	 texname = '\\text{ZDL12}' ) 
 
ZDL13 = 	 Parameter(name='ZDL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL13 + complex(0,1)*iZDL13', 
	 texname = '\\text{ZDL13}' ) 
 
ZDL21 = 	 Parameter(name='ZDL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL21 + complex(0,1)*iZDL21', 
	 texname = '\\text{ZDL21}' ) 
 
ZDL22 = 	 Parameter(name='ZDL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL22 + complex(0,1)*iZDL22', 
	 texname = '\\text{ZDL22}' ) 
 
ZDL23 = 	 Parameter(name='ZDL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL23 + complex(0,1)*iZDL23', 
	 texname = '\\text{ZDL23}' ) 
 
ZDL31 = 	 Parameter(name='ZDL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL31 + complex(0,1)*iZDL31', 
	 texname = '\\text{ZDL31}' ) 
 
ZDL32 = 	 Parameter(name='ZDL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL32 + complex(0,1)*iZDL32', 
	 texname = '\\text{ZDL32}' ) 
 
ZDL33 = 	 Parameter(name='ZDL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL33 + complex(0,1)*iZDL33', 
	 texname = '\\text{ZDL33}' ) 
 
ZDR11 = 	 Parameter(name='ZDR11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR11 + complex(0,1)*iZDR11', 
	 texname = '\\text{ZDR11}' ) 
 
ZDR12 = 	 Parameter(name='ZDR12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR12 + complex(0,1)*iZDR12', 
	 texname = '\\text{ZDR12}' ) 
 
ZDR13 = 	 Parameter(name='ZDR13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR13 + complex(0,1)*iZDR13', 
	 texname = '\\text{ZDR13}' ) 
 
ZDR21 = 	 Parameter(name='ZDR21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR21 + complex(0,1)*iZDR21', 
	 texname = '\\text{ZDR21}' ) 
 
ZDR22 = 	 Parameter(name='ZDR22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR22 + complex(0,1)*iZDR22', 
	 texname = '\\text{ZDR22}' ) 
 
ZDR23 = 	 Parameter(name='ZDR23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR23 + complex(0,1)*iZDR23', 
	 texname = '\\text{ZDR23}' ) 
 
ZDR31 = 	 Parameter(name='ZDR31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR31 + complex(0,1)*iZDR31', 
	 texname = '\\text{ZDR31}' ) 
 
ZDR32 = 	 Parameter(name='ZDR32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR32 + complex(0,1)*iZDR32', 
	 texname = '\\text{ZDR32}' ) 
 
ZDR33 = 	 Parameter(name='ZDR33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR33 + complex(0,1)*iZDR33', 
	 texname = '\\text{ZDR33}' ) 
 
ZUL11 = 	 Parameter(name='ZUL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL11 + complex(0,1)*iZUL11', 
	 texname = '\\text{ZUL11}' ) 
 
ZUL12 = 	 Parameter(name='ZUL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL12 + complex(0,1)*iZUL12', 
	 texname = '\\text{ZUL12}' ) 
 
ZUL13 = 	 Parameter(name='ZUL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL13 + complex(0,1)*iZUL13', 
	 texname = '\\text{ZUL13}' ) 
 
ZUL21 = 	 Parameter(name='ZUL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL21 + complex(0,1)*iZUL21', 
	 texname = '\\text{ZUL21}' ) 
 
ZUL22 = 	 Parameter(name='ZUL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL22 + complex(0,1)*iZUL22', 
	 texname = '\\text{ZUL22}' ) 
 
ZUL23 = 	 Parameter(name='ZUL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL23 + complex(0,1)*iZUL23', 
	 texname = '\\text{ZUL23}' ) 
 
ZUL31 = 	 Parameter(name='ZUL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL31 + complex(0,1)*iZUL31', 
	 texname = '\\text{ZUL31}' ) 
 
ZUL32 = 	 Parameter(name='ZUL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL32 + complex(0,1)*iZUL32', 
	 texname = '\\text{ZUL32}' ) 
 
ZUL33 = 	 Parameter(name='ZUL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL33 + complex(0,1)*iZUL33', 
	 texname = '\\text{ZUL33}' ) 
 
ZUR11 = 	 Parameter(name='ZUR11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR11 + complex(0,1)*iZUR11', 
	 texname = '\\text{ZUR11}' ) 
 
ZUR12 = 	 Parameter(name='ZUR12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR12 + complex(0,1)*iZUR12', 
	 texname = '\\text{ZUR12}' ) 
 
ZUR13 = 	 Parameter(name='ZUR13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR13 + complex(0,1)*iZUR13', 
	 texname = '\\text{ZUR13}' ) 
 
ZUR21 = 	 Parameter(name='ZUR21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR21 + complex(0,1)*iZUR21', 
	 texname = '\\text{ZUR21}' ) 
 
ZUR22 = 	 Parameter(name='ZUR22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR22 + complex(0,1)*iZUR22', 
	 texname = '\\text{ZUR22}' ) 
 
ZUR23 = 	 Parameter(name='ZUR23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR23 + complex(0,1)*iZUR23', 
	 texname = '\\text{ZUR23}' ) 
 
ZUR31 = 	 Parameter(name='ZUR31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR31 + complex(0,1)*iZUR31', 
	 texname = '\\text{ZUR31}' ) 
 
ZUR32 = 	 Parameter(name='ZUR32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR32 + complex(0,1)*iZUR32', 
	 texname = '\\text{ZUR32}' ) 
 
ZUR33 = 	 Parameter(name='ZUR33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR33 + complex(0,1)*iZUR33', 
	 texname = '\\text{ZUR33}' ) 
 
G = 	 Parameter(name='G', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)', 
	 texname = 'G') 
 
Ye11 = 	 Parameter(name='Ye11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '(cmath.sqrt(2)*Me1)/vd', 
	 texname = 'Ye11') 
 
Ye12 = 	 Parameter(name='Ye12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '1.*(0)', 
	 texname = 'Ye12') 
 
Ye13 = 	 Parameter(name='Ye13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '1.*(0)', 
	 texname = 'Ye13') 
 
Ye21 = 	 Parameter(name='Ye21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '1.*(0)', 
	 texname = 'Ye21') 
 
Ye22 = 	 Parameter(name='Ye22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '(cmath.sqrt(2)*Me2)/vd', 
	 texname = 'Ye22') 
 
Ye23 = 	 Parameter(name='Ye23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '1.*(0)', 
	 texname = 'Ye23') 
 
Ye31 = 	 Parameter(name='Ye31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '1.*(0)', 
	 texname = 'Ye31') 
 
Ye32 = 	 Parameter(name='Ye32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '1.*(0)', 
	 texname = 'Ye32') 
 
Ye33 = 	 Parameter(name='Ye33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = '(cmath.sqrt(2)*Me3)/vd', 
	 texname = 'Ye33') 
 
el = 	 Parameter(name='el', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(1/aEWM1)*cmath.sqrt(cmath.pi)', 
	 texname = 'el') 
 
MWm = 	 Parameter(name='MWm', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (MZ**2*cmath.pi)/(cmath.sqrt(2)*aEWM1*Gf)))', 
	 texname = 'MWm') 
 
TW = 	 Parameter(name='TW', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.asin(cmath.sqrt(1 - MWm**2/MZ**2))', 
	 texname = 'TW') 
 
g1 = 	 Parameter(name='g1', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'el*1./cmath.cos(TW)', 
	 texname = 'g1') 
 
g2 = 	 Parameter(name='g2', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'el*1./cmath.sin(TW)', 
	 texname = 'g2') 
 
RXiWm = 	 Parameter(name='RXiWm', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiWm') 
 
RXiZ = 	 Parameter(name='RXiZ', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiZ') 
 
