# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 12.0.0 for Mac OS X x86 (64-bit) (April 7, 2019)
# Date: Tue 15 Oct 2019 22:22:58


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(Ah*complex(0,1))',
                order = {'HIW':1})

GC_2 = Coupling(name = 'GC_2',
                value = '-(AHH*complex(0,1))',
                order = {'HIW':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'ee*(0. + 0.3333333333333333*complex(0,1))',
                order = {'QED':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee*(0. - 0.6666666666666666*complex(0,1))',
                order = {'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = 'gAvlqb0/cmath.sqrt(2)',
                 order = {'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(gAvlqbl/cmath.sqrt(2))',
                 order = {'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'gAvlqbl/cmath.sqrt(2)',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(gAvlqbr/cmath.sqrt(2))',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'gAvlqbr/cmath.sqrt(2)',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'gAvlqt0/cmath.sqrt(2)',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '-(gAvlqtl/cmath.sqrt(2))',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = 'gAvlqtl/cmath.sqrt(2)',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-(gAvlqtr/cmath.sqrt(2))',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = 'gAvlqtr/cmath.sqrt(2)',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'complex(0,1)*gdmzl',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = 'complex(0,1)*gdmzr',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'complex(0,1)*gdwl',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-((complex(0,1)*gdyuk1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-((complex(0,1)*gdyuk2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'complex(0,1)*gdzl',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'complex(0,1)*gdzr',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = 'complex(0,1)*gemzl',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'complex(0,1)*gemzr',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = 'complex(0,1)*gewl',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '-((complex(0,1)*geyuk1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-((complex(0,1)*geyuk2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = 'complex(0,1)*gezl',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = 'complex(0,1)*gezr',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-(complex(0,1)*Gh)',
                 order = {'HIG':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '-(G*Gh)',
                 order = {'HIG':1,'QCD':1})

GC_39 = Coupling(name = 'GC_39',
                 value = 'complex(0,1)*G**2*Gh',
                 order = {'HIG':1,'QCD':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '-((complex(0,1)*gh2vlqb0)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-((complex(0,1)*gh2vlqbl)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-((complex(0,1)*gh2vlqbr)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-((complex(0,1)*gh2vlqt0)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-((complex(0,1)*gh2vlqtl)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-((complex(0,1)*gh2vlqtr)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-((complex(0,1)*ghdl1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-((complex(0,1)*ghdl2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-((complex(0,1)*ghdr1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-((complex(0,1)*ghdr2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '-((complex(0,1)*ghel1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-((complex(0,1)*ghel2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '-((complex(0,1)*gher1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '-((complex(0,1)*gher2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(complex(0,1)*GHH)',
                 order = {'HIG':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '-(G*GHH)',
                 order = {'HIG':1,'QCD':1})

GC_56 = Coupling(name = 'GC_56',
                 value = 'complex(0,1)*G**2*GHH',
                 order = {'HIG':1,'QCD':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '-2*complex(0,1)*gHhh*cmath.sqrt(2)',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-((complex(0,1)*ghmu1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '-((complex(0,1)*ghmu2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-((complex(0,1)*gHmvlqbl)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-((complex(0,1)*gHmvlqtr)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-((complex(0,1)*gHpvlqbr)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '-((complex(0,1)*gHpvlqtl)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-((complex(0,1)*ghvlqb0)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-((complex(0,1)*ghvlqbl)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-((complex(0,1)*ghvlqbr)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '-((complex(0,1)*ghvlqt0)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '-((complex(0,1)*ghvlqtl)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '-((complex(0,1)*ghvlqtr)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-((complex(0,1)*gnhr1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '-((complex(0,1)*gnhr2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = 'complex(0,1)*gnnwl',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = 'complex(0,1)*gnnwr',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-(complex(0,1)*gnvzl)',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(complex(0,1)*gnvzr)',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = 'complex(0,1)*gnwl',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = 'complex(0,1)*gnwr',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '-((complex(0,1)*gnyuk1)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-((complex(0,1)*gnyuk2)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = 'complex(0,1)*gnzl',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(complex(0,1)*Gphi)/8.',
                 order = {'HIG':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(G*Gphi)/4.',
                 order = {'HIG':1,'QCD':1})

GC_83 = Coupling(name = 'GC_83',
                 value = 'complex(0,1)*gwvlq0l',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = 'complex(0,1)*gwvlq0r',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = 'complex(0,1)*gwvlqbl',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = 'complex(0,1)*gwvlqbr',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = 'complex(0,1)*gwvlqtl',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = 'complex(0,1)*gwvlqtr',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = 'complex(0,1)*gzvlqb0l',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = 'complex(0,1)*gzvlqb0r',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = 'complex(0,1)*gzvlqbl',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = 'complex(0,1)*gzvlqbr',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = 'complex(0,1)*gzvlqt0l',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = 'complex(0,1)*gzvlqt0r',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = 'complex(0,1)*gzvlqtl',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = 'complex(0,1)*gzvlqtr',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = 'complex(0,1)*gawmu + (ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_99 = Coupling(name = 'GC_99',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '(-2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = 'complex(0,1)*I1a11*cmath.cos(beta)',
                  order = {'QED':1})

GC_130 = Coupling(name = 'GC_130',
                  value = 'complex(0,1)*I1a12*cmath.cos(beta)',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = 'complex(0,1)*I1a13*cmath.cos(beta)',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = 'complex(0,1)*I1a21*cmath.cos(beta)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = 'complex(0,1)*I1a22*cmath.cos(beta)',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = 'complex(0,1)*I1a23*cmath.cos(beta)',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = 'complex(0,1)*I1a31*cmath.cos(beta)',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = 'complex(0,1)*I1a32*cmath.cos(beta)',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = 'complex(0,1)*I1a33*cmath.cos(beta)',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-(complex(0,1)*I2a11*cmath.cos(beta))',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '-(complex(0,1)*I2a12*cmath.cos(beta))',
                  order = {'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(complex(0,1)*I2a13*cmath.cos(beta))',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-(complex(0,1)*I2a21*cmath.cos(beta))',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(complex(0,1)*I2a22*cmath.cos(beta))',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '-(complex(0,1)*I2a23*cmath.cos(beta))',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(complex(0,1)*I2a31*cmath.cos(beta))',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '-(complex(0,1)*I2a32*cmath.cos(beta))',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(complex(0,1)*I2a33*cmath.cos(beta))',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '-(complex(0,1)*I3a11*cmath.cos(beta))',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(complex(0,1)*I3a12*cmath.cos(beta))',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '-(complex(0,1)*I3a13*cmath.cos(beta))',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-(complex(0,1)*I3a21*cmath.cos(beta))',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '-(complex(0,1)*I3a22*cmath.cos(beta))',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(complex(0,1)*I3a23*cmath.cos(beta))',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '-(complex(0,1)*I3a31*cmath.cos(beta))',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(complex(0,1)*I3a32*cmath.cos(beta))',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '-(complex(0,1)*I3a33*cmath.cos(beta))',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = 'complex(0,1)*I4a11*cmath.cos(beta)',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = 'complex(0,1)*I4a12*cmath.cos(beta)',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = 'complex(0,1)*I4a13*cmath.cos(beta)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = 'complex(0,1)*I4a21*cmath.cos(beta)',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = 'complex(0,1)*I4a22*cmath.cos(beta)',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = 'complex(0,1)*I4a23*cmath.cos(beta)',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = 'complex(0,1)*I4a31*cmath.cos(beta)',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = 'complex(0,1)*I4a32*cmath.cos(beta)',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = 'complex(0,1)*I4a33*cmath.cos(beta)',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-((yb*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '-((complex(0,1)*yb*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '(yb*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '-((yc*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(complex(0,1)*yc*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '(yc*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '-((ydo*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-((complex(0,1)*ydo*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '(ydo*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = 'complex(0,1)*ye*cmath.cos(beta)',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-((ye*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '-((complex(0,1)*ye*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '(ye*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = 'complex(0,1)*ym*cmath.cos(beta)',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-((ym*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-((complex(0,1)*ym*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '(ym*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '-((ys*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-((complex(0,1)*ys*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '(ys*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-((yt*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '(complex(0,1)*yt*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '(yt*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = 'complex(0,1)*ytau*cmath.cos(beta)',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-((ytau*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-((complex(0,1)*ytau*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '(ytau*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '-((yup*cmath.cos(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(complex(0,1)*yup*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '(yup*cmath.cos(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-2*complex(0,1)*lam*vd*cmath.cos(beta)**3*cmath.sqrt(2)',
                  order = {'QED':2})

GC_196 = Coupling(name = 'GC_196',
                  value = '-6*complex(0,1)*lam*vd*cmath.cos(beta)**3*cmath.sqrt(2)',
                  order = {'QED':2})

GC_197 = Coupling(name = 'GC_197',
                  value = '-2*complex(0,1)*lam*cmath.cos(beta)**4',
                  order = {'QED':2})

GC_198 = Coupling(name = 'GC_198',
                  value = '-4*complex(0,1)*lam*cmath.cos(beta)**4',
                  order = {'QED':2})

GC_199 = Coupling(name = 'GC_199',
                  value = '-6*complex(0,1)*lam*cmath.cos(beta)**4',
                  order = {'QED':2})

GC_200 = Coupling(name = 'GC_200',
                  value = '-(complex(0,1)*I1a11*cmath.sin(beta))',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(complex(0,1)*I1a12*cmath.sin(beta))',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(complex(0,1)*I1a13*cmath.sin(beta))',
                  order = {'QED':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '-(complex(0,1)*I1a21*cmath.sin(beta))',
                  order = {'QED':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '-(complex(0,1)*I1a22*cmath.sin(beta))',
                  order = {'QED':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '-(complex(0,1)*I1a23*cmath.sin(beta))',
                  order = {'QED':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '-(complex(0,1)*I1a31*cmath.sin(beta))',
                  order = {'QED':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-(complex(0,1)*I1a32*cmath.sin(beta))',
                  order = {'QED':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '-(complex(0,1)*I1a33*cmath.sin(beta))',
                  order = {'QED':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '-(complex(0,1)*I2a11*cmath.sin(beta))',
                  order = {'QED':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '-(complex(0,1)*I2a12*cmath.sin(beta))',
                  order = {'QED':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-(complex(0,1)*I2a13*cmath.sin(beta))',
                  order = {'QED':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '-(complex(0,1)*I2a21*cmath.sin(beta))',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '-(complex(0,1)*I2a22*cmath.sin(beta))',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '-(complex(0,1)*I2a23*cmath.sin(beta))',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '-(complex(0,1)*I2a31*cmath.sin(beta))',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '-(complex(0,1)*I2a32*cmath.sin(beta))',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '-(complex(0,1)*I2a33*cmath.sin(beta))',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-(complex(0,1)*I3a11*cmath.sin(beta))',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '-(complex(0,1)*I3a12*cmath.sin(beta))',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '-(complex(0,1)*I3a13*cmath.sin(beta))',
                  order = {'QED':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '-(complex(0,1)*I3a21*cmath.sin(beta))',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '-(complex(0,1)*I3a22*cmath.sin(beta))',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '-(complex(0,1)*I3a23*cmath.sin(beta))',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '-(complex(0,1)*I3a31*cmath.sin(beta))',
                  order = {'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '-(complex(0,1)*I3a32*cmath.sin(beta))',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '-(complex(0,1)*I3a33*cmath.sin(beta))',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '-(complex(0,1)*I4a11*cmath.sin(beta))',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '-(complex(0,1)*I4a12*cmath.sin(beta))',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '-(complex(0,1)*I4a13*cmath.sin(beta))',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '-(complex(0,1)*I4a21*cmath.sin(beta))',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '-(complex(0,1)*I4a22*cmath.sin(beta))',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '-(complex(0,1)*I4a23*cmath.sin(beta))',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '-(complex(0,1)*I4a31*cmath.sin(beta))',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '-(complex(0,1)*I4a32*cmath.sin(beta))',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-(complex(0,1)*I4a33*cmath.sin(beta))',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '-((yb*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '-((complex(0,1)*yb*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(yb*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '-((yc*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '-((complex(0,1)*yc*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_241 = Coupling(name = 'GC_241',
                  value = '(yc*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '-((ydo*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '-((complex(0,1)*ydo*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '(ydo*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '-(complex(0,1)*ye*cmath.sin(beta))',
                  order = {'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '-((ye*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '-((complex(0,1)*ye*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '(ye*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '-(complex(0,1)*ym*cmath.sin(beta))',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-((ym*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '-((complex(0,1)*ym*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '(ym*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '-((ys*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '-((complex(0,1)*ys*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(ys*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '-((yt*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '-((complex(0,1)*yt*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '(yt*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '-(complex(0,1)*ytau*cmath.sin(beta))',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '-((ytau*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '-((complex(0,1)*ytau*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '(ytau*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '-((yup*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '-((complex(0,1)*yup*cmath.sin(beta))/cmath.sqrt(2))',
                  order = {'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '(yup*cmath.sin(beta))/cmath.sqrt(2)',
                  order = {'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '-2*complex(0,1)*lam*vd*cmath.cos(beta)**2*cmath.sqrt(2)*cmath.sin(beta)',
                  order = {'QED':2})

GC_267 = Coupling(name = 'GC_267',
                  value = '2*complex(0,1)*lam*vd*cmath.cos(beta)**2*cmath.sqrt(2)*cmath.sin(beta)',
                  order = {'QED':2})

GC_268 = Coupling(name = 'GC_268',
                  value = '-6*complex(0,1)*lam*vd*cmath.cos(beta)**2*cmath.sqrt(2)*cmath.sin(beta)',
                  order = {'QED':2})

GC_269 = Coupling(name = 'GC_269',
                  value = '-2*complex(0,1)*lam*cmath.cos(beta)**3*cmath.sin(beta)',
                  order = {'QED':2})

GC_270 = Coupling(name = 'GC_270',
                  value = '2*complex(0,1)*lam*cmath.cos(beta)**3*cmath.sin(beta)',
                  order = {'QED':2})

GC_271 = Coupling(name = 'GC_271',
                  value = '4*complex(0,1)*lam*cmath.cos(beta)**3*cmath.sin(beta)',
                  order = {'QED':2})

GC_272 = Coupling(name = 'GC_272',
                  value = '-6*complex(0,1)*lam*cmath.cos(beta)**3*cmath.sin(beta)',
                  order = {'QED':2})

GC_273 = Coupling(name = 'GC_273',
                  value = '6*complex(0,1)*lam*cmath.cos(beta)**3*cmath.sin(beta)',
                  order = {'QED':2})

GC_274 = Coupling(name = 'GC_274',
                  value = '-2*complex(0,1)*lam*vd*cmath.cos(beta)*cmath.sqrt(2)*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = '2*complex(0,1)*lam*vd*cmath.cos(beta)*cmath.sqrt(2)*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_276 = Coupling(name = 'GC_276',
                  value = '-6*complex(0,1)*lam*vd*cmath.cos(beta)*cmath.sqrt(2)*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_277 = Coupling(name = 'GC_277',
                  value = '-2*complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_278 = Coupling(name = 'GC_278',
                  value = '2*complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_279 = Coupling(name = 'GC_279',
                  value = '-4*complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_280 = Coupling(name = 'GC_280',
                  value = '-6*complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_281 = Coupling(name = 'GC_281',
                  value = '-2*complex(0,1)*lam*vd*cmath.sqrt(2)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_282 = Coupling(name = 'GC_282',
                  value = '-6*complex(0,1)*lam*vd*cmath.sqrt(2)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_283 = Coupling(name = 'GC_283',
                  value = '-2*complex(0,1)*lam*cmath.cos(beta)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_284 = Coupling(name = 'GC_284',
                  value = '2*complex(0,1)*lam*cmath.cos(beta)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_285 = Coupling(name = 'GC_285',
                  value = '4*complex(0,1)*lam*cmath.cos(beta)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_286 = Coupling(name = 'GC_286',
                  value = '-6*complex(0,1)*lam*cmath.cos(beta)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_287 = Coupling(name = 'GC_287',
                  value = '6*complex(0,1)*lam*cmath.cos(beta)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_288 = Coupling(name = 'GC_288',
                  value = '-2*complex(0,1)*lam*cmath.sin(beta)**4',
                  order = {'QED':2})

GC_289 = Coupling(name = 'GC_289',
                  value = '-4*complex(0,1)*lam*cmath.sin(beta)**4',
                  order = {'QED':2})

GC_290 = Coupling(name = 'GC_290',
                  value = '-6*complex(0,1)*lam*cmath.sin(beta)**4',
                  order = {'QED':2})

GC_291 = Coupling(name = 'GC_291',
                  value = '(ee**2*complex(0,1)*vu*cmath.cos(beta))/(cw*cmath.sqrt(2)) - (ee**2*complex(0,1)*vd*cmath.sin(beta))/(cw*cmath.sqrt(2))',
                  order = {'QED':2})

GC_292 = Coupling(name = 'GC_292',
                  value = '-((ee**2*complex(0,1)*vu*cmath.cos(beta))/(sw**2*cmath.sqrt(2))) + (ee**2*complex(0,1)*vd*cmath.sin(beta))/(sw**2*cmath.sqrt(2))',
                  order = {'QED':2})

GC_293 = Coupling(name = 'GC_293',
                  value = '-((ee**2*complex(0,1)*vu*cmath.cos(beta))/(sw*cmath.sqrt(2))) + (ee**2*complex(0,1)*vd*cmath.sin(beta))/(sw*cmath.sqrt(2))',
                  order = {'QED':2})

GC_294 = Coupling(name = 'GC_294',
                  value = '-((cw**2*ee**2*complex(0,1)*vu*cmath.cos(beta))/(sw**2*cmath.sqrt(2))) - (ee**2*complex(0,1)*sw**2*vu*cmath.cos(beta))/(cw**2*cmath.sqrt(2)) - ee**2*complex(0,1)*vu*cmath.cos(beta)*cmath.sqrt(2) + (cw**2*ee**2*complex(0,1)*vd*cmath.sin(beta))/(sw**2*cmath.sqrt(2)) + (ee**2*complex(0,1)*sw**2*vd*cmath.sin(beta))/(cw**2*cmath.sqrt(2)) + ee**2*complex(0,1)*vd*cmath.sqrt(2)*cmath.sin(beta)',
                  order = {'QED':2})

GC_295 = Coupling(name = 'GC_295',
                  value = '(ee**2*complex(0,1)*vd*cmath.cos(beta))/(cw*cmath.sqrt(2)) + (ee**2*complex(0,1)*vu*cmath.sin(beta))/(cw*cmath.sqrt(2))',
                  order = {'QED':2})

GC_296 = Coupling(name = 'GC_296',
                  value = '(ee**2*complex(0,1)*vd*cmath.cos(beta))/(sw**2*cmath.sqrt(2)) + (ee**2*complex(0,1)*vu*cmath.sin(beta))/(sw**2*cmath.sqrt(2))',
                  order = {'QED':2})

GC_297 = Coupling(name = 'GC_297',
                  value = '-((ee**2*complex(0,1)*vd*cmath.cos(beta))/(sw*cmath.sqrt(2))) - (ee**2*complex(0,1)*vu*cmath.sin(beta))/(sw*cmath.sqrt(2))',
                  order = {'QED':2})

GC_298 = Coupling(name = 'GC_298',
                  value = '(cw**2*ee**2*complex(0,1)*vd*cmath.cos(beta))/(sw**2*cmath.sqrt(2)) + (ee**2*complex(0,1)*sw**2*vd*cmath.cos(beta))/(cw**2*cmath.sqrt(2)) + ee**2*complex(0,1)*vd*cmath.cos(beta)*cmath.sqrt(2) + (cw**2*ee**2*complex(0,1)*vu*cmath.sin(beta))/(sw**2*cmath.sqrt(2)) + (ee**2*complex(0,1)*sw**2*vu*cmath.sin(beta))/(cw**2*cmath.sqrt(2)) + ee**2*complex(0,1)*vu*cmath.sqrt(2)*cmath.sin(beta)',
                  order = {'QED':2})

GC_299 = Coupling(name = 'GC_299',
                  value = '-(ee*complex(0,1)*cmath.cos(beta)**2) - ee*complex(0,1)*cmath.sin(beta)**2',
                  order = {'QED':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '2*ee**2*complex(0,1)*cmath.cos(beta)**2 + 2*ee**2*complex(0,1)*cmath.sin(beta)**2',
                  order = {'QED':2})

GC_301 = Coupling(name = 'GC_301',
                  value = '-(ee**2*cmath.cos(beta)**2)/(2.*cw) - (ee**2*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':2})

GC_302 = Coupling(name = 'GC_302',
                  value = '-(ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*cw) - (ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':2})

GC_303 = Coupling(name = 'GC_303',
                  value = '(ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*cw) + (ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':2})

GC_304 = Coupling(name = 'GC_304',
                  value = '(ee**2*cmath.cos(beta)**2)/(2.*cw) + (ee**2*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':2})

GC_305 = Coupling(name = 'GC_305',
                  value = '(ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*sw**2) + (ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_306 = Coupling(name = 'GC_306',
                  value = '-(ee*cmath.cos(beta)**2)/(2.*sw) - (ee*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '-(ee*complex(0,1)*cmath.cos(beta)**2)/(2.*sw) - (ee*complex(0,1)*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '(ee*complex(0,1)*cmath.cos(beta)**2)/(2.*sw) + (ee*complex(0,1)*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '-(ee**2*cmath.cos(beta)**2)/(2.*sw) - (ee**2*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':2})

GC_310 = Coupling(name = 'GC_310',
                  value = '-(ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*sw) - (ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':2})

GC_311 = Coupling(name = 'GC_311',
                  value = '(ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*sw) + (ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':2})

GC_312 = Coupling(name = 'GC_312',
                  value = '(ee**2*cmath.cos(beta)**2)/(2.*sw) + (ee**2*cmath.sin(beta)**2)/(2.*sw)',
                  order = {'QED':2})

GC_313 = Coupling(name = 'GC_313',
                  value = '-(cw*ee*cmath.cos(beta)**2)/(2.*sw) - (ee*sw*cmath.cos(beta)**2)/(2.*cw) - (cw*ee*cmath.sin(beta)**2)/(2.*sw) - (ee*sw*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '-(cw*ee*complex(0,1)*cmath.cos(beta)**2)/(2.*sw) + (ee*complex(0,1)*sw*cmath.cos(beta)**2)/(2.*cw) - (cw*ee*complex(0,1)*cmath.sin(beta)**2)/(2.*sw) + (ee*complex(0,1)*sw*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '(cw*ee*cmath.cos(beta)**2)/(2.*sw) + (ee*sw*cmath.cos(beta)**2)/(2.*cw) + (cw*ee*cmath.sin(beta)**2)/(2.*sw) + (ee*sw*cmath.sin(beta)**2)/(2.*cw)',
                  order = {'QED':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '(cw*ee**2*complex(0,1)*cmath.cos(beta)**2)/sw - (ee**2*complex(0,1)*sw*cmath.cos(beta)**2)/cw + (cw*ee**2*complex(0,1)*cmath.sin(beta)**2)/sw - (ee**2*complex(0,1)*sw*cmath.sin(beta)**2)/cw',
                  order = {'QED':2})

GC_317 = Coupling(name = 'GC_317',
                  value = '-(ee**2*complex(0,1)*cmath.cos(beta)**2) + (cw**2*ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*cmath.cos(beta)**2)/(2.*cw**2) - ee**2*complex(0,1)*cmath.sin(beta)**2 + (cw**2*ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*cmath.sin(beta)**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_318 = Coupling(name = 'GC_318',
                  value = 'ee**2*complex(0,1)*cmath.cos(beta)**2 + (cw**2*ee**2*complex(0,1)*cmath.cos(beta)**2)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*cmath.cos(beta)**2)/(2.*cw**2) + ee**2*complex(0,1)*cmath.sin(beta)**2 + (cw**2*ee**2*complex(0,1)*cmath.sin(beta)**2)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*cmath.sin(beta)**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_319 = Coupling(name = 'GC_319',
                  value = '(lam*vu*cmath.cos(beta)**3)/cmath.sqrt(2) - (lam*vd*cmath.cos(beta)**2*cmath.sin(beta))/cmath.sqrt(2) + (lam*vu*cmath.cos(beta)*cmath.sin(beta)**2)/cmath.sqrt(2) - (lam*vd*cmath.sin(beta)**3)/cmath.sqrt(2)',
                  order = {'QED':2})

GC_320 = Coupling(name = 'GC_320',
                  value = '(complex(0,1)*lam*vu*cmath.cos(beta)**3)/cmath.sqrt(2) + (3*complex(0,1)*lam*vd*cmath.cos(beta)**2*cmath.sin(beta))/cmath.sqrt(2) + (complex(0,1)*lam*vu*cmath.cos(beta)*cmath.sin(beta)**2)/cmath.sqrt(2) - (complex(0,1)*lam*vd*cmath.sin(beta)**3)/cmath.sqrt(2)',
                  order = {'QED':2})

GC_321 = Coupling(name = 'GC_321',
                  value = '-((lam*vu*cmath.cos(beta)**3)/cmath.sqrt(2)) + (lam*vd*cmath.cos(beta)**2*cmath.sin(beta))/cmath.sqrt(2) - (lam*vu*cmath.cos(beta)*cmath.sin(beta)**2)/cmath.sqrt(2) + (lam*vd*cmath.sin(beta)**3)/cmath.sqrt(2)',
                  order = {'QED':2})

GC_322 = Coupling(name = 'GC_322',
                  value = 'complex(0,1)*lam*vu*cmath.cos(beta)**3*cmath.sqrt(2) - 3*complex(0,1)*lam*vd*cmath.cos(beta)**2*cmath.sqrt(2)*cmath.sin(beta) + complex(0,1)*lam*vu*cmath.cos(beta)*cmath.sqrt(2)*cmath.sin(beta)**2 - complex(0,1)*lam*vd*cmath.sqrt(2)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = '-((lam*vd*cmath.cos(beta)**3)/cmath.sqrt(2)) - (lam*vu*cmath.cos(beta)**2*cmath.sin(beta))/cmath.sqrt(2) - (lam*vd*cmath.cos(beta)*cmath.sin(beta)**2)/cmath.sqrt(2) - (lam*vu*cmath.sin(beta)**3)/cmath.sqrt(2)',
                  order = {'QED':2})

GC_324 = Coupling(name = 'GC_324',
                  value = '-((complex(0,1)*lam*vd*cmath.cos(beta)**3)/cmath.sqrt(2)) - (complex(0,1)*lam*vu*cmath.cos(beta)**2*cmath.sin(beta))/cmath.sqrt(2) + (3*complex(0,1)*lam*vd*cmath.cos(beta)*cmath.sin(beta)**2)/cmath.sqrt(2) - (complex(0,1)*lam*vu*cmath.sin(beta)**3)/cmath.sqrt(2)',
                  order = {'QED':2})

GC_325 = Coupling(name = 'GC_325',
                  value = '(lam*vd*cmath.cos(beta)**3)/cmath.sqrt(2) + (lam*vu*cmath.cos(beta)**2*cmath.sin(beta))/cmath.sqrt(2) + (lam*vd*cmath.cos(beta)*cmath.sin(beta)**2)/cmath.sqrt(2) + (lam*vu*cmath.sin(beta)**3)/cmath.sqrt(2)',
                  order = {'QED':2})

GC_326 = Coupling(name = 'GC_326',
                  value = '-(complex(0,1)*lam*vd*cmath.cos(beta)**3*cmath.sqrt(2)) - complex(0,1)*lam*vu*cmath.cos(beta)**2*cmath.sqrt(2)*cmath.sin(beta) - 3*complex(0,1)*lam*vd*cmath.cos(beta)*cmath.sqrt(2)*cmath.sin(beta)**2 - complex(0,1)*lam*vu*cmath.sqrt(2)*cmath.sin(beta)**3',
                  order = {'QED':2})

GC_327 = Coupling(name = 'GC_327',
                  value = '-(lam*cmath.cos(beta)**4)/2. - lam*cmath.cos(beta)**2*cmath.sin(beta)**2 - (lam*cmath.sin(beta)**4)/2.',
                  order = {'QED':2})

GC_328 = Coupling(name = 'GC_328',
                  value = '-(complex(0,1)*lam*cmath.cos(beta)**4)/2. + complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2 - (complex(0,1)*lam*cmath.sin(beta)**4)/2.',
                  order = {'QED':2})

GC_329 = Coupling(name = 'GC_329',
                  value = '(complex(0,1)*lam*cmath.cos(beta)**4)/2. - complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2 + (complex(0,1)*lam*cmath.sin(beta)**4)/2.',
                  order = {'QED':2})

GC_330 = Coupling(name = 'GC_330',
                  value = '-(complex(0,1)*lam*cmath.cos(beta)**4) - 4*complex(0,1)*lam*cmath.cos(beta)**2*cmath.sin(beta)**2 - complex(0,1)*lam*cmath.sin(beta)**4',
                  order = {'QED':2})

GC_331 = Coupling(name = 'GC_331',
                  value = '(lam*cmath.cos(beta)**4)/2. + lam*cmath.cos(beta)**2*cmath.sin(beta)**2 + (lam*cmath.sin(beta)**4)/2.',
                  order = {'QED':2})

