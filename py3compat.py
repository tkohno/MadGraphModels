#!/usr/bin/env python3
import ast,sys

def test_source_code_compatible(code_data):
    try:
        return ast.parse(code_data)
    except SyntaxError as exc:
        return False

import glob
module_list = glob.glob('*/*/*.py')
model = None
model_ok = True
all_ok = True
for a_module in sorted(module_list):
    if model != a_module.split('/')[0]:
        #if model_ok:
        #    print('Model '+model+' appears python3 compatible.')
        model = a_module.split('/')[0]
        model_ok = True
    ast_tree = test_source_code_compatible(open(a_module, 'rb').read())
    if not ast_tree:
        print('Module '+a_module+' could not be loaded - python3 incompatible?')
        model_ok = False
        all_ok = False

if not all_ok:
    sys.exit(1)

sys.exit(0)
