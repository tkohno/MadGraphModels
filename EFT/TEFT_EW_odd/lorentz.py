# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Wed 14 Sep 2016 20:40:23


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


FF1 = Lorentz(name = 'FF1',
              spins = [ 2, 2 ],
              structure = 'P(-1,1)*Gamma(-1,2,1)')

FF2 = Lorentz(name = 'FF2',
              spins = [ 2, 2 ],
              structure = 'Identity(2,1)')

FF3 = Lorentz(name = 'FF3',
              spins = [ 2, 2 ],
              structure = 'ProjM(2,1) + ProjP(2,1)')

FF4 = Lorentz(name = 'FF4',
              spins = [ 2, 2 ],
              structure = 'P(-1,1)**2*ProjM(2,1) + P(-1,1)**2*ProjP(2,1)')

FF5 = Lorentz(name = 'FF5',
              spins = [ 2, 2 ],
              structure = 'P(-1,1)*Gamma(-1,2,-2)*ProjM(-2,1) + P(-1,1)*Gamma(-1,2,-2)*ProjP(-2,1)')

FF6 = Lorentz(name = 'FF6',
              spins = [ 2, 2 ],
              structure = '-(P(-1,1)*Gamma(-1,2,1)) + P(-1,1)*Gamma(-1,2,-2)*ProjM(-2,1) + P(-1,1)*Gamma(-1,2,-2)*ProjP(-2,1)')

FF7 = Lorentz(name = 'FF7',
              spins = [ 2, 2 ],
              structure = '(P(-1,1)*Gamma(-1,2,1))/2. + P(-1,1)*Gamma(-1,2,-2)*ProjM(-2,1) + P(-1,1)*Gamma(-1,2,-2)*ProjP(-2,1)')

VV1 = Lorentz(name = 'VV1',
              spins = [ 3, 3 ],
              structure = 'P(1,2)*P(2,2)')

VV2 = Lorentz(name = 'VV2',
              spins = [ 3, 3 ],
              structure = 'Metric(1,2)')

VV3 = Lorentz(name = 'VV3',
              spins = [ 3, 3 ],
              structure = 'P(-1,2)**2*Metric(1,2)')

VV4 = Lorentz(name = 'VV4',
              spins = [ 3, 3 ],
              structure = 'P(1,2)*P(2,2) - (3*P(-1,2)**2*Metric(1,2))/2.')

VV5 = Lorentz(name = 'VV5',
              spins = [ 3, 3 ],
              structure = 'P(1,2)*P(2,2) - P(-1,2)**2*Metric(1,2)')

UUV1 = Lorentz(name = 'UUV1',
               spins = [ -1, -1, 3 ],
               structure = 'P(3,2) + P(3,3)')

SSS1 = Lorentz(name = 'SSS1',
               spins = [ 1, 1, 1 ],
               structure = '1')

FFS1 = Lorentz(name = 'FFS1',
               spins = [ 2, 2, 1 ],
               structure = 'ProjM(2,1) + ProjP(2,1)')

FFV1 = Lorentz(name = 'FFV1',
               spins = [ 2, 2, 3 ],
               structure = 'Gamma(3,2,1)')

FFV2 = Lorentz(name = 'FFV2',
               spins = [ 2, 2, 3 ],
               structure = 'P(-1,3)*Gamma(-1,2,-2)*Gamma(3,-2,1)')

FFV3 = Lorentz(name = 'FFV3',
               spins = [ 2, 2, 3 ],
               structure = 'P(-1,3)*Gamma5(-2,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)')

FFV4 = Lorentz(name = 'FFV4',
               spins = [ 2, 2, 3 ],
               structure = 'P(-1,3)*Gamma(-1,-2,1)*Gamma(3,2,-2)')

FFV5 = Lorentz(name = 'FFV5',
               spins = [ 2, 2, 3 ],
               structure = '-(P(-1,3)*Gamma(-1,2,-2)*Gamma(3,-2,1)) + P(-1,3)*Gamma(-1,-2,1)*Gamma(3,2,-2)')

FFV6 = Lorentz(name = 'FFV6',
               spins = [ 2, 2, 3 ],
               structure = 'P(-1,3)*Gamma5(-2,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)')

FFV7 = Lorentz(name = 'FFV7',
               spins = [ 2, 2, 3 ],
               structure = '-(P(-1,3)*Gamma5(-3,1)*Gamma(-1,2,-2)*Gamma(3,-2,-3)) + P(-1,3)*Gamma5(-2,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)')

FFV8 = Lorentz(name = 'FFV8',
               spins = [ 2, 2, 3 ],
               structure = '-(P(-1,3)*Gamma5(-2,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)) + P(-1,3)*Gamma5(-2,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)')

FFV9 = Lorentz(name = 'FFV9',
               spins = [ 2, 2, 3 ],
               structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV10 = Lorentz(name = 'FFV10',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1)')

FFV11 = Lorentz(name = 'FFV11',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1)')

FFV12 = Lorentz(name = 'FFV12',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1)')

FFV13 = Lorentz(name = 'FFV13',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1)')

FFV14 = Lorentz(name = 'FFV14',
                spins = [ 2, 2, 3 ],
                structure = '-(P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1)) + P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1)')

FFV15 = Lorentz(name = 'FFV15',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

FFV16 = Lorentz(name = 'FFV16',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1) - 2*Gamma(3,2,-1)*ProjP(-1,1)')

FFV17 = Lorentz(name = 'FFV17',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1) - (5*Gamma(3,2,-1)*ProjP(-1,1))/3.')

FFV18 = Lorentz(name = 'FFV18',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1) - Gamma(3,2,-1)*ProjM(-1,1) - Gamma(3,2,-1)*ProjP(-1,1)')

FFV19 = Lorentz(name = 'FFV19',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1) - (7*Gamma(3,2,-1)*ProjP(-1,1))/17.')

FFV20 = Lorentz(name = 'FFV20',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1) + Gamma(3,2,-1)*ProjP(-1,1)')

FFV21 = Lorentz(name = 'FFV21',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1) + 2*Gamma(3,2,-1)*ProjP(-1,1)')

FFV22 = Lorentz(name = 'FFV22',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1) + 4*Gamma(3,2,-1)*ProjP(-1,1)')

FFV23 = Lorentz(name = 'FFV23',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1)')

FFV24 = Lorentz(name = 'FFV24',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1)')

FFV25 = Lorentz(name = 'FFV25',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,-2,1)*Gamma(3,2,-2) - P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1) - P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1)')

FFV26 = Lorentz(name = 'FFV26',
                spins = [ 2, 2, 3 ],
                structure = 'P(3,1)*Identity(2,1) - P(3,2)*Identity(2,1) - (5*P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/42. + (101*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/84. - (5*P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1))/42. - (37*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1))/28. - (5*P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/42. + (101*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/84. - (5*P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/42. - (37*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/28.')

FFV27 = Lorentz(name = 'FFV27',
                spins = [ 2, 2, 3 ],
                structure = '(19*P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/21. + (4*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/7. + P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1) + (8*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1))/21. + P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1) + (13*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/21. + (19*P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/21. + (P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/3.')

FFV28 = Lorentz(name = 'FFV28',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1) + (5*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/8. + P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1) + (3*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1))/8. + P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1) + (5*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/8. + P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1) + (3*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/8.')

FFV29 = Lorentz(name = 'FFV29',
                spins = [ 2, 2, 3 ],
                structure = '(11*P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/13. + (7*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/13. + P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1) + (5*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/13.')

FFV30 = Lorentz(name = 'FFV30',
                spins = [ 2, 2, 3 ],
                structure = '(13*P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/11. + (8*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjM(-2,1))/11. + P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1) + (4*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjM(-2,1))/11. + P(-1,1)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1) + (7*P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1))/11. + (13*P(-1,1)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/11. + (5*P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1))/11.')

FFV31 = Lorentz(name = 'FFV31',
                spins = [ 2, 2, 3 ],
                structure = '-(P(-1,3)*Gamma(-1,2,-3)*Gamma(3,-3,-2)*ProjP(-2,1)) + P(-1,3)*Gamma(-1,-3,-2)*Gamma(3,2,-3)*ProjP(-2,1)')

VVS1 = Lorentz(name = 'VVS1',
               spins = [ 3, 3, 1 ],
               structure = 'Metric(1,2)')

VVV1 = Lorentz(name = 'VVV1',
               spins = [ 3, 3, 3 ],
               structure = '-(Epsilon(1,2,3,-1)*P(-1,1)) + Epsilon(1,2,3,-1)*P(-1,2)')

VVV2 = Lorentz(name = 'VVV2',
               spins = [ 3, 3, 3 ],
               structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS1 = Lorentz(name = 'SSSS1',
                spins = [ 1, 1, 1, 1 ],
                structure = '1')

FFVV1 = Lorentz(name = 'FFVV1',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma(3,-1,1)*Gamma(4,2,-1)')

FFVV2 = Lorentz(name = 'FFVV2',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma5(-1,1)*Gamma(3,-2,-1)*Gamma(4,2,-2)')

FFVV3 = Lorentz(name = 'FFVV3',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma(3,2,-1)*Gamma(4,-1,1)')

FFVV4 = Lorentz(name = 'FFVV4',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma5(-1,1)*Gamma(3,2,-2)*Gamma(4,-2,-1)')

FFVV5 = Lorentz(name = 'FFVV5',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Identity(2,1)*Metric(3,4)')

FFVV6 = Lorentz(name = 'FFVV6',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma(3,2,-1)*Gamma(4,-1,1) + Gamma(3,-1,1)*Gamma(4,2,-1) - (2*Identity(2,1)*Metric(3,4))/7.')

FFVV7 = Lorentz(name = 'FFVV7',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma(3,2,-1)*Gamma(4,-1,1) + Gamma(3,-1,1)*Gamma(4,2,-1) + (56*Identity(2,1)*Metric(3,4))/65.')

FFVV8 = Lorentz(name = 'FFVV8',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Metric(3,4)*ProjM(2,1)')

FFVV9 = Lorentz(name = 'FFVV9',
                spins = [ 2, 2, 3, 3 ],
                structure = 'Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjM(-1,1)')

FFVV10 = Lorentz(name = 'FFVV10',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjM(-1,1)')

FFVV11 = Lorentz(name = 'FFVV11',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjM(-1,1) + (11*Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjM(-1,1))/8. - (Metric(3,4)*ProjM(2,1))/20.')

FFVV12 = Lorentz(name = 'FFVV12',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Metric(3,4)*ProjP(2,1)')

FFVV13 = Lorentz(name = 'FFVV13',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjP(-1,1)')

FFVV14 = Lorentz(name = 'FFVV14',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjP(-1,1)')

FFVV15 = Lorentz(name = 'FFVV15',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjM(-1,1) + (11*Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjM(-1,1))/8. - (Metric(3,4)*ProjM(2,1))/20. + (5*Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjP(-1,1))/8. + (Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjP(-1,1))/4. + (29*Metric(3,4)*ProjP(2,1))/20.')

FFVV16 = Lorentz(name = 'FFVV16',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjP(-1,1) + (2*Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjP(-1,1))/5. + (58*Metric(3,4)*ProjP(2,1))/25.')

FFVV17 = Lorentz(name = 'FFVV17',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjM(-1,1) + (19*Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjM(-1,1))/28. + (23*Metric(3,4)*ProjM(2,1))/14. + (37*Gamma(3,2,-2)*Gamma(4,-2,-1)*ProjP(-1,1))/28. + (23*Gamma(3,-2,-1)*Gamma(4,2,-2)*ProjP(-1,1))/14. + (5*Metric(3,4)*ProjP(2,1))/14.')

VVSS1 = Lorentz(name = 'VVSS1',
                spins = [ 3, 3, 1, 1 ],
                structure = 'Metric(1,2)')

VVVV1 = Lorentz(name = 'VVVV1',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Epsilon(1,2,3,4)')

VVVV2 = Lorentz(name = 'VVVV2',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,4)*Metric(2,3)')

VVVV3 = Lorentz(name = 'VVVV3',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,3)*Metric(2,4)')

VVVV4 = Lorentz(name = 'VVVV4',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV5 = Lorentz(name = 'VVVV5',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,2)*Metric(3,4)')

VVVV6 = Lorentz(name = 'VVVV6',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV7 = Lorentz(name = 'VVVV7',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV8 = Lorentz(name = 'VVVV8',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV9 = Lorentz(name = 'VVVV9',
                spins = [ 3, 3, 3, 3 ],
                structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

VVVV10 = Lorentz(name = 'VVVV10',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) + Metric(1,2)*Metric(3,4)')

