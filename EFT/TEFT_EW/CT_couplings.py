# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Mon 4 Jul 2016 18:12:49


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_101_2 = Coupling(name = 'R2GC_101_2',
                      value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_102_3 = Coupling(name = 'R2GC_102_3',
                      value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_105_4 = Coupling(name = 'R2GC_105_4',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_106_5 = Coupling(name = 'R2GC_106_5',
                      value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) - (5*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_107_6 = Coupling(name = 'R2GC_107_6',
                      value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) + (5*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_108_7 = Coupling(name = 'R2GC_108_7',
                      value = '-(ccc*cfb*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cfQ1*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cft*ee**2*complex(0,1)*G**2*MT**2)/(36.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(24.*cmath.pi**2*Lambda**2*sw**2)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_109_8 = Coupling(name = 'R2GC_109_8',
                      value = '(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_110_9 = Coupling(name = 'R2GC_110_9',
                      value = '(ccc*cfb*ee**2*complex(0,1)*G**2*MT**2)/(144.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee**2*complex(0,1)*G**2*MT**2)/(144.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(48.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw*cmath.pi**2*Lambda**2*sw)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_111_10 = Coupling(name = 'R2GC_111_10',
                       value = '-(ccc*cfb*ee*complex(0,1)*G**3*MT**2)/(96.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee*complex(0,1)*G**3*MT**2)/(48.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*complex(0,1)*G**3*MT**2)/(96.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_112_11 = Coupling(name = 'R2GC_112_11',
                       value = '(-7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw) + (35*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(108.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_113_12 = Coupling(name = 'R2GC_113_12',
                       value = '-(ccc*cfb*ee*G**2*MT**2)/(24.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ1*ee*G**2*MT**2)/(12.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*G**2*MT**2)/(24.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*G**2*MT**2)/(16.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*G**2*MT**2*sw)/(16.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_114_13 = Coupling(name = 'R2GC_114_13',
                       value = '(3*ccc*cfb*ee*complex(0,1)*G**3*MT**2)/(32.*cw*cmath.pi**2*Lambda**2*sw) - (3*ccc*cfQ1*ee*complex(0,1)*G**3*MT**2)/(16.*cw*cmath.pi**2*Lambda**2*sw) + (3*ccc*cft*ee*complex(0,1)*G**3*MT**2)/(32.*cw*cmath.pi**2*Lambda**2*sw) + (3*ccc*ctG*cw*ee*complex(0,1)*G**3*MT**2)/(16.*cmath.pi**2*Lambda**2*sw) + (3*ccc*ctG*ee*complex(0,1)*G**3*MT**2*sw)/(16.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_115_14 = Coupling(name = 'R2GC_115_14',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2) - (7*ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(48.*cmath.pi**2*Lambda**2*sw**2) - (119*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(432.*cw**2*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_116_15 = Coupling(name = 'R2GC_116_15',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_117_16 = Coupling(name = 'R2GC_117_16',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_117_17 = Coupling(name = 'R2GC_117_17',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_118_18 = Coupling(name = 'R2GC_118_18',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_118_19 = Coupling(name = 'R2GC_118_19',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_119_20 = Coupling(name = 'R2GC_119_20',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_119_21 = Coupling(name = 'R2GC_119_21',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_120_22 = Coupling(name = 'R2GC_120_22',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_120_23 = Coupling(name = 'R2GC_120_23',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_121_24 = Coupling(name = 'R2GC_121_24',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_121_25 = Coupling(name = 'R2GC_121_25',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_122_26 = Coupling(name = 'R2GC_122_26',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_122_27 = Coupling(name = 'R2GC_122_27',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_123_28 = Coupling(name = 'R2GC_123_28',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_123_29 = Coupling(name = 'R2GC_123_29',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_124_30 = Coupling(name = 'R2GC_124_30',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_125_31 = Coupling(name = 'R2GC_125_31',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_127_32 = Coupling(name = 'R2GC_127_32',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_141_33 = Coupling(name = 'R2GC_141_33',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_142_34 = Coupling(name = 'R2GC_142_34',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_143_35 = Coupling(name = 'R2GC_143_35',
                       value = '(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_144_36 = Coupling(name = 'R2GC_144_36',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_148_37 = Coupling(name = 'R2GC_148_37',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_148_38 = Coupling(name = 'R2GC_148_38',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_149_39 = Coupling(name = 'R2GC_149_39',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_149_40 = Coupling(name = 'R2GC_149_40',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_150_41 = Coupling(name = 'R2GC_150_41',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_150_42 = Coupling(name = 'R2GC_150_42',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_151_43 = Coupling(name = 'R2GC_151_43',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_152_44 = Coupling(name = 'R2GC_152_44',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_152_45 = Coupling(name = 'R2GC_152_45',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_153_46 = Coupling(name = 'R2GC_153_46',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_161_47 = Coupling(name = 'R2GC_161_47',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_165_48 = Coupling(name = 'R2GC_165_48',
                       value = '-(ccc*ctG*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2})

R2GC_172_49 = Coupling(name = 'R2GC_172_49',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(8.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_173_50 = Coupling(name = 'R2GC_173_50',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_174_51 = Coupling(name = 'R2GC_174_51',
                       value = '-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_175_52 = Coupling(name = 'R2GC_175_52',
                       value = '-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_176_53 = Coupling(name = 'R2GC_176_53',
                       value = '(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_177_54 = Coupling(name = 'R2GC_177_54',
                       value = '(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_180_55 = Coupling(name = 'R2GC_180_55',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_180_56 = Coupling(name = 'R2GC_180_56',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_181_57 = Coupling(name = 'R2GC_181_57',
                       value = '(11*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_181_58 = Coupling(name = 'R2GC_181_58',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_59 = Coupling(name = 'R2GC_182_59',
                       value = '(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_60 = Coupling(name = 'R2GC_182_60',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_183_61 = Coupling(name = 'R2GC_183_61',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_183_62 = Coupling(name = 'R2GC_183_62',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_63 = Coupling(name = 'R2GC_184_63',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_64 = Coupling(name = 'R2GC_184_64',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_188_65 = Coupling(name = 'R2GC_188_65',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_190_66 = Coupling(name = 'R2GC_190_66',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_191_67 = Coupling(name = 'R2GC_191_67',
                       value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_193_68 = Coupling(name = 'R2GC_193_68',
                       value = '(ccc*ctG*G**3*MT**2)/(8.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3})

R2GC_194_69 = Coupling(name = 'R2GC_194_69',
                       value = '(ccc*ctG*complex(0,1)*G**4*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_195_70 = Coupling(name = 'R2GC_195_70',
                       value = '-(ccc*ctG*complex(0,1)*G**4*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_198_71 = Coupling(name = 'R2GC_198_71',
                       value = '(3*ccc*ctG*complex(0,1)*G**4*MT)/(64.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_199_72 = Coupling(name = 'R2GC_199_72',
                       value = '(-3*ccc*ctG*complex(0,1)*G**4*MT)/(64.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_202_73 = Coupling(name = 'R2GC_202_73',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) - (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_203_74 = Coupling(name = 'R2GC_203_74',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) + (7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_204_75 = Coupling(name = 'R2GC_204_75',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) + (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_205_76 = Coupling(name = 'R2GC_205_76',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) - (7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_208_77 = Coupling(name = 'R2GC_208_77',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*MT*cmath.sqrt(2))/(9.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_210_78 = Coupling(name = 'R2GC_210_78',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_215_79 = Coupling(name = 'R2GC_215_79',
                       value = '(-5*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_216_80 = Coupling(name = 'R2GC_216_80',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_217_81 = Coupling(name = 'R2GC_217_81',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_61_82 = Coupling(name = 'R2GC_61_82',
                      value = '-(ccc*ctG*complex(0,1)*G**2*MT)/(2.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2})

R2GC_62_83 = Coupling(name = 'R2GC_62_83',
                      value = '(4*ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_63_84 = Coupling(name = 'R2GC_63_84',
                      value = '(-14*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(27.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_64_85 = Coupling(name = 'R2GC_64_85',
                      value = '(7*ccc*ctG*complex(0,1)*G**3*MT)/(16.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3})

R2GC_65_86 = Coupling(name = 'R2GC_65_86',
                      value = '(-65*ccc*ctG*ee*complex(0,1)*G**3*MT)/(288.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3,'QED':1})

R2GC_66_87 = Coupling(name = 'R2GC_66_87',
                      value = '(-17*ccc*ctG*complex(0,1)*G**4*MT)/(192.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':4})

R2GC_67_88 = Coupling(name = 'R2GC_67_88',
                      value = '(-11*ccc*ctG*complex(0,1)*G**4*MT)/(96.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':4})

R2GC_68_89 = Coupling(name = 'R2GC_68_89',
                      value = '(11*ccc*ctG*complex(0,1)*G**4*MT)/(96.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':4})

R2GC_69_90 = Coupling(name = 'R2GC_69_90',
                      value = '(17*ccc*ctG*complex(0,1)*G**4*MT)/(36.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':4})

R2GC_70_91 = Coupling(name = 'R2GC_70_91',
                      value = '(-37*ccc*ctG*complex(0,1)*G**4*MT)/(72.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':4})

R2GC_71_92 = Coupling(name = 'R2GC_71_92',
                      value = '(2*ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(9.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_72_93 = Coupling(name = 'R2GC_72_93',
                      value = '(7*ccc*ctG*complex(0,1)*G**3*MT**2)/(48.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3})

R2GC_73_94 = Coupling(name = 'R2GC_73_94',
                      value = '(7*ccc*ctG*complex(0,1)*G**2*MT**3)/(6.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2})

R2GC_74_95 = Coupling(name = 'R2GC_74_95',
                      value = '(ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(24.*cmath.pi**2*Lambda**2*sw**2)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_75_96 = Coupling(name = 'R2GC_75_96',
                      value = '(11*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_76_97 = Coupling(name = 'R2GC_76_97',
                      value = '(13*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_77_98 = Coupling(name = 'R2GC_77_98',
                      value = '(11*ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_78_99 = Coupling(name = 'R2GC_78_99',
                      value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_79_100 = Coupling(name = 'R2GC_79_100',
                       value = '(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_80_101 = Coupling(name = 'R2GC_80_101',
                       value = '(-25*ccc*ctG*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_81_102 = Coupling(name = 'R2GC_81_102',
                       value = '(-5*ccc*ctG*ee*complex(0,1)*G**3*MT)/(24.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_82_103 = Coupling(name = 'R2GC_82_103',
                       value = '(-5*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(48.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_83_104 = Coupling(name = 'R2GC_83_104',
                       value = '(ccc*cfb*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_84_105 = Coupling(name = 'R2GC_84_105',
                       value = '-(ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_85_106 = Coupling(name = 'R2GC_85_106',
                       value = '(-7*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(24.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_86_107 = Coupling(name = 'R2GC_86_107',
                       value = '(35*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(288.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_87_108 = Coupling(name = 'R2GC_87_108',
                       value = '(-17*ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_88_109 = Coupling(name = 'R2GC_88_109',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_89_110 = Coupling(name = 'R2GC_89_110',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_91_111 = Coupling(name = 'R2GC_91_111',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_97_112 = Coupling(name = 'R2GC_97_112',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

UVGC_129_1 = Coupling(name = 'UVGC_129_1',
                      value = {-1:'(-22*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(81.*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_130_2 = Coupling(name = 'UVGC_130_2',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_131_3 = Coupling(name = 'UVGC_131_3',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_133_4 = Coupling(name = 'UVGC_133_4',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_138_5 = Coupling(name = 'UVGC_138_5',
                      value = {-1:'(complex(0,1)*G**2)/(6.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_141_6 = Coupling(name = 'UVGC_141_6',
                      value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_143_7 = Coupling(name = 'UVGC_143_7',
                      value = {-1:'(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_144_8 = Coupling(name = 'UVGC_144_8',
                      value = {-1:'(-5*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_145_9 = Coupling(name = 'UVGC_145_9',
                      value = {-1:'(-11*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(108.*cmath.pi**2*Lambda**2*sw) + (55*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(324.*cw*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_146_10 = Coupling(name = 'UVGC_146_10',
                       value = {-1:'(11*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(216.*cmath.pi**2*Lambda**2) - (11*ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(144.*cmath.pi**2*Lambda**2*sw**2) - (187*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(1296.*cw**2*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_147_11 = Coupling(name = 'UVGC_147_11',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_147_12 = Coupling(name = 'UVGC_147_12',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_148_13 = Coupling(name = 'UVGC_148_13',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_148_14 = Coupling(name = 'UVGC_148_14',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_149_15 = Coupling(name = 'UVGC_149_15',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_149_16 = Coupling(name = 'UVGC_149_16',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_17 = Coupling(name = 'UVGC_151_17',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_18 = Coupling(name = 'UVGC_151_18',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_19 = Coupling(name = 'UVGC_152_19',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_20 = Coupling(name = 'UVGC_152_20',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_21 = Coupling(name = 'UVGC_153_21',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_154_22 = Coupling(name = 'UVGC_154_22',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_154_23 = Coupling(name = 'UVGC_154_23',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_154_24 = Coupling(name = 'UVGC_154_24',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_154_25 = Coupling(name = 'UVGC_154_25',
                       value = {-1:'(-3*complex(0,1)*G**3)/(16.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_161_26 = Coupling(name = 'UVGC_161_26',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_161_27 = Coupling(name = 'UVGC_161_27',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_163_28 = Coupling(name = 'UVGC_163_28',
                       value = {-1:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2)',0:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_164_29 = Coupling(name = 'UVGC_164_29',
                       value = {-1:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2)',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_165_30 = Coupling(name = 'UVGC_165_30',
                       value = {-1:'(ccc*ctG*complex(0,1)*G**2*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_166_31 = Coupling(name = 'UVGC_166_31',
                       value = {-1:'-(ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(9.*cmath.pi**2*Lambda**2) + (2*ccc*ctG*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(3.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_167_32 = Coupling(name = 'UVGC_167_32',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**3*MT**2)/(6.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT**2*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_168_33 = Coupling(name = 'UVGC_168_33',
                       value = {-1:'(3*ccc*ctG*complex(0,1)*G**2*MT**3)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**3)/(2.*cmath.pi**2*Lambda**2) - (3*ccc*ctG*complex(0,1)*G**2*MT**3*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_169_34 = Coupling(name = 'UVGC_169_34',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_170_35 = Coupling(name = 'UVGC_170_35',
                       value = {-1:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_171_36 = Coupling(name = 'UVGC_171_36',
                       value = {-1:'(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_172_37 = Coupling(name = 'UVGC_172_37',
                       value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(-2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_173_38 = Coupling(name = 'UVGC_173_38',
                       value = {-1:'(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(-2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_174_39 = Coupling(name = 'UVGC_174_39',
                       value = {-1:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cff*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_175_40 = Coupling(name = 'UVGC_175_40',
                       value = {-1:'-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_176_41 = Coupling(name = 'UVGC_176_41',
                       value = {-1:'(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw)',0:'(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_177_42 = Coupling(name = 'UVGC_177_42',
                       value = {-1:'(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw)',0:'(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_178_43 = Coupling(name = 'UVGC_178_43',
                       value = {-1:'(ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(12.*cw*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw*reglog(MT/MU_R))/(6.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_179_44 = Coupling(name = 'UVGC_179_44',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_180_45 = Coupling(name = 'UVGC_180_45',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_46 = Coupling(name = 'UVGC_180_46',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_47 = Coupling(name = 'UVGC_180_47',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_48 = Coupling(name = 'UVGC_180_48',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_181_49 = Coupling(name = 'UVGC_181_49',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_181_50 = Coupling(name = 'UVGC_181_50',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_181_51 = Coupling(name = 'UVGC_181_51',
                       value = {-1:'(17*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_181_52 = Coupling(name = 'UVGC_181_52',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_53 = Coupling(name = 'UVGC_182_53',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_54 = Coupling(name = 'UVGC_182_54',
                       value = {-1:'(5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_55 = Coupling(name = 'UVGC_183_55',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_56 = Coupling(name = 'UVGC_183_56',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_57 = Coupling(name = 'UVGC_183_57',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_58 = Coupling(name = 'UVGC_183_58',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_184_59 = Coupling(name = 'UVGC_184_59',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_184_60 = Coupling(name = 'UVGC_184_60',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_185_61 = Coupling(name = 'UVGC_185_61',
                       value = {0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_186_62 = Coupling(name = 'UVGC_186_62',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_187_63 = Coupling(name = 'UVGC_187_63',
                       value = {-1:'(-7*complex(0,1)*G**3)/(16.*cmath.pi**2)',0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_188_64 = Coupling(name = 'UVGC_188_64',
                       value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_189_65 = Coupling(name = 'UVGC_189_65',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',0:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_190_66 = Coupling(name = 'UVGC_190_66',
                       value = {-1:'-(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw)',0:'-(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw)'},
                       order = {'QCD':2,'QED':1})

UVGC_191_67 = Coupling(name = 'UVGC_191_67',
                       value = {-1:'(ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_192_68 = Coupling(name = 'UVGC_192_68',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**2*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_193_69 = Coupling(name = 'UVGC_193_69',
                       value = {-1:'(ccc*ctG*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*G**3*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_194_70 = Coupling(name = 'UVGC_194_70',
                       value = {-1:'(ccc*ctG*complex(0,1)*G**4*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**4*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_195_71 = Coupling(name = 'UVGC_195_71',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**4*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**4*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_196_72 = Coupling(name = 'UVGC_196_72',
                       value = {-1:'-(ccc*ctB*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2) + (2*ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctB*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctB*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctB*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2) + (4*ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(9.*cmath.pi**2*Lambda**2) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_197_73 = Coupling(name = 'UVGC_197_73',
                       value = {-1:'(-43*ccc*ctG*complex(0,1)*G**3*MT)/(96.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**3*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_198_74 = Coupling(name = 'UVGC_198_74',
                       value = {-1:'(11*ccc*ctG*complex(0,1)*G**4*MT)/(16.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**4*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**4*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**4*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_199_75 = Coupling(name = 'UVGC_199_75',
                       value = {-1:'(-11*ccc*ctG*complex(0,1)*G**4*MT)/(16.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**4*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**4*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**4*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_200_76 = Coupling(name = 'UVGC_200_76',
                       value = {-1:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_201_77 = Coupling(name = 'UVGC_201_77',
                       value = {-1:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_202_78 = Coupling(name = 'UVGC_202_78',
                       value = {-1:'-(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_204_79 = Coupling(name = 'UVGC_204_79',
                       value = {-1:'(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_206_80 = Coupling(name = 'UVGC_206_80',
                       value = {-1:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_207_81 = Coupling(name = 'UVGC_207_81',
                       value = {-1:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_208_82 = Coupling(name = 'UVGC_208_82',
                       value = {-1:'-(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_209_83 = Coupling(name = 'UVGC_209_83',
                       value = {-1:'(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_211_84 = Coupling(name = 'UVGC_211_84',
                       value = {-1:'(cbW*ccc*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw)',0:'(cbW*ccc*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_212_85 = Coupling(name = 'UVGC_212_85',
                       value = {-1:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_213_86 = Coupling(name = 'UVGC_213_86',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_214_87 = Coupling(name = 'UVGC_214_87',
                       value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_216_88 = Coupling(name = 'UVGC_216_88',
                       value = {-1:'(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_218_89 = Coupling(name = 'UVGC_218_89',
                       value = {-1:'(ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw) - (ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(6.*cw*cmath.pi**2*Lambda**2) - (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(36.*cw*cmath.pi**2*Lambda**2)',0:'-(ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(3.*cw*cmath.pi**2*Lambda**2) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2) + (ccc*ctG*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(6.*cw*cmath.pi**2*Lambda**2) - (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(18.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

