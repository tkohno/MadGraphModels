# This file was automatically created by FeynRules 2.3.31
# Mathematica version: 9.0 for Linux x86 (64-bit) (February 7, 2013)
# Date: Tue 11 Sep 2018 16:19:44


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '(-4*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '(5*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-G',
                order = {'QCD':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_10 = Coupling(name = 'GC_10',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*KBLh',
                 order = {'BHL':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*KBRh',
                 order = {'BHR':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-4*cw**2*complex(0,1)*KetaBB',
                 order = {'EBB':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-KetaDP1x1',
                 order = {'EDP':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '-KetaDP2x2',
                 order = {'EDP':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-KetaDP3x3',
                 order = {'EDP':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'complex(0,1)*KetaDS1x1',
                 order = {'EDS':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'complex(0,1)*KetaDS2x2',
                 order = {'EDS':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'complex(0,1)*KetaDS3x3',
                 order = {'EDS':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(complex(0,1)*KetaGG)',
                 order = {'EGG':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-2*G*KetaGG',
                 order = {'EGG':1,'QCD':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-KetaLP1x1',
                 order = {'ELP':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '-KetaLP2x2',
                 order = {'ELP':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-KetaLP3x3',
                 order = {'ELP':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'complex(0,1)*KetaLS1x1',
                 order = {'ELS':1})

GC_26 = Coupling(name = 'GC_26',
                 value = 'complex(0,1)*KetaLS2x2',
                 order = {'ELS':1})

GC_27 = Coupling(name = 'GC_27',
                 value = 'complex(0,1)*KetaLS3x3',
                 order = {'ELS':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-KetaNP1x1',
                 order = {'ENP':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '-KetaNP2x2',
                 order = {'ENP':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-KetaNP3x3',
                 order = {'ENP':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'complex(0,1)*KetaNS1x1',
                 order = {'ENS':1})

GC_32 = Coupling(name = 'GC_32',
                 value = 'complex(0,1)*KetaNS2x2',
                 order = {'ENS':1})

GC_33 = Coupling(name = 'GC_33',
                 value = 'complex(0,1)*KetaNS3x3',
                 order = {'ENS':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-KetaUP1x1',
                 order = {'EUP':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '-KetaUP2x2',
                 order = {'EUP':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '-KetaUP3x3',
                 order = {'EUP':1})

GC_37 = Coupling(name = 'GC_37',
                 value = 'complex(0,1)*KetaUS1x1',
                 order = {'EUS':1})

GC_38 = Coupling(name = 'GC_38',
                 value = 'complex(0,1)*KetaUS2x2',
                 order = {'EUS':1})

GC_39 = Coupling(name = 'GC_39',
                 value = 'complex(0,1)*KetaUS3x3',
                 order = {'EUS':1})

GC_40 = Coupling(name = 'GC_40',
                 value = 'complex(0,1)*KetaVLBL',
                 order = {'EBL':1})

GC_41 = Coupling(name = 'GC_41',
                 value = 'complex(0,1)*KetaVLBR',
                 order = {'EBR':1})

GC_42 = Coupling(name = 'GC_42',
                 value = 'complex(0,1)*KetaVLTL',
                 order = {'ETL':1})

GC_43 = Coupling(name = 'GC_43',
                 value = 'complex(0,1)*KetaVLTR',
                 order = {'ETR':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-4*complex(0,1)*KetaWW',
                 order = {'EWW':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-4*cw**2*complex(0,1)*KetaWW',
                 order = {'EWW':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '8*ee*complex(0,1)*KetaWW',
                 order = {'EWW':1,'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = 'complex(0,1)*KTLh',
                 order = {'THL':1})

GC_48 = Coupling(name = 'GC_48',
                 value = 'complex(0,1)*KTRh',
                 order = {'THR':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '(ee*complex(0,1)*KBLw)/(sw*cmath.sqrt(2))',
                 order = {'BWL':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(ee*complex(0,1)*KBLz)/(2.*cw*sw)',
                 order = {'BZL':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(ee*complex(0,1)*KBRw)/(sw*cmath.sqrt(2))',
                 order = {'BWR':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(ee*complex(0,1)*KBRz)/(2.*cw*sw)',
                 order = {'BZR':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(8*cw*ee*complex(0,1)*KetaWW)/sw',
                 order = {'EWW':1,'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(ee*complex(0,1)*KTLw)/(sw*cmath.sqrt(2))',
                 order = {'TWL':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(ee*complex(0,1)*KTLz)/(2.*cw*sw)',
                 order = {'TZL':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(ee*complex(0,1)*KTRw)/(sw*cmath.sqrt(2))',
                 order = {'TWR':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(ee*complex(0,1)*KTRz)/(2.*cw*sw)',
                 order = {'TZR':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(ee*complex(0,1)*KXL)/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'XWL':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '(ee*complex(0,1)*KXR)/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'XWR':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(ee*complex(0,1)*KYL)/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'YWL':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(ee*complex(0,1)*KYR)/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'YWR':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '4*cw*complex(0,1)*KetaBB*sw',
                 order = {'EBB':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-4*cw*complex(0,1)*KetaWW*sw',
                 order = {'EWW':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-4*complex(0,1)*KetaBB*sw**2',
                 order = {'EBB':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-4*complex(0,1)*KetaWW*sw**2',
                 order = {'EWW':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = '-6*complex(0,1)*lam*vev',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

