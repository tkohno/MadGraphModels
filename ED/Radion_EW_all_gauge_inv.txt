Requestor: Kalliopi Iordanidou
Content: Triboson signal from warmped extra dimensions; KK EW gauge bosons and Radion, where only EW gauge fields are in the extended bulk.
Presentation: https://indico.cern.ch/event/743019/#2-theory-talk-generalized-tri
Paper1: https://arxiv.org/abs/1608.00526
Paper2: https://arxiv.org/abs/1809.07334 
Paper3: https://arxiv.org/abs/1711.09920 
Paper4: https://arxiv.org/abs/1612.00047  
Source: Kaustubh Agashe and Peizhi Du (private communication)
JIRA: https://its.cern.ch/jira/browse/AGENE-1640
