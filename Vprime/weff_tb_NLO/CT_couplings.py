# This file was automatically created by FeynRules 2.3.15
# Mathematica version: 10.0 for Linux x86 (64-bit) (December 4, 2014)
# Date: Tue 19 Apr 2016 09:07:23


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_100_2 = Coupling(name = 'R2GC_100_2',
                      value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_101_3 = Coupling(name = 'R2GC_101_3',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_101_4 = Coupling(name = 'R2GC_101_4',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_102_5 = Coupling(name = 'R2GC_102_5',
                      value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_102_6 = Coupling(name = 'R2GC_102_6',
                      value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_103_7 = Coupling(name = 'R2GC_103_7',
                      value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_103_8 = Coupling(name = 'R2GC_103_8',
                      value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_104_9 = Coupling(name = 'R2GC_104_9',
                      value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_104_10 = Coupling(name = 'R2GC_104_10',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_105_11 = Coupling(name = 'R2GC_105_11',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_107_12 = Coupling(name = 'R2GC_107_12',
                       value = '(CLq2x3*CLq3x2*complex(0,1)*G**2*gL**2)/(96.*cmath.pi**2) + (CRq2x3*CRq3x2*complex(0,1)*G**2*gR**2)/(96.*cmath.pi**2)',
                       order = {'NP':2,'QCD':2})

R2GC_107_13 = Coupling(name = 'R2GC_107_13',
                       value = '(CLq3x3**2*complex(0,1)*G**2*gL**2)/(96.*cmath.pi**2) + (CRq3x3**2*complex(0,1)*G**2*gR**2)/(96.*cmath.pi**2)',
                       order = {'NP':2,'QCD':2})

R2GC_107_14 = Coupling(name = 'R2GC_107_14',
                       value = '(CLq1x3*CLq3x1*complex(0,1)*G**2*gL**2)/(96.*cmath.pi**2) + (CRq1x3*CRq3x1*complex(0,1)*G**2*gR**2)/(96.*cmath.pi**2)',
                       order = {'NP':2,'QCD':2})

R2GC_107_15 = Coupling(name = 'R2GC_107_15',
                       value = '(CLq1x2*CLq2x1*complex(0,1)*G**2*gL**2)/(96.*cmath.pi**2) + (CRq1x2*CRq2x1*complex(0,1)*G**2*gR**2)/(96.*cmath.pi**2)',
                       order = {'NP':2,'QCD':2})

R2GC_107_16 = Coupling(name = 'R2GC_107_16',
                       value = '(CLq2x2**2*complex(0,1)*G**2*gL**2)/(96.*cmath.pi**2) + (CRq2x2**2*complex(0,1)*G**2*gR**2)/(96.*cmath.pi**2)',
                       order = {'NP':2,'QCD':2})

R2GC_107_17 = Coupling(name = 'R2GC_107_17',
                       value = '(CLq1x1**2*complex(0,1)*G**2*gL**2)/(96.*cmath.pi**2) + (CRq1x1**2*complex(0,1)*G**2*gR**2)/(96.*cmath.pi**2)',
                       order = {'NP':2,'QCD':2})

R2GC_108_18 = Coupling(name = 'R2GC_108_18',
                       value = '(CKM3x3*CLq3x3*ee*complex(0,1)*G**2*gL)/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_108_19 = Coupling(name = 'R2GC_108_19',
                       value = '(CKM2x1*CLq1x2*ee*complex(0,1)*G**2*gL)/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_108_20 = Coupling(name = 'R2GC_108_20',
                       value = '(CKM2x2*CLq2x2*ee*complex(0,1)*G**2*gL)/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_108_21 = Coupling(name = 'R2GC_108_21',
                       value = '(CKM1x1*CLq1x1*ee*complex(0,1)*G**2*gL)/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_108_22 = Coupling(name = 'R2GC_108_22',
                       value = '(CKM1x2*CLq2x1*ee*complex(0,1)*G**2*gL)/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_109_23 = Coupling(name = 'R2GC_109_23',
                       value = '(CKM3x3*ee**2*complex(0,1)*G**2*complexconjugate(CKM3x3))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_109_24 = Coupling(name = 'R2GC_109_24',
                       value = '(CKM2x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_109_25 = Coupling(name = 'R2GC_109_25',
                       value = '(CKM2x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_109_26 = Coupling(name = 'R2GC_109_26',
                       value = '(CKM1x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_109_27 = Coupling(name = 'R2GC_109_27',
                       value = '(CKM1x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_110_28 = Coupling(name = 'R2GC_110_28',
                       value = '(CLq3x3*ee*complex(0,1)*G**2*gL*complexconjugate(CKM3x3))/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_110_29 = Coupling(name = 'R2GC_110_29',
                       value = '(CLq2x1*ee*complex(0,1)*G**2*gL*complexconjugate(CKM2x1))/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_110_30 = Coupling(name = 'R2GC_110_30',
                       value = '(CLq2x2*ee*complex(0,1)*G**2*gL*complexconjugate(CKM2x2))/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_110_31 = Coupling(name = 'R2GC_110_31',
                       value = '(CLq1x1*ee*complex(0,1)*G**2*gL*complexconjugate(CKM1x1))/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_110_32 = Coupling(name = 'R2GC_110_32',
                       value = '(CLq1x2*ee*complex(0,1)*G**2*gL*complexconjugate(CKM1x2))/(96.*cmath.pi**2*sw)',
                       order = {'NP':1,'QCD':2,'QED':1})

R2GC_122_33 = Coupling(name = 'R2GC_122_33',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_122_34 = Coupling(name = 'R2GC_122_34',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_123_35 = Coupling(name = 'R2GC_123_35',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_123_36 = Coupling(name = 'R2GC_123_36',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_124_37 = Coupling(name = 'R2GC_124_37',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_124_38 = Coupling(name = 'R2GC_124_38',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_125_39 = Coupling(name = 'R2GC_125_39',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_126_40 = Coupling(name = 'R2GC_126_40',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_126_41 = Coupling(name = 'R2GC_126_41',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_127_42 = Coupling(name = 'R2GC_127_42',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_127_43 = Coupling(name = 'R2GC_127_43',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_128_44 = Coupling(name = 'R2GC_128_44',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_128_45 = Coupling(name = 'R2GC_128_45',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_129_46 = Coupling(name = 'R2GC_129_46',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_130_47 = Coupling(name = 'R2GC_130_47',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_131_48 = Coupling(name = 'R2GC_131_48',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_141_49 = Coupling(name = 'R2GC_141_49',
                       value = '-(CLq1x2*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_142_50 = Coupling(name = 'R2GC_142_50',
                       value = '-(CLq2x1*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_143_51 = Coupling(name = 'R2GC_143_51',
                       value = '-(CRq1x2*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_144_52 = Coupling(name = 'R2GC_144_52',
                       value = '-(CRq2x1*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_145_53 = Coupling(name = 'R2GC_145_53',
                       value = '-(CKM2x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_146_54 = Coupling(name = 'R2GC_146_54',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_147_55 = Coupling(name = 'R2GC_147_55',
                       value = '-(CLq2x2*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_148_56 = Coupling(name = 'R2GC_148_56',
                       value = '-(CRq2x2*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_149_57 = Coupling(name = 'R2GC_149_57',
                       value = '-(CKM2x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_150_58 = Coupling(name = 'R2GC_150_58',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_151_59 = Coupling(name = 'R2GC_151_59',
                       value = '-(CLq1x1*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_152_60 = Coupling(name = 'R2GC_152_60',
                       value = '-(CRq1x1*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_153_61 = Coupling(name = 'R2GC_153_61',
                       value = '-(CKM1x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_154_62 = Coupling(name = 'R2GC_154_62',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_159_63 = Coupling(name = 'R2GC_159_63',
                       value = '-(CKM1x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_160_64 = Coupling(name = 'R2GC_160_64',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_161_65 = Coupling(name = 'R2GC_161_65',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_163_66 = Coupling(name = 'R2GC_163_66',
                       value = '-(CLq1x3*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_164_67 = Coupling(name = 'R2GC_164_67',
                       value = '-(CLq2x3*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_165_68 = Coupling(name = 'R2GC_165_68',
                       value = '-(CLq3x1*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_166_69 = Coupling(name = 'R2GC_166_69',
                       value = '-(CLq3x2*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_167_70 = Coupling(name = 'R2GC_167_70',
                       value = '-(CRq1x3*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_168_71 = Coupling(name = 'R2GC_168_71',
                       value = '-(CRq2x3*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_169_72 = Coupling(name = 'R2GC_169_72',
                       value = '-(CRq3x1*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_170_73 = Coupling(name = 'R2GC_170_73',
                       value = '-(CRq3x2*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_171_74 = Coupling(name = 'R2GC_171_74',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_172_75 = Coupling(name = 'R2GC_172_75',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_173_76 = Coupling(name = 'R2GC_173_76',
                       value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_174_77 = Coupling(name = 'R2GC_174_77',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_176_78 = Coupling(name = 'R2GC_176_78',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_176_79 = Coupling(name = 'R2GC_176_79',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_182_80 = Coupling(name = 'R2GC_182_80',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_81 = Coupling(name = 'R2GC_182_81',
                       value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_183_82 = Coupling(name = 'R2GC_183_82',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_83 = Coupling(name = 'R2GC_184_83',
                       value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_185_84 = Coupling(name = 'R2GC_185_84',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_186_85 = Coupling(name = 'R2GC_186_85',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_187_86 = Coupling(name = 'R2GC_187_86',
                       value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_195_87 = Coupling(name = 'R2GC_195_87',
                       value = '-(CLq3x3*complex(0,1)*G**2*gL)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_200_88 = Coupling(name = 'R2GC_200_88',
                       value = '-(CRq3x3*complex(0,1)*G**2*gR)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'NP':1,'QCD':2})

R2GC_201_89 = Coupling(name = 'R2GC_201_89',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_202_90 = Coupling(name = 'R2GC_202_90',
                       value = '-(CKM3x3*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_204_91 = Coupling(name = 'R2GC_204_91',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_205_92 = Coupling(name = 'R2GC_205_92',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_206_93 = Coupling(name = 'R2GC_206_93',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_84_94 = Coupling(name = 'R2GC_84_94',
                      value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_95_95 = Coupling(name = 'R2GC_95_95',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_95_96 = Coupling(name = 'R2GC_95_96',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_96_97 = Coupling(name = 'R2GC_96_97',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_96_98 = Coupling(name = 'R2GC_96_98',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_97_99 = Coupling(name = 'R2GC_97_99',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_97_100 = Coupling(name = 'R2GC_97_100',
                       value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_98_101 = Coupling(name = 'R2GC_98_101',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_99_102 = Coupling(name = 'R2GC_99_102',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_99_103 = Coupling(name = 'R2GC_99_103',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_111_1 = Coupling(name = 'UVGC_111_1',
                      value = {-1:'(51*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_112_2 = Coupling(name = 'UVGC_112_2',
                      value = {-1:'G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_113_3 = Coupling(name = 'UVGC_113_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_114_4 = Coupling(name = 'UVGC_114_4',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_116_5 = Coupling(name = 'UVGC_116_5',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_121_6 = Coupling(name = 'UVGC_121_6',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_121_7 = Coupling(name = 'UVGC_121_7',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_122_8 = Coupling(name = 'UVGC_122_8',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_122_9 = Coupling(name = 'UVGC_122_9',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_123_10 = Coupling(name = 'UVGC_123_10',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_123_11 = Coupling(name = 'UVGC_123_11',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_125_12 = Coupling(name = 'UVGC_125_12',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_125_13 = Coupling(name = 'UVGC_125_13',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_126_14 = Coupling(name = 'UVGC_126_14',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_126_15 = Coupling(name = 'UVGC_126_15',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_127_16 = Coupling(name = 'UVGC_127_16',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_127_17 = Coupling(name = 'UVGC_127_17',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_18 = Coupling(name = 'UVGC_128_18',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_19 = Coupling(name = 'UVGC_128_19',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_129_20 = Coupling(name = 'UVGC_129_20',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_130_21 = Coupling(name = 'UVGC_130_21',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_131_22 = Coupling(name = 'UVGC_131_22',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_141_23 = Coupling(name = 'UVGC_141_23',
                       value = {-1:'(CLq1x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_141_24 = Coupling(name = 'UVGC_141_24',
                       value = {-1:'-(CLq1x2*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_142_25 = Coupling(name = 'UVGC_142_25',
                       value = {-1:'(CLq2x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_142_26 = Coupling(name = 'UVGC_142_26',
                       value = {-1:'-(CLq2x1*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_143_27 = Coupling(name = 'UVGC_143_27',
                       value = {-1:'(CRq1x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_143_28 = Coupling(name = 'UVGC_143_28',
                       value = {-1:'-(CRq1x2*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_144_29 = Coupling(name = 'UVGC_144_29',
                       value = {-1:'(CRq2x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_144_30 = Coupling(name = 'UVGC_144_30',
                       value = {-1:'-(CRq2x1*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_145_31 = Coupling(name = 'UVGC_145_31',
                       value = {-1:'(CKM2x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_145_32 = Coupling(name = 'UVGC_145_32',
                       value = {-1:'-(CKM2x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_146_33 = Coupling(name = 'UVGC_146_33',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_146_34 = Coupling(name = 'UVGC_146_34',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_147_35 = Coupling(name = 'UVGC_147_35',
                       value = {-1:'(CLq2x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_147_36 = Coupling(name = 'UVGC_147_36',
                       value = {-1:'-(CLq2x2*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_148_37 = Coupling(name = 'UVGC_148_37',
                       value = {-1:'(CRq2x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_148_38 = Coupling(name = 'UVGC_148_38',
                       value = {-1:'-(CRq2x2*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_149_39 = Coupling(name = 'UVGC_149_39',
                       value = {-1:'(CKM2x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_149_40 = Coupling(name = 'UVGC_149_40',
                       value = {-1:'-(CKM2x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_150_41 = Coupling(name = 'UVGC_150_41',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_150_42 = Coupling(name = 'UVGC_150_42',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_151_43 = Coupling(name = 'UVGC_151_43',
                       value = {-1:'(CLq1x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_151_44 = Coupling(name = 'UVGC_151_44',
                       value = {-1:'-(CLq1x1*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_152_45 = Coupling(name = 'UVGC_152_45',
                       value = {-1:'(CRq1x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_152_46 = Coupling(name = 'UVGC_152_46',
                       value = {-1:'-(CRq1x1*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_153_47 = Coupling(name = 'UVGC_153_47',
                       value = {-1:'(CKM1x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_153_48 = Coupling(name = 'UVGC_153_48',
                       value = {-1:'-(CKM1x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_154_49 = Coupling(name = 'UVGC_154_49',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_154_50 = Coupling(name = 'UVGC_154_50',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_159_51 = Coupling(name = 'UVGC_159_51',
                       value = {-1:'(CKM1x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_159_52 = Coupling(name = 'UVGC_159_52',
                       value = {-1:'-(CKM1x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_160_53 = Coupling(name = 'UVGC_160_53',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_160_54 = Coupling(name = 'UVGC_160_54',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_161_55 = Coupling(name = 'UVGC_161_55',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_162_56 = Coupling(name = 'UVGC_162_56',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_163_57 = Coupling(name = 'UVGC_163_57',
                       value = {-1:'( -(CLq1x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq1x3*complex(0,1)*G**2*gL*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_163_58 = Coupling(name = 'UVGC_163_58',
                       value = {-1:'(CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_163_59 = Coupling(name = 'UVGC_163_59',
                       value = {-1:'-(CLq1x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_164_60 = Coupling(name = 'UVGC_164_60',
                       value = {-1:'( -(CLq2x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq2x3*complex(0,1)*G**2*gL*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_164_61 = Coupling(name = 'UVGC_164_61',
                       value = {-1:'(CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_164_62 = Coupling(name = 'UVGC_164_62',
                       value = {-1:'-(CLq2x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_165_63 = Coupling(name = 'UVGC_165_63',
                       value = {-1:'( -(CLq3x1*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq3x1*complex(0,1)*G**2*gL*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_165_64 = Coupling(name = 'UVGC_165_64',
                       value = {-1:'(CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_165_65 = Coupling(name = 'UVGC_165_65',
                       value = {-1:'-(CLq3x1*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_166_66 = Coupling(name = 'UVGC_166_66',
                       value = {-1:'( -(CLq3x2*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq3x2*complex(0,1)*G**2*gL*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_166_67 = Coupling(name = 'UVGC_166_67',
                       value = {-1:'(CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_166_68 = Coupling(name = 'UVGC_166_68',
                       value = {-1:'-(CLq3x2*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_167_69 = Coupling(name = 'UVGC_167_69',
                       value = {-1:'( -(CRq1x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq1x3*complex(0,1)*G**2*gR*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_167_70 = Coupling(name = 'UVGC_167_70',
                       value = {-1:'(CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_167_71 = Coupling(name = 'UVGC_167_71',
                       value = {-1:'-(CRq1x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_168_72 = Coupling(name = 'UVGC_168_72',
                       value = {-1:'( -(CRq2x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq2x3*complex(0,1)*G**2*gR*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_168_73 = Coupling(name = 'UVGC_168_73',
                       value = {-1:'(CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_168_74 = Coupling(name = 'UVGC_168_74',
                       value = {-1:'-(CRq2x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_169_75 = Coupling(name = 'UVGC_169_75',
                       value = {-1:'( -(CRq3x1*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq3x1*complex(0,1)*G**2*gR*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_169_76 = Coupling(name = 'UVGC_169_76',
                       value = {-1:'(CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_169_77 = Coupling(name = 'UVGC_169_77',
                       value = {-1:'-(CRq3x1*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_170_78 = Coupling(name = 'UVGC_170_78',
                       value = {-1:'( -(CRq3x2*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq3x2*complex(0,1)*G**2*gR*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_170_79 = Coupling(name = 'UVGC_170_79',
                       value = {-1:'(CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_170_80 = Coupling(name = 'UVGC_170_80',
                       value = {-1:'-(CRq3x2*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'NP':1,'QCD':2})

UVGC_171_81 = Coupling(name = 'UVGC_171_81',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_172_82 = Coupling(name = 'UVGC_172_82',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw)'},
                       order = {'QCD':2,'QED':1})

UVGC_173_83 = Coupling(name = 'UVGC_173_83',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_174_84 = Coupling(name = 'UVGC_174_84',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_175_85 = Coupling(name = 'UVGC_175_85',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_175_86 = Coupling(name = 'UVGC_175_86',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_176_87 = Coupling(name = 'UVGC_176_87',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(8.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_176_88 = Coupling(name = 'UVGC_176_88',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_176_89 = Coupling(name = 'UVGC_176_89',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(8.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_177_90 = Coupling(name = 'UVGC_177_90',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )',0:'( (complex(0,1)*G**3*reglog(MB/MU_R))/(24.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_177_91 = Coupling(name = 'UVGC_177_91',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_177_92 = Coupling(name = 'UVGC_177_92',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_177_93 = Coupling(name = 'UVGC_177_93',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_177_94 = Coupling(name = 'UVGC_177_94',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )',0:'( (complex(0,1)*G**3*reglog(MT/MU_R))/(24.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_177_95 = Coupling(name = 'UVGC_177_95',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_181_96 = Coupling(name = 'UVGC_181_96',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_182_97 = Coupling(name = 'UVGC_182_97',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_182_98 = Coupling(name = 'UVGC_182_98',
                       value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_99 = Coupling(name = 'UVGC_182_99',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_100 = Coupling(name = 'UVGC_182_100',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_183_101 = Coupling(name = 'UVGC_183_101',
                        value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_183_102 = Coupling(name = 'UVGC_183_102',
                        value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_184_103 = Coupling(name = 'UVGC_184_103',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_184_104 = Coupling(name = 'UVGC_184_104',
                        value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_184_105 = Coupling(name = 'UVGC_184_105',
                        value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_184_106 = Coupling(name = 'UVGC_184_106',
                        value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_184_107 = Coupling(name = 'UVGC_184_107',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_185_108 = Coupling(name = 'UVGC_185_108',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_185_109 = Coupling(name = 'UVGC_185_109',
                        value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_185_110 = Coupling(name = 'UVGC_185_110',
                        value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_185_111 = Coupling(name = 'UVGC_185_111',
                        value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_185_112 = Coupling(name = 'UVGC_185_112',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_186_113 = Coupling(name = 'UVGC_186_113',
                        value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_186_114 = Coupling(name = 'UVGC_186_114',
                        value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_187_115 = Coupling(name = 'UVGC_187_115',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_187_116 = Coupling(name = 'UVGC_187_116',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_187_117 = Coupling(name = 'UVGC_187_117',
                        value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_187_118 = Coupling(name = 'UVGC_187_118',
                        value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_187_119 = Coupling(name = 'UVGC_187_119',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_188_120 = Coupling(name = 'UVGC_188_120',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_189_121 = Coupling(name = 'UVGC_189_121',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_190_122 = Coupling(name = 'UVGC_190_122',
                        value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_191_123 = Coupling(name = 'UVGC_191_123',
                        value = {-1:'( -(CLq1x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq1x3*complex(0,1)*G**2*gL*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq1x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_192_124 = Coupling(name = 'UVGC_192_124',
                        value = {-1:'( -(CLq2x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq2x3*complex(0,1)*G**2*gL*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq2x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_193_125 = Coupling(name = 'UVGC_193_125',
                        value = {-1:'( -(CLq3x1*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq3x1*complex(0,1)*G**2*gL*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq3x1*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_194_126 = Coupling(name = 'UVGC_194_126',
                        value = {-1:'( -(CLq3x2*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq3x2*complex(0,1)*G**2*gL*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq3x2*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_195_127 = Coupling(name = 'UVGC_195_127',
                        value = {-1:'( -(CLq3x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq3x3*complex(0,1)*G**2*gL*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_195_128 = Coupling(name = 'UVGC_195_128',
                        value = {-1:'( -(CLq3x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CLq3x3*complex(0,1)*G**2*gL*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CLq3x3*complex(0,1)*G**2*gL)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_195_129 = Coupling(name = 'UVGC_195_129',
                        value = {-1:'-(CLq3x3*complex(0,1)*G**2*gL)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_196_130 = Coupling(name = 'UVGC_196_130',
                        value = {-1:'( -(CRq1x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq1x3*complex(0,1)*G**2*gR*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq1x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_197_131 = Coupling(name = 'UVGC_197_131',
                        value = {-1:'( -(CRq2x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq2x3*complex(0,1)*G**2*gR*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq2x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_198_132 = Coupling(name = 'UVGC_198_132',
                        value = {-1:'( -(CRq3x1*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq3x1*complex(0,1)*G**2*gR*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq3x1*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_199_133 = Coupling(name = 'UVGC_199_133',
                        value = {-1:'( -(CRq3x2*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq3x2*complex(0,1)*G**2*gR*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq3x2*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_200_134 = Coupling(name = 'UVGC_200_134',
                        value = {-1:'( -(CRq3x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq3x3*complex(0,1)*G**2*gR*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_200_135 = Coupling(name = 'UVGC_200_135',
                        value = {-1:'( -(CRq3x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CRq3x3*complex(0,1)*G**2*gR*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (CRq3x3*complex(0,1)*G**2*gR)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_200_136 = Coupling(name = 'UVGC_200_136',
                        value = {-1:'-(CRq3x3*complex(0,1)*G**2*gR)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'NP':1,'QCD':2})

UVGC_201_137 = Coupling(name = 'UVGC_201_137',
                        value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_202_138 = Coupling(name = 'UVGC_202_138',
                        value = {-1:'( -(CKM3x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM3x3*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_202_139 = Coupling(name = 'UVGC_202_139',
                        value = {-1:'( -(CKM3x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM3x3*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_202_140 = Coupling(name = 'UVGC_202_140',
                        value = {-1:'-(CKM3x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_203_141 = Coupling(name = 'UVGC_203_141',
                        value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_204_142 = Coupling(name = 'UVGC_204_142',
                        value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_205_143 = Coupling(name = 'UVGC_205_143',
                        value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_206_144 = Coupling(name = 'UVGC_206_144',
                        value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3)*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_206_145 = Coupling(name = 'UVGC_206_145',
                        value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3)*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_206_146 = Coupling(name = 'UVGC_206_146',
                        value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

