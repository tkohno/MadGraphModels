# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 10.3.1 for Mac OS X x86 (64-bit) (December 9, 2015)
# Date: Tue 31 Oct 2017 23:11:37


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

OcA = CouplingOrder(name = 'OcA',
                    expansion_order = 99,
                    hierarchy = 1)

OcZpH = CouplingOrder(name = 'OcZpH',
                      expansion_order = 99,
                      hierarchy = 1)

OgZp = CouplingOrder(name = 'OgZp',
                     expansion_order = 99,
                     hierarchy = 1)

Osh = CouplingOrder(name = 'Osh',
                    expansion_order = 99,
                    hierarchy = 1)

Ozq = CouplingOrder(name = 'Ozq',
                    expansion_order = 99,
                    hierarchy = 1)

