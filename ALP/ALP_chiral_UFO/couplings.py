# This file was automatically created by FeynRules 2.3.26
# Mathematica version: 11.0.1 for Linux x86 (64-bit) (September 21, 2016)
# Date: Wed 20 Sep 2017 14:51:31


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '(CGtil*complex(0,1))/(2.*fa)',
                order = {'NP':1})

GC_7 = Coupling(name = 'GC_7',
                value = '(2*CWtil*complex(0,1))/fa',
                order = {'NP':1})

GC_8 = Coupling(name = 'GC_8',
                value = '(4*CWtil*ee*complex(0,1))/fa',
                order = {'NP':1,'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = '-(complex(0,1)*G)',
                order = {'QCD':1})

GC_10 = Coupling(name = 'GC_10',
                 value = 'G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-((CGtil*G)/fa)',
                 order = {'NP':1,'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '(-2*aD*C2D*I1a11*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '(-2*aD*C2D*I1a12*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '(-2*aD*C2D*I1a13*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(-2*aD*C2D*I1a21*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '(-2*aD*C2D*I1a22*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '(-2*aD*C2D*I1a23*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(-2*aD*C2D*I1a31*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '(-2*aD*C2D*I1a32*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '(-2*aD*C2D*I1a33*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '(2*aD*C2D*I2a11*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '(2*aD*C2D*I2a12*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '(2*aD*C2D*I2a13*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '(2*aD*C2D*I2a21*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '(2*aD*C2D*I2a22*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '(2*aD*C2D*I2a23*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(2*aD*C2D*I2a31*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(2*aD*C2D*I2a32*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '(2*aD*C2D*I2a33*cmath.sqrt(2))/fa',
                 order = {'NP':1,'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-(C2*ee**2*complex(0,1))/(8.*cw*fa*cmath.pi) - (C7*ee**2*complex(0,1))/(4.*cw*fa*cmath.pi) - (3*C2*cw*ee**2*complex(0,1))/(8.*fa*cmath.pi*sw**2) - (C7*cw*ee**2*complex(0,1))/(4.*fa*cmath.pi*sw**2)',
                 order = {'NP':1,'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '-(C5*cw*ee**3)/(16.*fa*cmath.pi**2*sw**3) - (C5*ee**3)/(16.*cw*fa*cmath.pi**2*sw)',
                 order = {'NP':1,'QED':3})

GC_34 = Coupling(name = 'GC_34',
                 value = '-(C4*cw*ee**3)/(32.*fa*cmath.pi**2*sw**3) - (C4*ee**3)/(32.*cw*fa*cmath.pi**2*sw) + (C8*ee**3)/(16.*cw*fa*cmath.pi**2*sw)',
                 order = {'NP':1,'QED':3})

GC_35 = Coupling(name = 'GC_35',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '(C8*ee**2)/(16.*fa*cmath.pi**2*sw**2)',
                 order = {'NP':1,'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-(C8*ee**3)/(16.*fa*cmath.pi**2*sw**2)',
                 order = {'NP':1,'QED':3})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(C6*cw*ee**2)/(2.*fa*cmath.pi*sw**2)',
                 order = {'NP':1,'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '-((ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-((CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-((CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-((CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-((CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-((CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-((CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-((CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-((CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '-((CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2)))',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '-((cw*ee*complex(0,1))/sw)',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '(4*cw*CWtil*ee*complex(0,1))/(fa*sw)',
                 order = {'NP':1,'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(C2*ee*complex(0,1))/(8.*fa*cmath.pi*sw)',
                 order = {'NP':1,'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(C6*ee)/(4.*fa*cmath.pi*sw)',
                 order = {'NP':1,'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-(C2*ee**2*complex(0,1))/(4.*fa*cmath.pi*sw)',
                 order = {'NP':1,'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '(C6*ee**2)/(4.*fa*cmath.pi*sw)',
                 order = {'NP':1,'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '(ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-(ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(-2*CBtil*cw*complex(0,1)*sw)/fa + (2*cw*CWtil*complex(0,1)*sw)/fa',
                 order = {'NP':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(2*b2D*C2D*cw*ee)/(fa*sw) + (2*b2D*C2D*ee*sw)/(cw*fa)',
                 order = {'NP':1,'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '(2*cw**2*CWtil*complex(0,1))/fa + (2*CBtil*complex(0,1)*sw**2)/fa',
                 order = {'NP':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(2*CBtil*cw**2*complex(0,1))/fa + (2*CWtil*complex(0,1)*sw**2)/fa',
                 order = {'NP':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(C1*cw*ee*complex(0,1))/(4.*fa*cmath.pi) - (C2*cw**2*ee*complex(0,1))/(8.*fa*cmath.pi*sw) - (C7*cw**2*ee*complex(0,1))/(4.*fa*cmath.pi*sw) - (C2*ee*complex(0,1)*sw)/(8.*fa*cmath.pi) - (C7*ee*complex(0,1)*sw)/(4.*fa*cmath.pi) + (C1*ee*complex(0,1)*sw**2)/(4.*cw*fa*cmath.pi)',
                 order = {'NP':1,'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '-(C2*cw*ee*complex(0,1))/(8.*fa*cmath.pi) - (C7*cw*ee*complex(0,1))/(4.*fa*cmath.pi) - (C1*cw**2*ee*complex(0,1))/(4.*fa*cmath.pi*sw) - (C1*ee*complex(0,1)*sw)/(4.*fa*cmath.pi) - (C2*ee*complex(0,1)*sw**2)/(8.*cw*fa*cmath.pi) - (C7*ee*complex(0,1)*sw**2)/(4.*cw*fa*cmath.pi)',
                 order = {'NP':1,'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(C4*cw**3*ee**3)/(16.*fa*cmath.pi**2*sw**3) - (C5*cw**3*ee**3)/(16.*fa*cmath.pi**2*sw**3) - (C9*cw**3*ee**3)/(8.*fa*cmath.pi**2*sw**3) - (3*C4*cw*ee**3)/(16.*fa*cmath.pi**2*sw) - (3*C5*cw*ee**3)/(16.*fa*cmath.pi**2*sw) - (3*C9*cw*ee**3)/(8.*fa*cmath.pi**2*sw) - (3*C4*ee**3*sw)/(16.*cw*fa*cmath.pi**2) - (3*C5*ee**3*sw)/(16.*cw*fa*cmath.pi**2) - (3*C9*ee**3*sw)/(8.*cw*fa*cmath.pi**2) - (C4*ee**3*sw**3)/(16.*cw**3*fa*cmath.pi**2) - (C5*ee**3*sw**3)/(16.*cw**3*fa*cmath.pi**2) - (C9*ee**3*sw**3)/(8.*cw**3*fa*cmath.pi**2)',
                 order = {'NP':1,'QED':3})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(b11*C11*cw*ee)/(8.*fa*cmath.pi**2*sw*vev**2) - (b11*C11*ee*sw)/(8.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_72 = Coupling(name = 'GC_72',
                 value = '-(b12*C12*cw*ee)/(8.*fa*cmath.pi**2*sw*vev**2) - (b12*C12*ee*sw)/(8.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(b13*C13*cw*ee)/(8.*fa*cmath.pi**2*sw*vev**2) - (b13*C13*ee*sw)/(8.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_74 = Coupling(name = 'GC_74',
                 value = '-(b14*C14*cw*ee)/(8.*fa*cmath.pi**2*sw*vev**2) - (b14*C14*ee*sw)/(8.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(a15*a15prime*C15*cw*ee)/(2.*fa*cmath.pi**2*sw*vev**2) - (a15*a15prime*C15*ee*sw)/(2.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(a16*a16prime*C16*cw*ee)/(4.*fa*cmath.pi**2*sw*vev**2) - (a16*a16prime*C16*ee*sw)/(4.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_77 = Coupling(name = 'GC_77',
                 value = '-(b17*C17*cw*ee)/(8.*fa*cmath.pi**2*sw*vev**2) - (b17*C17*ee*sw)/(8.*cw*fa*cmath.pi**2*vev**2)',
                 order = {'NP':1,'QED':3})

GC_78 = Coupling(name = 'GC_78',
                 value = '(b3*C3*cw)/(2.*fa*cmath.pi*vev**2) + (b10*C10*sw)/(2.*fa*cmath.pi*vev**2)',
                 order = {'NP':1,'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(b10*C10*cw)/(2.*fa*cmath.pi*vev**2) - (b3*C3*sw)/(2.*fa*cmath.pi*vev**2)',
                 order = {'NP':1,'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(a11*C11*cw*ee)/(8.*fa*cmath.pi**2*sw*vev) - (a11*C11*ee*sw)/(8.*cw*fa*cmath.pi**2*vev)',
                 order = {'NP':1,'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = '-(a12*C12*cw*ee)/(8.*fa*cmath.pi**2*sw*vev) - (a12*C12*ee*sw)/(8.*cw*fa*cmath.pi**2*vev)',
                 order = {'NP':1,'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = '-(a13*C13*cw*ee)/(8.*fa*cmath.pi**2*sw*vev) - (a13*C13*ee*sw)/(8.*cw*fa*cmath.pi**2*vev)',
                 order = {'NP':1,'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '-(a14*C14*cw*ee)/(8.*fa*cmath.pi**2*sw*vev) - (a14*C14*ee*sw)/(8.*cw*fa*cmath.pi**2*vev)',
                 order = {'NP':1,'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '-(a17*C17*cw*ee)/(8.*fa*cmath.pi**2*sw*vev) - (a17*C17*ee*sw)/(8.*cw*fa*cmath.pi**2*vev)',
                 order = {'NP':1,'QED':2})

GC_85 = Coupling(name = 'GC_85',
                 value = '(a3*C3*cw)/(2.*fa*cmath.pi*vev) + (a10*C10*sw)/(2.*fa*cmath.pi*vev)',
                 order = {'NP':1,'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(a10*C10*cw)/(2.*fa*cmath.pi*vev) - (a3*C3*sw)/(2.*fa*cmath.pi*vev)',
                 order = {'NP':1,'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '(a1*C1*cw*ee*complex(0,1))/(2.*fa*cmath.pi*vev) - (a2*C2*cw**2*ee*complex(0,1))/(4.*fa*cmath.pi*sw*vev) - (a7*C7*cw**2*ee*complex(0,1))/(2.*fa*cmath.pi*sw*vev) - (a2*C2*ee*complex(0,1)*sw)/(4.*fa*cmath.pi*vev) - (a7*C7*ee*complex(0,1)*sw)/(2.*fa*cmath.pi*vev) + (a1*C1*ee*complex(0,1)*sw**2)/(2.*cw*fa*cmath.pi*vev)',
                 order = {'NP':1,'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '-(a2*C2*cw*ee*complex(0,1))/(4.*fa*cmath.pi*vev) - (a7*C7*cw*ee*complex(0,1))/(2.*fa*cmath.pi*vev) - (a1*C1*cw**2*ee*complex(0,1))/(2.*fa*cmath.pi*sw*vev) - (a1*C1*ee*complex(0,1)*sw)/(2.*fa*cmath.pi*vev) - (a2*C2*ee*complex(0,1)*sw**2)/(4.*cw*fa*cmath.pi*vev) - (a7*C7*ee*complex(0,1)*sw**2)/(2.*cw*fa*cmath.pi*vev)',
                 order = {'NP':1,'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '(aD*C17*I1a11)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_90 = Coupling(name = 'GC_90',
                 value = '(aD*C17*I1a12)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_91 = Coupling(name = 'GC_91',
                 value = '(aD*C17*I1a13)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_92 = Coupling(name = 'GC_92',
                 value = '(aD*C17*I1a21)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_93 = Coupling(name = 'GC_93',
                 value = '(aD*C17*I1a22)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_94 = Coupling(name = 'GC_94',
                 value = '(aD*C17*I1a23)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_95 = Coupling(name = 'GC_95',
                 value = '(aD*C17*I1a31)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_96 = Coupling(name = 'GC_96',
                 value = '(aD*C17*I1a32)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_97 = Coupling(name = 'GC_97',
                 value = '(aD*C17*I1a33)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_98 = Coupling(name = 'GC_98',
                 value = '-(aD*C17*I2a11)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_99 = Coupling(name = 'GC_99',
                 value = '-(aD*C17*I2a12)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                 order = {'NP':1,'QED':3})

GC_100 = Coupling(name = 'GC_100',
                  value = '-(aD*C17*I2a13)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_101 = Coupling(name = 'GC_101',
                  value = '-(aD*C17*I2a21)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_102 = Coupling(name = 'GC_102',
                  value = '-(aD*C17*I2a22)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_103 = Coupling(name = 'GC_103',
                  value = '-(aD*C17*I2a23)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_104 = Coupling(name = 'GC_104',
                  value = '-(aD*C17*I2a31)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(aD*C17*I2a32)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_106 = Coupling(name = 'GC_106',
                  value = '-(aD*C17*I2a33)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_107 = Coupling(name = 'GC_107',
                  value = '(C17*I1a11)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = '(C17*I1a12)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = '(C17*I1a13)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '(C17*I1a21)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(C17*I1a22)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '(C17*I1a23)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '(C17*I1a31)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '(C17*I1a32)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '(C17*I1a33)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(C17*I2a11)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '-(C17*I2a12)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(C17*I2a13)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(C17*I2a21)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(C17*I2a22)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(C17*I2a23)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '-(C17*I2a31)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(C17*I2a32)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(C17*I2a33)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = '(a8*C8*ee**2)/(8.*fa*cmath.pi**2*sw**2*vev)',
                  order = {'NP':1,'QED':3})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(a10*C10*ee)/(2.*fa*cmath.pi*sw*vev)',
                  order = {'NP':1,'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(a2*C2*ee*complex(0,1))/(4.*fa*cmath.pi*sw*vev)',
                  order = {'NP':1,'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = '(a6*C6*ee)/(2.*fa*cmath.pi*sw*vev)',
                  order = {'NP':1,'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '-((C2D*I1a11*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '-((C2D*I1a12*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-((C2D*I1a13*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-((C2D*I1a21*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '-((C2D*I1a22*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-((C2D*I1a23*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '-((C2D*I1a31*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-((C2D*I1a32*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-((C2D*I1a33*vev*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '(C2D*I2a11*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '(C2D*I2a12*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '(C2D*I2a13*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '(C2D*I2a21*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '(C2D*I2a22*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '(C2D*I2a23*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '(C2D*I2a31*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '(C2D*I2a32*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '(C2D*I2a33*vev*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '(2*a2D*C2D*cw*ee*vev)/(fa*sw) + (2*a2D*C2D*ee*sw*vev)/(cw*fa)',
                  order = {'NP':1})

GC_150 = Coupling(name = 'GC_150',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-((complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(2*aU*C2D*yc*cmath.sqrt(2))/fa',
                  order = {'NP':1,'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(aU*C17*yc)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_155 = Coupling(name = 'GC_155',
                  value = '-(C17*yc)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '(C2D*vev*yc*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '-((complex(0,1)*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '-((complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '(-2*aL*C2D*ye*cmath.sqrt(2))/fa',
                  order = {'NP':1,'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '(aL*C17*ye)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_161 = Coupling(name = 'GC_161',
                  value = '(C17*ye)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = '-((C2D*vev*ye*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '-((complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '(-2*aL*C2D*ym*cmath.sqrt(2))/fa',
                  order = {'NP':1,'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '(aL*C17*ym)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_166 = Coupling(name = 'GC_166',
                  value = '(C17*ym)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '-((C2D*vev*ym*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '-((complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '(2*aU*C2D*yt*cmath.sqrt(2))/fa',
                  order = {'NP':1,'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '-(aU*C17*yt)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_172 = Coupling(name = 'GC_172',
                  value = '-(C17*yt)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_173 = Coupling(name = 'GC_173',
                  value = '(C2D*vev*yt*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '(-2*aL*C2D*ytau*cmath.sqrt(2))/fa',
                  order = {'NP':1,'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '(aL*C17*ytau)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_177 = Coupling(name = 'GC_177',
                  value = '(C17*ytau)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_178 = Coupling(name = 'GC_178',
                  value = '-((C2D*vev*ytau*cmath.sqrt(2))/fa)',
                  order = {'NP':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-((complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '(2*aU*C2D*yup*cmath.sqrt(2))/fa',
                  order = {'NP':1,'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-(aU*C17*yup)/(4.*fa*cmath.pi**2*vev**2*cmath.sqrt(2))',
                  order = {'NP':1,'QED':3})

GC_182 = Coupling(name = 'GC_182',
                  value = '-(C17*yup)/(8.*fa*cmath.pi**2*vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = '(C2D*vev*yup*cmath.sqrt(2))/fa',
                  order = {'NP':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '-((ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

