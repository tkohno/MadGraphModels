(* ************************** *)
(* *****  Information   ***** *)
(* ************************** *)

M$ModelName = "CHresonance_neutral_scalar";

(* M$InteractionOrderHierarchy = { *)
(*   {NP , 1},                     *)
(*   {QCD, 1},                     *)
(*   {QED, 2}                      *)
(* };                              *)

M$ClassesDescription = {
  S[4] == {
    ClassName     -> s0,
    SelfConjugate -> True,
    Mass          -> {Ms0, 1000.0},
    Width         -> {Ws0, 1.0},
    ParticleName  -> "s0"}
}

M$Parameters = {
  keta == {
    ParameterType    -> External,
    Value            -> 1.0,
    InteractionOrder -> {NP,1},
    Description      -> "Scalar cubic coupling times (v/f)"},
  gweak == {
    ParameterType    -> Internal,
    Value            -> gw,
    InteractionOrder -> {QED,0},
    Description      -> "gw at QED=0 order"},
  vweak == {
    ParameterType    -> Internal,
    Value            -> vev,
    InteractionOrder -> {QED,0},
    Description      -> "vev at QED=0 order"}
}

Lscalar := 1/2 del[s0, mu] del[s0, mu] - 1/2 Ms0^2 s0^2 + keta gweak MW W[mu] Wbar[mu] s0 + keta gweak (1/2) (MZ/cw) Z[mu] Z[mu] s0 + keta (1/vweak) del[H, mu] del[H, mu] s0
