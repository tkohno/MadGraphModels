Requestor: Quanyin Li
Contents: Fractionally charged particles with 2/3 and 1/3 charge for DY production
Description: https://its.cern.ch/jira/secure/attachment/249932/249932_Fractional_charge_model.pdf
Source: Guido Gagliardi (ATLAS member who produced the UFO)
JIRA: https://its.cern.ch/jira/browse/AGENE-1895
Update: Same JIRA