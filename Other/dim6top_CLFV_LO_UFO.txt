Requestor: Carlo A. Gottardo
Contents: Modified version of the dim6top UFO model including only the charged lepton-flavour operators listed in 10.1007/JHEP04(2019)014, needed for the decay t->ll'q with l,l' = {e, mu, tau}, q={u,c}. Restrictions cards to turn on one operator at a time are included.
Paper: 10.1007/JHEP04(2019)014
Source: https://gitlab.cern.ch/cgottard/dim6top_clfv
Jira: none, added via merge request. 
