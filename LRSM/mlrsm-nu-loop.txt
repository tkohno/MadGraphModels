Requestor: Blaz Leban
Content: Complete and fully NLO-consistent implementation of the Minimal Left-Right Symmetric Model
Paper: https://arxiv.org/abs/2403.07756
Webpage 1: https://sites.google.com/site/leftrighthep/1-lrsm-feynrules
Webpage 2: https://feynrules.irmp.ucl.ac.be/wiki/LRSM_NLO
