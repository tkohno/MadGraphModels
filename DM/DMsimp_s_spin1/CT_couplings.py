# This file was automatically created by FeynRules 2.3.7
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Fri 23 Sep 2016 16:59:06


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_109_1 = Coupling(name = 'R2GC_109_1',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_110_2 = Coupling(name = 'R2GC_110_2',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_111_3 = Coupling(name = 'R2GC_111_3',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_112_4 = Coupling(name = 'R2GC_112_4',
                      value = '(complex(0,1)*G**2*gAd33)/(6.*cmath.pi**2)',
                      order = {'DMV':1,'QCD':2})

R2GC_113_5 = Coupling(name = 'R2GC_113_5',
                      value = '-(complex(0,1)*G**2*gVd33)/(6.*cmath.pi**2)',
                      order = {'DMV':1,'QCD':2})

R2GC_114_6 = Coupling(name = 'R2GC_114_6',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_115_7 = Coupling(name = 'R2GC_115_7',
                      value = '(complex(0,1)*G**2*gAu22)/(6.*cmath.pi**2)',
                      order = {'DMV':1,'QCD':2})

R2GC_116_8 = Coupling(name = 'R2GC_116_8',
                      value = '-(complex(0,1)*G**2*gVu22)/(6.*cmath.pi**2)',
                      order = {'DMV':1,'QCD':2})

R2GC_117_9 = Coupling(name = 'R2GC_117_9',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_118_10 = Coupling(name = 'R2GC_118_10',
                       value = '(complex(0,1)*G**2*gAd11)/(6.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2})

R2GC_119_11 = Coupling(name = 'R2GC_119_11',
                       value = '-(complex(0,1)*G**2*gVd11)/(6.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2})

R2GC_121_12 = Coupling(name = 'R2GC_121_12',
                       value = '(complex(0,1)*G**2*gAd22)/(6.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2})

R2GC_122_13 = Coupling(name = 'R2GC_122_13',
                       value = '-(complex(0,1)*G**2*gVd22)/(6.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2})

R2GC_125_14 = Coupling(name = 'R2GC_125_14',
                       value = '(complex(0,1)*G**2*gAu11)/(6.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2})

R2GC_126_15 = Coupling(name = 'R2GC_126_15',
                       value = '-(complex(0,1)*G**2*gVu11)/(6.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2})

R2GC_128_16 = Coupling(name = 'R2GC_128_16',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_128_17 = Coupling(name = 'R2GC_128_17',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_129_18 = Coupling(name = 'R2GC_129_18',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_129_19 = Coupling(name = 'R2GC_129_19',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_130_20 = Coupling(name = 'R2GC_130_20',
                       value = '(complex(0,1)*G**3*gAd33)/(16.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_130_21 = Coupling(name = 'R2GC_130_21',
                       value = '(complex(0,1)*G**3*gAu22)/(16.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_130_22 = Coupling(name = 'R2GC_130_22',
                       value = '(complex(0,1)*G**3*gAd11)/(16.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_130_23 = Coupling(name = 'R2GC_130_23',
                       value = '(complex(0,1)*G**3*gAd22)/(16.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_130_24 = Coupling(name = 'R2GC_130_24',
                       value = '(complex(0,1)*G**3*gAu33)/(16.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_130_25 = Coupling(name = 'R2GC_130_25',
                       value = '(complex(0,1)*G**3*gAu11)/(16.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_131_26 = Coupling(name = 'R2GC_131_26',
                       value = '-(ee*complex(0,1)*G**2*gVd33)/(72.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_131_27 = Coupling(name = 'R2GC_131_27',
                       value = '(ee*complex(0,1)*G**2*gVu22)/(36.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_131_28 = Coupling(name = 'R2GC_131_28',
                       value = '-(ee*complex(0,1)*G**2*gVd11)/(72.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_131_29 = Coupling(name = 'R2GC_131_29',
                       value = '-(ee*complex(0,1)*G**2*gVd22)/(72.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_131_30 = Coupling(name = 'R2GC_131_30',
                       value = '(ee*complex(0,1)*G**2*gVu33)/(36.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_131_31 = Coupling(name = 'R2GC_131_31',
                       value = '(ee*complex(0,1)*G**2*gVu11)/(36.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_132_32 = Coupling(name = 'R2GC_132_32',
                       value = '(complex(0,1)*G**3*gVd33)/(48.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_132_33 = Coupling(name = 'R2GC_132_33',
                       value = '(complex(0,1)*G**3*gVu22)/(48.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_132_34 = Coupling(name = 'R2GC_132_34',
                       value = '(complex(0,1)*G**3*gVd11)/(48.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_132_35 = Coupling(name = 'R2GC_132_35',
                       value = '(complex(0,1)*G**3*gVd22)/(48.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_132_36 = Coupling(name = 'R2GC_132_36',
                       value = '(complex(0,1)*G**3*gVu33)/(48.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_132_37 = Coupling(name = 'R2GC_132_37',
                       value = '(complex(0,1)*G**3*gVu11)/(48.*cmath.pi**2)',
                       order = {'DMV':1,'QCD':3})

R2GC_133_38 = Coupling(name = 'R2GC_133_38',
                       value = '(complex(0,1)*G**2*gAd33**2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gVd33**2)/(24.*cmath.pi**2)',
                       order = {'DMV':2,'QCD':2})

R2GC_133_39 = Coupling(name = 'R2GC_133_39',
                       value = '(complex(0,1)*G**2*gAu22**2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gVu22**2)/(24.*cmath.pi**2)',
                       order = {'DMV':2,'QCD':2})

R2GC_133_40 = Coupling(name = 'R2GC_133_40',
                       value = '(complex(0,1)*G**2*gAd11**2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gVd11**2)/(24.*cmath.pi**2)',
                       order = {'DMV':2,'QCD':2})

R2GC_133_41 = Coupling(name = 'R2GC_133_41',
                       value = '(complex(0,1)*G**2*gAd22**2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gVd22**2)/(24.*cmath.pi**2)',
                       order = {'DMV':2,'QCD':2})

R2GC_133_42 = Coupling(name = 'R2GC_133_42',
                       value = '(complex(0,1)*G**2*gAu33**2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gVu33**2)/(24.*cmath.pi**2)',
                       order = {'DMV':2,'QCD':2})

R2GC_133_43 = Coupling(name = 'R2GC_133_43',
                       value = '(complex(0,1)*G**2*gAu11**2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gVu11**2)/(24.*cmath.pi**2)',
                       order = {'DMV':2,'QCD':2})

R2GC_134_44 = Coupling(name = 'R2GC_134_44',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_134_45 = Coupling(name = 'R2GC_134_45',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_135_46 = Coupling(name = 'R2GC_135_46',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_135_47 = Coupling(name = 'R2GC_135_47',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_136_48 = Coupling(name = 'R2GC_136_48',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_136_49 = Coupling(name = 'R2GC_136_49',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_137_50 = Coupling(name = 'R2GC_137_50',
                       value = '(cw*ee*complex(0,1)*G**2*gAd33)/(96.*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2*gVd33)/(96.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*gAd33*sw)/(96.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*gVd33*sw)/(288.*cw*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_137_51 = Coupling(name = 'R2GC_137_51',
                       value = '-(cw*ee*complex(0,1)*G**2*gAu22)/(96.*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2*gVu22)/(96.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*gAu22*sw)/(96.*cw*cmath.pi**2) - (5*ee*complex(0,1)*G**2*gVu22*sw)/(288.*cw*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_137_52 = Coupling(name = 'R2GC_137_52',
                       value = '(cw*ee*complex(0,1)*G**2*gAd11)/(96.*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2*gVd11)/(96.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*gAd11*sw)/(96.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*gVd11*sw)/(288.*cw*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_137_53 = Coupling(name = 'R2GC_137_53',
                       value = '(cw*ee*complex(0,1)*G**2*gAd22)/(96.*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2*gVd22)/(96.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*gAd22*sw)/(96.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*gVd22*sw)/(288.*cw*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_137_54 = Coupling(name = 'R2GC_137_54',
                       value = '-(cw*ee*complex(0,1)*G**2*gAu33)/(96.*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2*gVu33)/(96.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*gAu33*sw)/(96.*cw*cmath.pi**2) - (5*ee*complex(0,1)*G**2*gVu33*sw)/(288.*cw*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_137_55 = Coupling(name = 'R2GC_137_55',
                       value = '-(cw*ee*complex(0,1)*G**2*gAu11)/(96.*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2*gVu11)/(96.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*gAu11*sw)/(96.*cw*cmath.pi**2) - (5*ee*complex(0,1)*G**2*gVu11*sw)/(288.*cw*cmath.pi**2)',
                       order = {'DMV':1,'QCD':2,'QED':1})

R2GC_138_56 = Coupling(name = 'R2GC_138_56',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_138_57 = Coupling(name = 'R2GC_138_57',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_139_58 = Coupling(name = 'R2GC_139_58',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_140_59 = Coupling(name = 'R2GC_140_59',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_144_60 = Coupling(name = 'R2GC_144_60',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_61 = Coupling(name = 'R2GC_144_61',
                       value = '(CKM2x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_62 = Coupling(name = 'R2GC_144_62',
                       value = '(CKM2x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_63 = Coupling(name = 'R2GC_144_63',
                       value = '(CKM1x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_64 = Coupling(name = 'R2GC_144_64',
                       value = '(CKM1x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_157_65 = Coupling(name = 'R2GC_157_65',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_157_66 = Coupling(name = 'R2GC_157_66',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_158_67 = Coupling(name = 'R2GC_158_67',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_158_68 = Coupling(name = 'R2GC_158_68',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_159_69 = Coupling(name = 'R2GC_159_69',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_159_70 = Coupling(name = 'R2GC_159_70',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_160_71 = Coupling(name = 'R2GC_160_71',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_72 = Coupling(name = 'R2GC_161_72',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_73 = Coupling(name = 'R2GC_161_73',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_162_74 = Coupling(name = 'R2GC_162_74',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_162_75 = Coupling(name = 'R2GC_162_75',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_163_76 = Coupling(name = 'R2GC_163_76',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_163_77 = Coupling(name = 'R2GC_163_77',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_164_78 = Coupling(name = 'R2GC_164_78',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_165_79 = Coupling(name = 'R2GC_165_79',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_167_80 = Coupling(name = 'R2GC_167_80',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_169_81 = Coupling(name = 'R2GC_169_81',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_186_82 = Coupling(name = 'R2GC_186_82',
                       value = '-(CKM2x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_187_83 = Coupling(name = 'R2GC_187_83',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_188_84 = Coupling(name = 'R2GC_188_84',
                       value = '-(CKM2x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_189_85 = Coupling(name = 'R2GC_189_85',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_190_86 = Coupling(name = 'R2GC_190_86',
                       value = '-(CKM1x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_191_87 = Coupling(name = 'R2GC_191_87',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_192_88 = Coupling(name = 'R2GC_192_88',
                       value = '-(CKM1x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_193_89 = Coupling(name = 'R2GC_193_89',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_194_90 = Coupling(name = 'R2GC_194_90',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_194_91 = Coupling(name = 'R2GC_194_91',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_195_92 = Coupling(name = 'R2GC_195_92',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_196_93 = Coupling(name = 'R2GC_196_93',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_196_94 = Coupling(name = 'R2GC_196_94',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_197_95 = Coupling(name = 'R2GC_197_95',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_197_96 = Coupling(name = 'R2GC_197_96',
                       value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_198_97 = Coupling(name = 'R2GC_198_97',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_199_98 = Coupling(name = 'R2GC_199_98',
                       value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_200_99 = Coupling(name = 'R2GC_200_99',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_201_100 = Coupling(name = 'R2GC_201_100',
                        value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_202_101 = Coupling(name = 'R2GC_202_101',
                        value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_206_102 = Coupling(name = 'R2GC_206_102',
                        value = '(complex(0,1)*G**2*gAu33)/(6.*cmath.pi**2)',
                        order = {'DMV':1,'QCD':2})

R2GC_207_103 = Coupling(name = 'R2GC_207_103',
                        value = '-(complex(0,1)*G**2*gVu33)/(6.*cmath.pi**2)',
                        order = {'DMV':1,'QCD':2})

R2GC_208_104 = Coupling(name = 'R2GC_208_104',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_209_105 = Coupling(name = 'R2GC_209_105',
                        value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_212_106 = Coupling(name = 'R2GC_212_106',
                        value = '(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_213_107 = Coupling(name = 'R2GC_213_107',
                        value = '-(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_214_108 = Coupling(name = 'R2GC_214_108',
                        value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_215_109 = Coupling(name = 'R2GC_215_109',
                        value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_145_1 = Coupling(name = 'UVGC_145_1',
                      value = {-1:'(51*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_146_2 = Coupling(name = 'UVGC_146_2',
                      value = {-1:'G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_147_3 = Coupling(name = 'UVGC_147_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_148_4 = Coupling(name = 'UVGC_148_4',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_150_5 = Coupling(name = 'UVGC_150_5',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_157_6 = Coupling(name = 'UVGC_157_6',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_157_7 = Coupling(name = 'UVGC_157_7',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_158_8 = Coupling(name = 'UVGC_158_8',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_158_9 = Coupling(name = 'UVGC_158_9',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_160_10 = Coupling(name = 'UVGC_160_10',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_160_11 = Coupling(name = 'UVGC_160_11',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_161_12 = Coupling(name = 'UVGC_161_12',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_161_13 = Coupling(name = 'UVGC_161_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_162_14 = Coupling(name = 'UVGC_162_14',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_162_15 = Coupling(name = 'UVGC_162_15',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_163_16 = Coupling(name = 'UVGC_163_16',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_163_17 = Coupling(name = 'UVGC_163_17',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_164_18 = Coupling(name = 'UVGC_164_18',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_165_19 = Coupling(name = 'UVGC_165_19',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_166_20 = Coupling(name = 'UVGC_166_20',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_166_21 = Coupling(name = 'UVGC_166_21',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_166_22 = Coupling(name = 'UVGC_166_22',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_166_23 = Coupling(name = 'UVGC_166_23',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_166_24 = Coupling(name = 'UVGC_166_24',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_167_25 = Coupling(name = 'UVGC_167_25',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_169_26 = Coupling(name = 'UVGC_169_26',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_186_27 = Coupling(name = 'UVGC_186_27',
                       value = {-1:'(CKM2x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_186_28 = Coupling(name = 'UVGC_186_28',
                       value = {-1:'-(CKM2x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_187_29 = Coupling(name = 'UVGC_187_29',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_187_30 = Coupling(name = 'UVGC_187_30',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_188_31 = Coupling(name = 'UVGC_188_31',
                       value = {-1:'(CKM2x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_188_32 = Coupling(name = 'UVGC_188_32',
                       value = {-1:'-(CKM2x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_189_33 = Coupling(name = 'UVGC_189_33',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_189_34 = Coupling(name = 'UVGC_189_34',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_190_35 = Coupling(name = 'UVGC_190_35',
                       value = {-1:'(CKM1x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_190_36 = Coupling(name = 'UVGC_190_36',
                       value = {-1:'-(CKM1x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_191_37 = Coupling(name = 'UVGC_191_37',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_191_38 = Coupling(name = 'UVGC_191_38',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_192_39 = Coupling(name = 'UVGC_192_39',
                       value = {-1:'(CKM1x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_192_40 = Coupling(name = 'UVGC_192_40',
                       value = {-1:'-(CKM1x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_193_41 = Coupling(name = 'UVGC_193_41',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_193_42 = Coupling(name = 'UVGC_193_42',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_194_43 = Coupling(name = 'UVGC_194_43',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_195_44 = Coupling(name = 'UVGC_195_44',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_195_45 = Coupling(name = 'UVGC_195_45',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_195_46 = Coupling(name = 'UVGC_195_46',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_196_47 = Coupling(name = 'UVGC_196_47',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_196_48 = Coupling(name = 'UVGC_196_48',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_197_49 = Coupling(name = 'UVGC_197_49',
                       value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_197_50 = Coupling(name = 'UVGC_197_50',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_197_51 = Coupling(name = 'UVGC_197_51',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_198_52 = Coupling(name = 'UVGC_198_52',
                       value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_198_53 = Coupling(name = 'UVGC_198_53',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_199_54 = Coupling(name = 'UVGC_199_54',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_199_55 = Coupling(name = 'UVGC_199_55',
                       value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_199_56 = Coupling(name = 'UVGC_199_56',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_199_57 = Coupling(name = 'UVGC_199_57',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_200_58 = Coupling(name = 'UVGC_200_58',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_200_59 = Coupling(name = 'UVGC_200_59',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_200_60 = Coupling(name = 'UVGC_200_60',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_200_61 = Coupling(name = 'UVGC_200_61',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_201_62 = Coupling(name = 'UVGC_201_62',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_201_63 = Coupling(name = 'UVGC_201_63',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_202_64 = Coupling(name = 'UVGC_202_64',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_202_65 = Coupling(name = 'UVGC_202_65',
                       value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_202_66 = Coupling(name = 'UVGC_202_66',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_202_67 = Coupling(name = 'UVGC_202_67',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_203_68 = Coupling(name = 'UVGC_203_68',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_204_69 = Coupling(name = 'UVGC_204_69',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_205_70 = Coupling(name = 'UVGC_205_70',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_206_71 = Coupling(name = 'UVGC_206_71',
                       value = {-1:'( (complex(0,1)*G**2*gAu33)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gAu33)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*gAu33)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2*gAu33)/(12.*cmath.pi**2) - (complex(0,1)*G**2*gAu33*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2*gAu33)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*gAu33)/(12.*cmath.pi**2)'},
                       order = {'DMV':1,'QCD':2})

UVGC_207_72 = Coupling(name = 'UVGC_207_72',
                       value = {-1:'( -(complex(0,1)*G**2*gVu33)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**2*gVu33)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*gVu33)/(12.*cmath.pi**2)',0:'( (-5*complex(0,1)*G**2*gVu33)/(12.*cmath.pi**2) + (complex(0,1)*G**2*gVu33*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gVu33)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*gVu33)/(12.*cmath.pi**2)'},
                       order = {'DMV':1,'QCD':2})

UVGC_208_73 = Coupling(name = 'UVGC_208_73',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_209_74 = Coupling(name = 'UVGC_209_74',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_209_75 = Coupling(name = 'UVGC_209_75',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_209_76 = Coupling(name = 'UVGC_209_76',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_210_77 = Coupling(name = 'UVGC_210_77',
                       value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_211_78 = Coupling(name = 'UVGC_211_78',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_212_79 = Coupling(name = 'UVGC_212_79',
                       value = {-1:'-(G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_212_80 = Coupling(name = 'UVGC_212_80',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_212_81 = Coupling(name = 'UVGC_212_81',
                       value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_213_82 = Coupling(name = 'UVGC_213_82',
                       value = {-1:'(G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_213_83 = Coupling(name = 'UVGC_213_83',
                       value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_213_84 = Coupling(name = 'UVGC_213_84',
                       value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_214_85 = Coupling(name = 'UVGC_214_85',
                       value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_215_86 = Coupling(name = 'UVGC_215_86',
                       value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

