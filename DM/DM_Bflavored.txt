Requestor: Priscilla Pani
Content: B-flavored dark matter
TWiki: https://twiki.cern.ch/twiki/bin/view/DMLHC/BFDM
Paper: http://arxiv.org/abs/1404.1373
JIRA: https://its.cern.ch/jira/browse/AGENE-1123