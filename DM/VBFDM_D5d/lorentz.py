# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:24:16


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS12 = Lorentz(name = 'UUS12',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV16 = Lorentz(name = 'UUV16',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2)')

UUV17 = Lorentz(name = 'UUV17',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,3)')

SSS12 = Lorentz(name = 'SSS12',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS24 = Lorentz(name = 'FFS24',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS25 = Lorentz(name = 'FFS25',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV48 = Lorentz(name = 'FFV48',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV49 = Lorentz(name = 'FFV49',
                spins = [ 2, 2, 3 ],
                structure = '-(Epsilon(3,-1,-2,-3)*P(-3,3)*Gamma(-2,2,-4)*Gamma(-1,-4,1))')

FFV50 = Lorentz(name = 'FFV50',
                spins = [ 2, 2, 3 ],
                structure = '-(Epsilon(3,-1,-2,-3)*P(-3,3)*Gamma(-2,-4,1)*Gamma(-1,2,-4))')

FFV51 = Lorentz(name = 'FFV51',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV52 = Lorentz(name = 'FFV52',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS12 = Lorentz(name = 'VSS12',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS12 = Lorentz(name = 'VVS12',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV12 = Lorentz(name = 'VVV12',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS12 = Lorentz(name = 'SSSS12',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

VVSS12 = Lorentz(name = 'VVSS12',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV56 = Lorentz(name = 'VVVV56',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV57 = Lorentz(name = 'VVVV57',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV58 = Lorentz(name = 'VVVV58',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV59 = Lorentz(name = 'VVVV59',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV60 = Lorentz(name = 'VVVV60',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

