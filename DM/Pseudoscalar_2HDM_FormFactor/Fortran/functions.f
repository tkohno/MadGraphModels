

      double complex function myGGH(ss)
      ! ggH coupling form factor
      implicit none     
      include 'input.inc' ! include all model parameter
      INCLUDE 'coupl.inc'
      complex*16 c0
      external c0
      real*8 mb2, mt2,ss
      real*8 ttH, bbH
      
      mt2=mdl_mt**2
      mb2=mdl_mb**2

      ! ymf definitions are not the same as Diego's model
      ! Here I try to fix it
      ttH=mdl_ymt*(-mdl_sinbma/mdl_tanbeta+dcos(dasin(mdl_sinbma)))
      bbH=mdl_ymb*( mdl_sinbma/mdl_tanbeta+dcos(dasin(mdl_sinbma)))
            
      myGGH = 3d0/ss*(mdl_mt*ttH*(2d0 + (4d0*mt2 - ss)*c0(ss,mt2))
     &               +mdl_mb*bbH*(2d0 + (4d0*mb2 - ss)*c0(ss,mb2)))
      return 
      end
      
      double complex function myGGA(ss)
      ! ggA coupling form factor
      implicit none     
      include 'input.inc' ! include all model parameter
      INCLUDE 'coupl.inc'
      complex*16 c0
      external c0
      real*8 mb2, mt2,ss
      real*8 ttA, bbA

      mt2=mdl_mt**2
      mb2=mdl_mb**2

      ! ymf definitions are not the same
      ! Here I try to fix it
      ttA = mdl_ymt / mdl_tanbeta
      bbA = mdl_ymb * mdl_tanbeta

      myGGA = -2d0*(mdl_mb*bbA*c0(ss,mb2)+mdl_mt*ttA*c0(ss,mt2))
 
      return 
      end
      
      double complex function c0(ss,mass2)
      implicit none

      COMPLEX*16 IMAG1
      PARAMETER (IMAG1=(0D0,1D0))
      real*8 pi,tau,ss,mass2
      parameter (pi=3.141592653589793d0)

      tau=ss/(4d0*mass2)

      if (tau.gt.1d0) then
        c0=1d0/(2d0*ss)*
     &  (log((sqrt(1d0-(1d0/tau))+1d0)/(1d0-sqrt(1d0-(1d0/tau))))
     &  -imag1*pi)**2
C     Note that tau<0 happens when e.g. t-channel exchanges is considere
C     d or extra gluon is required.
C     Including this triangular graphs leads to a gauge-dependent amplit
C     ude, and thus an unphysical result. High-order corrections from 
C     e.g. the box graph are required to be introduced from external pro
C     gram. 
C     Uncomment this only when you know what you're doing!
C       elseif (tau.le.0d0) then
C         c0=(2d0/ss)*(log(sqrt(-tau)+sqrt(-tau+1d0)))**2
      else
        c0=-(2d0/ss)*dasin(sqrt(tau))**2
      endif


      end

      
      
      
