Requestor: Artur Semushin
Content: six EFT operators for neutral triple gauge couplings
Paper: https://arxiv.org/abs/1308.6323, https://arxiv.org/abs/2008.04298
Recent Paper: https://arxiv.org/abs/2206.11676
JIRA: https://its.cern.ch/jira/browse/AGENE-2204
