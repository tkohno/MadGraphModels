Requestor: Ruth Pottgen
Content: First generation scalar lepto-quarks
Paper: http://arxiv.org/abs/1506.07369
JIRA: https://its.cern.ch/jira/browse/AGENE-1150