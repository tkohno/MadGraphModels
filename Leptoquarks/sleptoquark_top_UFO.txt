Requestor: Jacob Kempster
Contents: S1 Leptoquark with coupling to all lepton and quark generations including the top quark
Source: https://github.com/ymzhong/vector_leptoquark , Martin Schmaltz (schmaltz@bu.edu) or Yiming Zhong (ymzhong@kicp.uchicago.edu)
Paper: https://link.springer.com/article/10.1007/JHEP10(2017)097 , https://link.springer.com/article/10.1007%2FJHEP01%282019%29132
JIRA: https://its.cern.ch/jira/browse/AGENE-2182
