# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 31 Oct 2015 20:00:39



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
LM = Parameter(name = 'LM',
               nature = 'external',
               type = 'real',
               value = 0.3,
               texname = '\\lambda',
               lhablock = 'LQINPUTS',
               lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Msu = Parameter(name = 'Msu',
                nature = 'external',
                type = 'real',
                value = 801,
                texname = '\\text{Msu}',
                lhablock = 'MASS',
                lhacode = [ 601 ])

Msd = Parameter(name = 'Msd',
                nature = 'external',
                type = 'real',
                value = 802,
                texname = '\\text{Msd}',
                lhablock = 'MASS',
                lhacode = [ 602 ])

Msc = Parameter(name = 'Msc',
                nature = 'external',
                type = 'real',
                value = 803,
                texname = '\\text{Msc}',
                lhablock = 'MASS',
                lhacode = [ 603 ])

Mss = Parameter(name = 'Mss',
                nature = 'external',
                type = 'real',
                value = 804,
                texname = '\\text{Mss}',
                lhablock = 'MASS',
                lhacode = [ 604 ])

Mst = Parameter(name = 'Mst',
                nature = 'external',
                type = 'real',
                value = 805,
                texname = '\\text{Mst}',
                lhablock = 'MASS',
                lhacode = [ 605 ])

Msb = Parameter(name = 'Msb',
                nature = 'external',
                type = 'real',
                value = 806,
                texname = '\\text{Msb}',
                lhablock = 'MASS',
                lhacode = [ 606 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

Wsu = Parameter(name = 'Wsu',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wsu}',
                lhablock = 'DECAY',
                lhacode = [ 601 ])

Wsd = Parameter(name = 'Wsd',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wsd}',
                lhablock = 'DECAY',
                lhacode = [ 602 ])

Wsc = Parameter(name = 'Wsc',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wsc}',
                lhablock = 'DECAY',
                lhacode = [ 603 ])

Wss = Parameter(name = 'Wss',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wss}',
                lhablock = 'DECAY',
                lhacode = [ 604 ])

Wst = Parameter(name = 'Wst',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wst}',
                lhablock = 'DECAY',
                lhacode = [ 605 ])

Wsb = Parameter(name = 'Wsb',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wsb}',
                lhablock = 'DECAY',
                lhacode = [ 606 ])

beta1 = Parameter(name = 'beta1',
                  nature = 'internal',
                  type = 'real',
                  value = '0.5',
                  texname = '\\beta _1')

beta2 = Parameter(name = 'beta2',
                  nature = 'internal',
                  type = 'real',
                  value = '0.5',
                  texname = '\\beta _2')

beta3 = Parameter(name = 'beta3',
                  nature = 'internal',
                  type = 'real',
                  value = '0.5',
                  texname = '\\beta _3')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

gsblR = Parameter(name = 'gsblR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sblR}}')

gsclR = Parameter(name = 'gsclR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sclR}}')

gsdlR = Parameter(name = 'gsdlR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sdlR}}')

gsslR = Parameter(name = 'gsslR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sslR}}')

gstlR = Parameter(name = 'gstlR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{stlR}}')

gsulR = Parameter(name = 'gsulR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sulR}}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

gsblL = Parameter(name = 'gsblL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(beta3)',
                  texname = 'g_{\\text{sblL}}')

gsbvL = Parameter(name = 'gsbvL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(1 - beta3)',
                  texname = 'g_{\\text{sbvL}}')

gsclL = Parameter(name = 'gsclL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(beta2)',
                  texname = 'g_{\\text{sclL}}')

gscvL = Parameter(name = 'gscvL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(1 - beta2)',
                  texname = 'g_{\\text{scvL}}')

gsdlL = Parameter(name = 'gsdlL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(beta1)',
                  texname = 'g_{\\text{sdlL}}')

gsdvL = Parameter(name = 'gsdvL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(1 - beta1)',
                  texname = 'g_{\\text{sdvL}}')

gsslL = Parameter(name = 'gsslL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(beta2)',
                  texname = 'g_{\\text{sslL}}')

gssvL = Parameter(name = 'gssvL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(1 - beta2)',
                  texname = 'g_{\\text{ssvL}}')

gstlL = Parameter(name = 'gstlL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(beta3)',
                  texname = 'g_{\\text{stlL}}')

gstvL = Parameter(name = 'gstvL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(1 - beta3)',
                  texname = 'g_{\\text{stvL}}')

gsulL = Parameter(name = 'gsulL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(beta1)',
                  texname = 'g_{\\text{sulL}}')

gsuvL = Parameter(name = 'gsuvL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM*cmath.sqrt(1 - beta1)',
                  texname = 'g_{\\text{suvL}}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1a33}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2a33}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3a33}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4a33}')

