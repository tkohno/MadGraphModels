Requestor: Patrick Bauer
Contents: U1 Vector Leptoquark
Paper: https://arxiv.org/abs/1901.10480v3
Source: https://feynrules.irmp.ucl.ac.be/wiki/LeptoQuark
JIRA: https://its.cern.ch/jira/browse/AGENE-1749