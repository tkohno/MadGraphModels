#!/usr/bin/bash

# Remove all top-level links from the repository
for alink in `find . -maxdepth 1 -type l`
do
  rm ${alink}
done

# Add links to all the models in the main directory
for amodel in `find * -maxdepth 1 -type d | grep -ve "PLUGIN" | grep -ve "shutil" | grep -ve "SUSY-HIT" | grep -ve "DEPRECATED" | grep "/"`
do
  ln -s ${amodel}
done

